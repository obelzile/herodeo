package com.khaosstar.herodeo.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.khaosstar.herodeo.holders.HolderManager;
import com.khaosstar.herodeo.model.actions.Action;
import com.khaosstar.herodeo.model.actions.CheckEmailAction;
import com.khaosstar.herodeo.model.actions.CreateLobbyAction;
import com.khaosstar.herodeo.model.actions.LoginAction;
import com.khaosstar.herodeo.model.actions.LogoutAction;
import com.khaosstar.herodeo.model.actions.RegisterAction;
import com.khaosstar.herodeo.model.services.IOService;

/**
 * Servlet implementation class Controller
 */
@WebServlet("/go")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private HashMap<String, Action> hashActions;
	private String paramJob;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		//invoquer super
		super.init(config);
		
		//instancier hash
		hashActions = new HashMap<String, Action>();
		//r�cup param de web.xml
		
		ServletContext sc = config.getServletContext();
		
		IOService.setContext(sc);
		
		HolderManager holder = (HolderManager)sc.getAttribute("holder");
		if(holder == null) {
			holder = new HolderManager();
			sc.setAttribute("holder", holder);
		}
			
		paramJob = sc.getInitParameter("nomParamJob");

		hashActions.put(LoginAction.class.getSimpleName(), new LoginAction("/mainlobby.jsp", "/login.jsp"));
		hashActions.put(CheckEmailAction.class.getSimpleName(), new CheckEmailAction("/login.jsp", "/login.jsp"));
		hashActions.put(RegisterAction.class.getSimpleName(), new RegisterAction("/login.jsp", "/login.jsp"));
		hashActions.put(LogoutAction.class.getSimpleName(), new LogoutAction("/login.jsp", "/mainlobby.jsp"));
		//hashActions.put(CreateLobbyAction.class.getSimpleName(), new CreateLobbyAction("/lobby.jsp", "/mainlobby.jsp"));
		hashActions.put(CreateLobbyAction.class.getSimpleName(), new CreateLobbyAction("/game.jsp", "/mainlobby.jsp"));
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String destination = "/login.jsp";
		String job = request.getParameter(paramJob);
		if(job!=null){
			Action a = hashActions.get(job);
			if(a!=null){
				a.doTheJob(request, response);
				destination= a.getDestination();
			}
			else{
				//action inconnue
				System.out.println("action inconnue");
			}
			
		}
		else{
			//le param jobToDo non pr�sent
			System.out.println("pas de param job");
		}
		
		//forwarder vers le view de destination
		System.out.println("Destination " + destination);
		RequestDispatcher rd = this.getServletContext().getRequestDispatcher(destination);
		rd.forward(request,  response);		
		
	}

}
