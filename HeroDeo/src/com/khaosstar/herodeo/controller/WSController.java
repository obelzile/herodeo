
package com.khaosstar.herodeo.controller;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import java.util.HashMap;


import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.WsOutbound;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import com.khaosstar.herodeo.holders.HolderManager;
import com.khaosstar.herodeo.model.entities.WSChannel;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.managers.ActionManager;
import com.khaosstar.herodeo.model.services.CommandDeserializer;

@SuppressWarnings( { "serial" } )
public class WSController extends WebSocketServlet
{

	Gson gSON;
	HolderManager holder;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		ServletContext sc = config.getServletContext();

		holder = (HolderManager)sc.getAttribute("holder");
		if(holder == null) {
			holder = new HolderManager();
			sc.setAttribute("holder", holder);
		}				

		gSON = new GsonBuilder().registerTypeAdapter(WSChannel.class, new CommandDeserializer()).create();
	}

	@Override
	protected boolean verifyOrigin(String origin) {
		return true;
	}

	@Override
	protected StreamInbound createWebSocketInbound(String arg0,
			HttpServletRequest request) {

		ServletContext sc = getServletContext();
		String uuid = request.getParameter("UUID");
		
		@SuppressWarnings("unchecked")
		HashMap<String, WSUser> hashOnlineUsers = (HashMap<String, WSUser>) sc.getAttribute("hashOnlineUsers");

		WSUser user = hashOnlineUsers.get(uuid);

		TheWebSocket socket = new TheWebSocket(user);	
		user.setUUID(uuid);
		user.setSocket(socket);

		HolderManager.hashConnexions.put(uuid, socket);

		return socket;

	}

	public class TheWebSocket extends MessageInbound
	{
		private WsOutbound outbound;
		private WSUser user;
		private boolean closed;

		public boolean isClosed() {
			return closed;
		}

		public void setClosed(boolean closed) {
			this.closed = closed;
		}

		public TheWebSocket(WSUser user) {
			this.user = user;
		}

		public WSUser getUser() {
			return user;
		}

		public void setUser(WSUser user) {
			this.user = user;
		}

		@Override
		public void onOpen( WsOutbound outbound )
		{
			this.outbound = outbound;
		}
		
		@Override
		protected void onClose(int status) {
			//user.setSocket(null);
			this.closed = true;

			for(ActionManager manager : HolderManager.hashManagers.values()) {
				manager.wsClosed(this.user);
			}
			//WsOutbound test = this.getWsOutbound();
			System.out.println("Socket Closed");
		}

		@Override
		protected void onTextMessage( CharBuffer buffer ) throws IOException
		{ 
			WSChannel packet = gSON.fromJson(buffer.toString(), WSChannel.class);
			String channel = packet.getChannel();

			if(HolderManager.hashManagers.containsKey(channel)) {
				HolderManager.hashManagers.get(channel).processAction(user,packet.getParams());
			}
		}

		@Override
		protected void onBinaryMessage( ByteBuffer buffer ) throws IOException
		{

		}
	}	
}
