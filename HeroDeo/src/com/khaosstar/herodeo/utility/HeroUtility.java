package com.khaosstar.herodeo.utility;

import java.util.Random;

public class HeroUtility {
	
	//Static random object to use in the entire project
	public static Random random = new Random(System.nanoTime());
	
	//HTMLEncode a string 
	public static String encodeHTML(String s)
	{
	    StringBuffer out = new StringBuffer();
	    for(int i=0; i<s.length(); i++)
	    {
	        char c = s.charAt(i);
	        if(c > 127 || c=='"' || c=='\'' || c=='<' || c=='>')
	        {
	           out.append("&#"+(int)c+";");
	        }
	        else
	        {
	            out.append(c);
	        }
	    }
	    return out.toString();
	}

}
