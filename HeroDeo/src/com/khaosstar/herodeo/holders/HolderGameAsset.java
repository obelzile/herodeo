package com.khaosstar.herodeo.holders;

import com.khaosstar.herodeo.model.game.managers.HeroManager;
import com.khaosstar.herodeo.model.game.managers.ItemManager;
import com.khaosstar.herodeo.model.game.managers.MonsterTypeManager;
import com.khaosstar.herodeo.model.game.managers.SpellManager;


public class HolderGameAsset {
	
	//Collection of all Heros
	private static HeroManager heroManager;

	//Collection of all Spells
	private static SpellManager spellManager;
	
	//Collection of all Items	
	private static ItemManager itemManager;
	
	private static MonsterTypeManager monstertypesManager;
	
	public HolderGameAsset() {
	}

	//MonsterTypeManager singleton
	public static MonsterTypeManager getMonsterTypesInstance() {
		if(monstertypesManager == null)
			monstertypesManager = new MonsterTypeManager();
		
		return monstertypesManager;
	}
	
	
	//HeroManager singleton
	public static HeroManager getHeroManagerInstance() {
		if(heroManager == null)
			heroManager = new HeroManager();
		
		return heroManager;
	}
	
	//HeroManager singleton
	public static SpellManager getSpellManagerInstance() {
		if(spellManager == null)
			spellManager = new SpellManager();
		
		return spellManager;
	}
	
	//ItemManager singleton
	public static ItemManager getItemManagerInstance() {
		if(itemManager == null)
			itemManager = new ItemManager();
		
		return itemManager;
	}
}
