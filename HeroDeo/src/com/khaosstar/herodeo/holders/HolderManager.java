package com.khaosstar.herodeo.holders;

import java.util.ArrayList;
import java.util.HashMap;

import com.khaosstar.herodeo.controller.WSController.TheWebSocket;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.managers.GameManager;
import com.khaosstar.herodeo.model.managers.ActionManager;
import com.khaosstar.herodeo.model.managers.ChatManager;

public class HolderManager {

	//Collection of all active WebSocket connections
	public static HashMap<String,TheWebSocket> hashConnexions = new HashMap<String, TheWebSocket>();;
	
	//Collection of managers that handle a WebSocket Channel
	public static HashMap<String,ActionManager> hashManagers  = new HashMap<String,ActionManager>();
	
	public HolderManager() {
		HolderManager.hashManagers.put("chat", new ChatManager("chat"));
		HolderManager.hashManagers.put("game", new GameManager("game"));
	}	
		
	//Search the WebSocket Connection to find the user that has the nickname
	public static WSUser getWSUserByNickname(String nickname){
		WSUser user = null;
		@SuppressWarnings({ "rawtypes", "unchecked" })
		ArrayList<TheWebSocket>alConnexions = new ArrayList(hashConnexions.values());
		for(TheWebSocket socket: alConnexions){
			if(nickname.equals(socket.getUser().getNickname())){
				user = socket.getUser();
			}
		}
		return user;
	}
}
