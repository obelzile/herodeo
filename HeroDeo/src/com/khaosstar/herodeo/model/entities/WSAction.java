package com.khaosstar.herodeo.model.entities;


import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;

public class WSAction {

	@Expose private String action;
	@Expose private WSActionParams params;
	
	public WSAction(String string) {
		params = new WSActionParams();		
	}
	
	public WSAction(String action, WSActionParams params) {
		this.action = action;
		this.params = params;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public WSActionParams getParams() {
		return params;
	}

	public void setParams(WSActionParams params) {
		this.params = params;
	}

	public String getRoomUUID() {
		if(params != null && params.containsKey("roomuuid") && !(((String)params.get("roomuuid")).contains("default")))
			return (String)params.get("roomuuid");
		else
			return "default"; //GlobalChat room default
	}
}
