package com.khaosstar.herodeo.model.entities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public abstract class WSActionListener {

	protected HashMap<String, Method> hashActions;
	
	public WSActionListener() {
		hashActions = new HashMap<String,Method>();		
	}
		
	protected boolean registerAction(String action, Method callback) {
		
		boolean result = false;
		
		if (!hashActions.containsKey(action)) {
			hashActions.put(action, callback);
			result = true;
		}
		
		return result;
	}

	/**
	 * Process the action coming from the client. call the right method using reflexion
	 * @param user
	 * @param action
	 * @return
	 */
	public boolean processAction(WSUser user,WSAction action) {
		
		boolean result = false;
		
		if (hashActions.containsKey(action.getAction())) {
			Method m = hashActions.get(action.getAction());
			try {
				m.invoke(this, user,action.getParams());
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			result = true;
		}
		
		return result;
	}
}
