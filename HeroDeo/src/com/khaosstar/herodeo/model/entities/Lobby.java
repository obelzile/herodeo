package com.khaosstar.herodeo.model.entities;

import java.util.ArrayList;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.managers.ChatManager;

public class Lobby extends Room {

	public Lobby(String uuid, String lobbyName, String password, ChatManager manager) {
		super(manager);
		this.uuid = uuid;
		this.lobbyName = lobbyName;
		this.password = password;
		this.opened = true;
		
		this.wsActionListener = new WSChatActionListener(this);		
	}
		
	/**
	 * Send the user the list of online users in the current room
	 * @param user
	 */
	public void SendUserOnlineListInCurrentRoom(WSUser user) {
		Room currentRoom = manager.getHashRoom().get(user.getChatRoomUUID());
		WSActionParams params = new WSActionParams();
		
		if(currentRoom == null) {
			//TODO room doesn't exist
		}
		
		for (WSUser u : currentRoom.getRoomMembers().values()){
			if (u.getNickname().equals(user.getNickname()))
				continue;
			params.put("nickname", u.getNickname());
			params.put("room", this.uuid);
			WSAction action = new WSAction("onlineuserlist", params);
			manager.sendMessage(action, user);
		}		
	}
	
	/**
	 * Send the user the list of games
	 * @param user
	 */
	public void SendOnlineLobbies(WSUser user) {
		ArrayList<Lobby> onlineLobbies = new ArrayList(manager.getHashRoom().values());
		WSActionParams params = new WSActionParams();
		
		for (Lobby l : onlineLobbies) {
			if (l.getLobbyName().equals("Global Chat")){
				continue;
			}
			
			params.put("roomname", l.getLobbyName());
			
			if(l.getPassword() != null && l.getPassword().length()>0){
				params.put("passwordPresent", "true");
			}
			WSAction onlinelobbieslist = new WSAction("onlinelobbieslist", params);
			manager.sendMessage(onlinelobbieslist, user);
		}	
	}	
	
	/**
	 * Process user joining a room
	 * @param user
	 * @return return true of successful 
	 */
	public boolean userJoin(WSUser user) {
		boolean result = false;
			
		if(!getRoomMembers().containsKey(user.getUUID())){
			getRoomMembers().put(user.getUUID(), user);
			result = true;
		} else if(getRoomMembers().size() == 1) {
			setAdminuuid(user.getUUID());
		}

		return result;
	}
	
	/**
	 * Process user leaving a lobby
	 * @param user
	 */
	public void userLeave(WSUser user) {
		Room currentRoom = manager.getHashRoom().get(user.getChatRoomUUID());
		if(currentRoom.roomMembers.containsKey(user.getUUID())){
			String currentLobbyName = ((Lobby)currentRoom).getLobbyName();
			
			currentRoom.roomMembers.remove(user.getUUID());
			if(currentRoom.roomMembers.size() == 0 && !currentLobbyName.contains("Global Chat")){
				manager.getHashRoom().remove(((Lobby)currentRoom).getUuid());
				WSActionParams params = new WSActionParams();
				params.put("roomname", currentLobbyName);
				
				WSAction roomEmpty = new WSAction("roomempty", params);
				manager.getHashRoom().get("default").broadcastAction(roomEmpty);
			}
			WSActionParams params = new WSActionParams();
			params.put("nickname", user.getNickname());
			
			WSAction userLeave = new WSAction("userquit", params);
			broadcastAction(userLeave);	
		}
	}
	
	//*** Properties
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getLobbyName() {
		return lobbyName;
	}

	public void setLobbyName(String lobbyName) {
		this.lobbyName = lobbyName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
