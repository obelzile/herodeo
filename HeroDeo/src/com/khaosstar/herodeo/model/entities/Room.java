package com.khaosstar.herodeo.model.entities;

import java.util.LinkedHashMap;

import com.khaosstar.herodeo.model.managers.ActionManager;

public abstract class Room {

	protected String uuid;
	protected String lobbyName;
	protected String password;
	protected Boolean opened;
	protected String adminuuid;
	protected LinkedHashMap<String,WSUser> roomMembers;
	
	protected ActionManager manager;
	protected WSActionListener wsActionListener;
	
	public Room(ActionManager manager) {
		this.manager = manager;	
		setRoomMembers(new LinkedHashMap<String,WSUser>());	
	}
	
	public String getAdmin() {
		return adminuuid;
	}

	public void setAdmin(String adminuuid) {
		this.adminuuid = adminuuid;
	}

	public boolean processAction(WSUser user,WSAction action) {
		
		return wsActionListener.processAction(user, action);
	}

	public ActionManager getManager() {
		return manager;
	}

	public void setManager(ActionManager manager) {
		this.manager = manager;
	}
	
	public LinkedHashMap<String,WSUser> getRoomMembers() {
		return roomMembers;
	}

	public void setRoomMembers(LinkedHashMap<String,WSUser> roomMembers) {
		this.roomMembers = roomMembers;
	}	

	public void userLeft(WSUser user) {
	
		/*
		WSUser membre = roomMembers.get(user.getUUID());
		
		if(membre != null) {
			roomMembers.remove(user.getUUID());
			
			HashMap<String, Object> params = new HashMap<>();
			params.clear();
			params.put("room", this.uuid);
			params.put("nickname", user.getNickname());
			
			WSAction userquit = new WSAction("userleftroom", params);
			broadcastAction(userquit);
		} */
	}
	
	public void broadcastAction(WSAction action) {
		for(WSUser user : roomMembers.values()) {
			manager.sendMessage(action, user);
		}

	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getLobbyName() {
		return lobbyName;
	}

	public void setLobbyName(String lobbyName) {
		this.lobbyName = lobbyName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getOpened() {
		return opened;
	}

	public void setOpened(Boolean opened) {
		this.opened = opened;
	}

	public String getAdminuuid() {
		return adminuuid;
	}

	public void setAdminuuid(String adminuuid) {
		this.adminuuid = adminuuid;
	}	
}
