package com.khaosstar.herodeo.model.entities;

import java.util.ArrayList;

import com.khaosstar.herodeo.holders.HolderManager;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;

public class WSChatActionListener extends WSActionListener  {

	private Lobby lobby;
	
	public WSChatActionListener(Lobby lobby) {
		this.lobby = lobby;
		
		try {
			registerAction("ChatMessage", WSChatActionListener.class.getDeclaredMethod("ChatMessage", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("ConnectToRoom", WSChatActionListener.class.getDeclaredMethod("ConnectToRoom", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("leaveRoom", WSChatActionListener.class.getDeclaredMethod("leaveRoom", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("KickUser", WSChatActionListener.class.getDeclaredMethod("KickUser", new Class[] {WSUser.class,WSActionParams.class}));
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {		
			e.printStackTrace();
		}		
	}
	
	public void ChatMessage(WSUser user,WSActionParams params) {
		WSUser recipientUser = null;
		String message = (String)params.get("message");
		String nickname = user.getNickname();
		String recipient =  (String)params.get("recipient");

		params.clear();
		params.put("nickname", nickname);
		params.put("message", message);
		params.put("room", this.lobby.getUuid());
			
		WSAction action = new WSAction("chatmessage",params);
		
		if (recipient != null ){
			ArrayList<WSUser> currentUsers = new ArrayList(((Lobby)this.lobby.getManager().getHashRoom().get(user.getChatRoomUUID())).getRoomMembers().values());
			for (WSUser recipientUsername : currentUsers) {
				if (recipientUsername.getNickname().equals(recipient)) {
					recipientUser = recipientUsername;
				}						
			}

			this.lobby.getManager().sendMessage(action, recipientUser);
			this.lobby.getManager().sendMessage(action, user);			
		}	
		else {
			this.lobby.broadcastAction(action);
		}		
	}
		
	public void ConnectToRoom(WSUser user,WSActionParams params) {
		String roomUUID, roomPW;
		
		roomUUID = (String)params.get("roomuuid");
		roomPW = (String)params.get("roompw");
		System.out.println(roomPW + "Room PW");
		if(this.lobby.getPassword().equals(roomPW) ||roomUUID.equals("default")) {
			if (user.getChatRoomUUID() != null) {
				Lobby lobby = (Lobby)this.lobby.getManager().getHashRoom().get(user.getChatRoomUUID());
				// no need to leave a room you are already in ( usualy happen when you reload the page )
				if(lobby != null && !user.getChatRoomUUID().equals(roomUUID)) {
					lobby.userLeave(user);
				}	
				user.setChatRoomUUID(roomUUID);
			}		
			this.lobby.userJoin(user);
			user.setChatRoomUUID(roomUUID);
			
			params.clear();
			params.put("room", this.lobby.getUuid());
			params.put("nickname", user.getNickname());
			
			if (roomPW != null && roomPW.length()>0) {
				params.put("passwordPresent", "true");
			}
			
			WSAction newroomuser = new WSAction("newroomuser",params);
			
			this.lobby.broadcastAction(newroomuser);
			
			this.lobby.SendUserOnlineListInCurrentRoom(user);
			this.lobby.SendOnlineLobbies(user);			
		}		
	}
	
	public void KickUser(WSUser user, WSActionParams params) {
		Lobby currentLobby = (Lobby)this.lobby.getManager().getHashRoom().get(user.getChatRoomUUID());
		if(user.getUUID().equals(currentLobby.getAdminuuid())){
			String nickname = (String) params.get("nickname");
			WSUser userToKick = HolderManager.getWSUserByNickname(nickname);
			WSAction userKicked = new WSAction("userkicked", params);
			this.lobby.getManager().sendMessage(userKicked, userToKick);
		}	
	}
	
	public void leaveRoom(WSUser user,WSActionParams params) {
		params.clear();
		params.put("room", this.lobby.getUuid());
		params.put("nickname", user.getNickname());
		
		WSAction userquit = new WSAction("userquit", params);
		this.lobby.broadcastAction(userquit);
	}
}
