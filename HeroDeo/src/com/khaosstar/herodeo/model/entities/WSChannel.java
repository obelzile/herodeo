package com.khaosstar.herodeo.model.entities;

import com.google.gson.annotations.Expose;

/**
 * Entity used to communicate with the client. is serialized to and from JSON
 * 
 *
 */
public class WSChannel {
	@Expose private String channel;
	@Expose private WSAction params;
	
	public WSChannel() {
		params = new WSAction("");
	}
	
	public WSChannel(String channel,WSAction params) {
		this.channel = channel;
		this.params = params;				
	}
	
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public WSAction getParams() {
		return params;
	}

	public void setParams(WSAction params) {
		this.params = params;
	}
}
