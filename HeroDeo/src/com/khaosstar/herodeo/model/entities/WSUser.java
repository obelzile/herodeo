package com.khaosstar.herodeo.model.entities;

import com.khaosstar.herodeo.controller.WSController.TheWebSocket;

/**
 * 
 * WebSocket User entity
 *
 */
public class WSUser extends User {

	private TheWebSocket socket;
	private String mUUID;
	private String chatRoomUUID;
	private String gameRoomUUID;

	public WSUser(WSUser u) {
		super(u.getUserId(), u.getEmail(), u.getPassword(), u.getFirstname(), u.getLastname(), u.getNickname());
		this.socket = u.getSocket();
		this.mUUID = u.getUUID();
		this.chatRoomUUID = u.getChatRoomUUID();
		this.gameRoomUUID = u.getGameRoomUUID(); 
	}
	
	public WSUser(User u) {
		super(u.getUserId(), u.getEmail(), u.getPassword(), u.getFirstname(), u.getLastname(), u.getNickname());
		this.mUUID = java.util.UUID.randomUUID().toString();		
	}

	public TheWebSocket getSocket() {
		return socket;
	}

	public void setSocket(TheWebSocket socket) {
		this.socket = socket;
	}

	public String getUUID() {
		return mUUID;
	}

	public void setUUID(String mUUID) {
		this.mUUID = mUUID;
	}

	public String getGameRoomUUID() {
		return gameRoomUUID;
	}

	public void setGameRoomUUID(String gameRoomUUID) {
		this.gameRoomUUID = gameRoomUUID;
	}

	public String getChatRoomUUID() {
		return this.chatRoomUUID;
	}

	public void setChatRoomUUID(String roomUUID) {
		this.chatRoomUUID = roomUUID;
	}	
}
