package com.khaosstar.herodeo.model.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSActionListener;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Cell.CellType;
import com.khaosstar.herodeo.model.game.entites.Player;

public class WSGameActionListener extends WSActionListener {
	
	private GameRoom gameroom;
	
	public WSGameActionListener(GameRoom gameroom) {
	
		this.gameroom = gameroom;
		
		try {			
			registerAction("ConnectToRoom", WSGameActionListener.class.getDeclaredMethod("connectToRoom", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("StartGame", WSGameActionListener.class.getDeclaredMethod("startGame", new Class[] {WSUser.class,WSActionParams.class}));			
			registerAction("SelectHero", WSGameActionListener.class.getDeclaredMethod("selectHero", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("PlayerEndTurn", WSGameActionListener.class.getDeclaredMethod("playerEndTurn", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("InitiateMonsterAttack", WSGameActionListener.class.getDeclaredMethod("initiateMonsterAttack", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("InitiateAttack", WSGameActionListener.class.getDeclaredMethod("initiateAttack", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("EndCombat", WSGameActionListener.class.getDeclaredMethod("endCombat", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("CounterAttack", WSGameActionListener.class.getDeclaredMethod("counterAttack", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("SetDefenseAction", WSGameActionListener.class.getDeclaredMethod("setDefenseAction", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("ThrowCoinInWell", WSGameActionListener.class.getDeclaredMethod("throwCoinInWell", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("DrinkFromWell", WSGameActionListener.class.getDeclaredMethod("drinkFromWell", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("CastSpell", WSGameActionListener.class.getDeclaredMethod("castSpell", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("ExecuteInitScripts", WSGameActionListener.class.getDeclaredMethod("executeInitScripts", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("GetSpellList", WSGameActionListener.class.getDeclaredMethod("getSpellList", new Class[] {WSUser.class,WSActionParams.class}));			
			registerAction("RamasserMarqueur", WSGameActionListener.class.getDeclaredMethod("ramasserMarqueur", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("VisiteMarchand", WSGameActionListener.class.getDeclaredMethod("visiteMarchand", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("BuyItem", WSGameActionListener.class.getDeclaredMethod("buyItem", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("RequestMovementState", WSGameActionListener.class.getDeclaredMethod("requestMovementState", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("FinishMovementState", WSGameActionListener.class.getDeclaredMethod("finishMovementState", new Class[] {WSUser.class,WSActionParams.class}));			
			registerAction("MovementRequest", WSGameActionListener.class.getDeclaredMethod("movementRequest", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("RequestMonsterMovementState", WSGameActionListener.class.getDeclaredMethod("requestMonsterMovementState", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("RequestMonsterAttackState", WSGameActionListener.class.getDeclaredMethod("requestMonsterAttackState", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("MonsterMovementRequest", WSGameActionListener.class.getDeclaredMethod("monsterMovementRequest", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("MonsterActionRequest", WSGameActionListener.class.getDeclaredMethod("monsterActionRequest", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("FinishMonsterMovementState", WSGameActionListener.class.getDeclaredMethod("finishMonsterMovementState", new Class[] {WSUser.class,WSActionParams.class}));
			registerAction("RequestMonsterControlSkip", WSGameActionListener.class.getDeclaredMethod("requestMonsterControlSkip", new Class[] {WSUser.class,WSActionParams.class}));
			
					
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
		
	////////////////////////
	// MOVEMENT BEGIN
	////////////////////////
	
	/**
	 * request to start moving. validate if moving is possible
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */	
	public void requestMonsterMovementState(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				if(this.gameroom.getControlledMonster() != null) {
					this.gameroom.Net.outbound.requestMonsterMoveState(player);
					this.gameroom.Net.outbound.updateCurrentPlayerControls();
				}
			}
		}
	}

	/**
	 * movement request from the client. validate if moving is possible
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void monsterMovementRequest(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				if(this.gameroom.getControlledMonster() != null && !this.gameroom.getControlledMonster().hasMoved()) {
					this.gameroom.MouvementManager.monsterMovementRequest(player,this.gameroom.getControlledMonster(),params);
				}
			}
		}
	}
	
	/**
	 * request to end movement. validate if moving is possible
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void finishMonsterMovementState(WSUser user,WSActionParams params) {
		this.gameroom.MouvementManager.finishMonsterMovementState(user, params);
		this.gameroom.Net.outbound.updateCurrentPlayerControls();
	}
	
	public void requestMonsterControlSkip(WSUser user,WSActionParams params) {
		this.gameroom.GameActionManager.requestMonsterControlSkip(user, params);
		this.gameroom.Net.outbound.updateCurrentPlayerControls();
	}
	
	/**
	 * Allow the user to move after triggering this state
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void requestMovementState(WSUser user,WSActionParams params) {
		this.gameroom.MouvementManager.requestMovementState(user, params);
	}
	
	/**
	 * movement request from the client. validate if moving is possible
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void movementRequest(WSUser user,WSActionParams params) {
		this.gameroom.MouvementManager.movementRequest(user, params);
	}
	
	/**
	 * Prevent the user from moving after this move state finished
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void finishMovementState(WSUser user,WSActionParams params) {
		this.gameroom.MouvementManager.finishMovementState(user, params);
	}
		
	////////////////////////
	// MOVEMENT END
	////////////////////////
	
		
	public void monsterActionRequest(WSUser user,WSActionParams params) throws Exception {		
		this.gameroom.GameActionManager.monsterActionRequest(user, params);
	}
		
	////////////////////////
	// ATTACK BEGIN
	////////////////////////
	
	public void requestMonsterAttackState(WSUser user,WSActionParams params) {
		this.gameroom.AttackManager.requestMonsterAttackState(user, params);		
	}		
		
	/**
	 * Initiate combat between 2 player if they are in range
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void initiateAttack(WSUser user,WSActionParams params) throws Exception {
		this.gameroom.AttackManager.initiateAttack(user, params);
	}
	
	/**
	 * Initiate combat between 2 player if they are in range
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void initiateMonsterAttack(WSUser user,WSActionParams params) throws Exception {
		this.gameroom.AttackManager.initiateMonsterAttack(user, params);
	}
	
	/**
	 * end current combat
	 * and resolve any death
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void endCombat(WSUser user,WSActionParams params) throws Exception {
		this.gameroom.AttackManager.endCombat(user, params);
	}

	/**
	 * Counter attack during combat
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void counterAttack(WSUser user,WSActionParams params) throws Exception {
		this.gameroom.AttackManager.counterAttack(user, params);
	}
	
	/**
	 * Set defensive action to block or esquive
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void setDefenseAction(WSUser user,WSActionParams params) throws Exception {
		this.gameroom.AttackManager.setDefenseAction(user, params);
	}	
	
	////////////////////////
	// ATTACK END
	////////////////////////
	
	
	public void ramasserMarqueur(WSUser user,WSActionParams params) {
		this.gameroom.GameActionManager.ramasserMarqueur(user, params);		
	}
	
	public void buyItem(WSUser user,WSActionParams params) {
		this.gameroom.GameActionManager.buyItem(user, params);
	}
		
	public void visiteMarchand(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {				
				this.gameroom.requestMerchant(player);
			}
		}				
	}
	
	public void getSpellList(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
								
				switch(params.getAsString("type")) {
				case "freenovice":
					params.put("spells", player.getSpellBook().getNoviceSpell(-1));
					break;
				case "free":
					params.put("spells", player.getSpellBook().getCastableSpells(-1));
					break;
				default:
					params.put("spells", player.getSpellBook().getCastableSpells(player.getCurrentPM()));					
				}
								
				WSAction action = new WSAction("spellsWindow",params);
				
				this.gameroom.getManager().sendMessage(action, user);
			}
		}				
	}
	
	/**
	 * Cast a spell
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void castSpell(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState() && !this.gameroom.isMoving()) {
			if(this.gameroom.getControlledMonster() == null || !this.gameroom.getControlledMonster().isControlling()) {
				this.gameroom.getSpellEngine().beginSpellCast(user, params);
			}
		}
	}
	
	/**
	 * Cast a spell
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void executeInitScripts(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			this.gameroom.getSpellEngine().executeInitScripts(user, params);
		}
	}	
	
	/**
	 * Perform the action of throwing a coin in the well
	 * trigger the destiny book
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void throwCoinInWell(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				if(player.getCurrentPO() > 0) {
					player.removePO(1);
					//TODO destin
					this.gameroom.Net.outbound.broadcastUnit(player, null);
				}
			}
		}
	}
	
	/**
	 * Perform the action of drinking from the well if the user has not drank from it already this turn
	 * gain 2 PV when drinking water
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void drinkFromWell(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				if(this.gameroom.getBoard().hasCellType(player.getPosition(), CellType.WELL) && !this.gameroom.hasDrank()) {
					this.gameroom.setDrank(true);
					player.addPV(GameRoom.HEALTH_FROM_WELL);
					this.gameroom.Net.outbound.broadcastUnit(player, null);
					this.gameroom.Net.outbound.updateCurrentPlayerControls();
				}
			}
		}
	}
	
	
	/**
	 * End current player turn and move to the next player. 
	 * if every player have played this turn, increase turn by 1 and move to first player
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void playerEndTurn(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				
				gameroom.setPlayerTurnIndex(gameroom.getPlayerTurnIndex()+1);
				
				if(gameroom.getPlayerTurnIndex() >= this.gameroom.getUnits().getPlayerCount()) {
					gameroom.setPlayerTurnIndex(0);
					this.gameroom.setGameTurn(this.gameroom.getGameTurn()+1);
					
					params.clear();
					params.put("turn", this.gameroom.getGameTurn());
					
					WSAction action = new WSAction("newTurn",params);
					this.gameroom.broadcastAction(action);
				} else {
					
				}
									
				//broadcast player update
				newPlayerTurn(params);
			}
		}
	}
	
	/**
	 * Start of a player turn
	 * reset values for that turn
	 *
	 * @param  params params sent with the action
	 */
	public void newPlayerTurn(WSActionParams params) {
		
		//boradcast new turn
		Player player = this.gameroom.getCurrentPlayer();
		
		this.gameroom.getMovementStep().clear();
		this.gameroom.setMoving(false);
		
		this.gameroom.setDrank(false);
		this.gameroom.setAttacked(false);
		this.gameroom.setMoved(false);
					
		params.clear();
		params.put("playerindex", gameroom.getPlayerTurnIndex());
		
		WSAction action = new WSAction("newPlayerTurn",params);
		this.gameroom.broadcastAction(action);
		
		this.gameroom.phaseBonus(player);
		
		this.gameroom.Net.outbound.broadcastUnit(player, null);
		
		action.setAction("bonusPhase");
		
		this.gameroom.broadcastAction(action);
		
		action.setAction("mainPhase");
		
		this.gameroom.broadcastAction(action);		
	}
	
	
	
	/**
	 * 
	 * Choose a hero is the hero selection
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void selectHero(WSUser user,WSActionParams params) {
		
		if(this.gameroom.isHeroSelectionState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				
				player.setHeroId(params.getAsInteger("heroid"));
				player.getSpellBook().drawCard(4);
								
				this.gameroom.Net.outbound.broadcastUnit(player, null);
								
				//done choosing game start
				if(this.gameroom.getPlayerTurnIndex() >= this.gameroom.getUnits().getPlayerCount()-1) {					
					this.gameroom.gameBegin();				
				} else {
					this.gameroom.setPlayerTurnIndex(this.gameroom.getPlayerTurnIndex()+1);
					this.gameroom.broadcastNewPlayerTurn(true);
				}				
			}
		}	
	}	
	
	/**
	 * User have join a game room
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void connectToRoom(WSUser user,WSActionParams params) {
		String roomUUID, roomPW;
		
		roomUUID = (String)params.get("roomuuid");
		roomPW = (String)params.get("roompw");
				
		this.gameroom.connectToRoom(user, roomUUID, roomPW);
		
	}
	
	/**
	 * Admin has started the game
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void startGame(WSUser user,WSActionParams params) {
		this.gameroom.startGame(user);
	}			
}

