package com.khaosstar.herodeo.model.game;

import java.util.EnumSet;

import com.khaosstar.herodeo.model.game.entites.Cell;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.services.SpellEngine;

public class GameControls {

	GameRoom room;
	
	private boolean controlingmonster;
	private boolean monstercanmove;
	private boolean monstercanattack;
	private boolean monstercanskip;
	
	private boolean canmove;
	private boolean canattack;
	private boolean candrink;
	private boolean candropcoin;
	private boolean canopenchest;
	private boolean cancastspell;
	private boolean cancastnovicefree;
	private boolean cancastfreespell;
	private boolean canvisitmerchant;	
	private boolean cancontrolmonster;	
	private boolean canendturn;

	public GameControls(GameRoom room) {
		this.room = room;
		
		controlingmonster = false;
		monstercanmove = false;
		monstercanattack = false;
		monstercanskip = false;
		
		canmove = false;
		canattack = false;
		candrink = false;
		candropcoin = false;
		canopenchest = false;
		cancastspell = false;
		cancastnovicefree = false;
		cancastfreespell = false;
		canvisitmerchant = false;	
		cancontrolmonster = false;	
		canendturn = true;
	}
	
	public void refresh() {
		
		Player player = room.getCurrentPlayer();

		controlingmonster = false;
		monstercanmove = false;
		monstercanattack = false;
		monstercanskip = true;		
		
		canmove = false;
		canattack = false;
		candrink = false;
		candropcoin = false;
		canopenchest = false;
		cancastspell = false;
		cancastnovicefree = false;
		cancastfreespell = false;
		canvisitmerchant = false;	
		cancontrolmonster = false;	
		canendturn = true;

		if(room.getGameState() == 2) {
			
			if(room.getControlledMonster() != null && room.getControlledMonster().isControlling()){			
				controlingmonster = true;				
				monstercanmove = !room.getControlledMonster().hasMoved();
				monstercanattack = !room.getControlledMonster().hasAttacked();				
			}
						
			if(!room.hasMoved())
				canmove = true;
			
			if(player.getAttackTargets() != null && player.getAttackTargets().size() > 0)
				canattack = true;
			
			if(room.getBoard().hasCellType(player.getPosition(), Cell.CellType.WELL) && !room.hasDrank())
				candrink = true;
			
			if(room.getBoard().hasCellType(player.getPosition(), Cell.CellType.WELL) && player.getCurrentPO() > 0)
				candropcoin = true;		
					
			if(room.getBoard().hasCellType(player.getPosition(), Cell.CellType.CHEST))
				canopenchest = true;						
					
			if(player.getSpellBook() != null && player.getSpellBook().getCastableSpellsCount(player.getCurrentPM()) > 0)
				cancastspell = true;	
	
			if(room.getBoard() != null && room.getBoard().hasCellType(player.getPosition(), Cell.CellType.BLUEENERGY) && room.isFreenovicespell())
				cancastnovicefree = true;		
			
			if(room.getBoard() != null && room.getBoard().hasCellType(player.getPosition(), Cell.CellType.REDCHEST) && room.isFreespell())
				cancastfreespell = true;		
			
			if(room.getBoard() != null && room.getBoard().hasAnyOfCellType(player.getPosition(), EnumSet.of(Cell.CellType.START0,Cell.CellType.START1,Cell.CellType.START2,Cell.CellType.START3)))
				canvisitmerchant = true;
			if(player.getEffects().contains(SpellEngine.enumEffect.PORTABLE_MERCHANT))
				canvisitmerchant = true;
			
			if(room.getControlledMonster() == null) 		
				cancontrolmonster = true;
			
			if(player.getCurrentSpell() != null && player.getCurrentSpell().isCasting())
				canendturn = false;
		}
	}	

	public boolean isCanmove() {
		return canmove;
	}

	public void setCanmove(boolean canmove) {
		this.canmove = canmove;
	}

	public boolean isCanattack() {
		return canattack;
	}

	public void setCanattack(boolean canattack) {
		this.canattack = canattack;
	}

	public boolean isCandrink() {
		return candrink;
	}

	public void setCandrink(boolean candrink) {
		this.candrink = candrink;
	}

	public boolean isCandropcoin() {
		return candropcoin;
	}

	public void setCandropcoin(boolean candropcoin) {
		this.candropcoin = candropcoin;
	}

	public boolean isCanopenchest() {
		return canopenchest;
	}

	public void setCanopenchest(boolean canopenchest) {
		this.canopenchest = canopenchest;
	}

	public boolean isCancastspell() {
		return cancastspell;
	}

	public void setCancastspell(boolean cancastspell) {
		this.cancastspell = cancastspell;
	}

	public boolean isCancastnovicefree() {
		return cancastnovicefree;
	}

	public void setCancastnovicefree(boolean cancastnovicefree) {
		this.cancastnovicefree = cancastnovicefree;
	}

	public boolean isCancastfreespell() {
		return cancastfreespell;
	}

	public void setCancasefreespell(boolean cancasefreespell) {
		this.cancastfreespell = cancasefreespell;
	}

	public boolean isCanvisitmerchant() {
		return canvisitmerchant;
	}

	public void setCanvisitmerchant(boolean canvisitmerchant) {
		this.canvisitmerchant = canvisitmerchant;
	}

	public boolean isCancontrolmonster() {
		return cancontrolmonster;
	}

	public void setCancontrolmonster(boolean cancontrolmonster) {
		this.cancontrolmonster = cancontrolmonster;
	}

	public boolean isCanendturn() {
		return canendturn;
	}

	public void setCanendturn(boolean canendturn) {
		this.canendturn = canendturn;
	}

	public boolean isControlingmonster() {
		return controlingmonster;
	}

	public void setControlingmonster(boolean controlingmonster) {
		this.controlingmonster = controlingmonster;
	}

	public boolean isMonstercanmove() {
		return monstercanmove;
	}

	public void setMonstercanmove(boolean monstercanmove) {
		this.monstercanmove = monstercanmove;
	}

	public boolean isMonstercanattack() {
		return monstercanattack;
	}

	public void setMonstercanattack(boolean monstercanattack) {
		this.monstercanattack = monstercanattack;
	}

	public boolean isMonstercanskip() {
		return monstercanskip;
	}

	public void setMonstercanskip(boolean monstercanskip) {
		this.monstercanskip = monstercanskip;
	}
		
	
}
