package com.khaosstar.herodeo.model.game.net;

import com.khaosstar.herodeo.model.game.GameRoom;

public class Net {
	public NetInBound inbound;
	public NetOutBound outbound;
	
	public Net(GameRoom room) {
		inbound = new NetInBound(room);
		outbound = new NetOutBound(room);
	}
}
