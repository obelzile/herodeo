package com.khaosstar.herodeo.model.game.net;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.GameControls;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Cell;
import com.khaosstar.herodeo.model.game.entites.Monster;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Spell;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.model.game.services.SpellEngine;

public class NetOutBound {
	GameRoom room;
	
	public NetOutBound(GameRoom room) {
		this.room = room;
	}
	
	/*public void broadcastUnitMove(Unit unit,WSUser user) {
		if(unit != null) {
			WSActionParams params = new WSActionParams();
			
			params.put("result", "ok");
			params.put("unituuid", unit.getUuid());
			params.put("position", unit.getPosition());		
			params.put("movement", unit.getStepLeft());
				
			WSAction action = new WSAction("playermove",params);
			
			if(user == null) {
				room.broadcastAction(action);
			} else {
				room.getManager().sendMessage(action, user);
			}
		}
	}*/
	
	public void broadcastPlayers(WSUser user) {
		List<Player> players = room.getUnits().getPlayerList();
		if(players.size() > 0) {
			WSActionParams params = new WSActionParams();
			params.put("units", players);
			
			WSAction action = new WSAction("unitsupdate",params);
			
			if(user == null) {
				room.broadcastAction(action);
			} else {
				room.getManager().sendMessage(action, user);
			}
		}
	}
	
	public void broadcastPlayer(int index,WSUser user) {						
		Player player = room.getUnits().getPlayerIndex(index);
		if(player != null) {
			WSActionParams params = new WSActionParams();			
			params.put("unit", player);
			
			WSAction action = new WSAction("unitupdate",params);
			
			if(user == null) {
				room.broadcastAction(action);
			} else {
				room.getManager().sendMessage(action, user);
			}
		}
	}	
	
	public void broadcastMonsters(WSUser user) {					
		List<Monster> monsters = room.getUnits().getMonsterList();
		if(monsters.size() > 0) {
			WSActionParams params = new WSActionParams();
			params.put("units", monsters);
			
			WSAction action = new WSAction("unitsupdate",params);
			
			if(user == null) {
				room.broadcastAction(action);
			} else {
				room.getManager().sendMessage(action, user);
			}
		}
	}	
	
	public void broadcastMonster(Monster monster, WSUser user) {
		if(monster != null) {
			WSActionParams params = new WSActionParams();
			params.put("unit", monster);
			
			WSAction action = new WSAction("unitupdate",params);
			
			if(user == null) {
				room.broadcastAction(action);
			} else {
				room.getManager().sendMessage(action, user);
			}
		}
	}	

	public void broadcastUnit(Unit unit, WSUser user) {
		if(unit != null) {
			WSActionParams params = new WSActionParams();
			params.put("unit", unit);
			
			WSAction action = new WSAction("unitupdate",params);
			
			if(user == null) {
				room.broadcastAction(action);
			} else {
				room.getManager().sendMessage(action, user);
			}
		}
	}
	
	public void broadcastRemoveUnit(Unit unit, WSUser user) {
		if(unit != null) {
			WSActionParams params = new WSActionParams();
			params.put("unit", unit);
			
			WSAction action = new WSAction("unitremove",params);
			
			if(user == null) {
				room.broadcastAction(action);
			} else {
				room.getManager().sendMessage(action, user);
			}
		}
	}
	
	public void broadcastUnitMove(Unit unit,WSUser user) {
		if(unit != null) {
			WSActionParams params = new WSActionParams();
			
			params.put("result", "ok");
			params.put("unit", unit);			
				
			WSAction action = new WSAction("unitmove",params);
			
			if(user == null) {
				room.broadcastAction(action);
			} else {
				room.getManager().sendMessage(action, user);
			}
		}		
	}
	
	public void broadcastGameStateInfos(WSUser user) {
		WSActionParams params = new WSActionParams();

		List<Player> broadcastPlayers = null;
		List<Player> players = room.getUnits().getPlayerList();
		
		if(user != null) {
			broadcastPlayers = new ArrayList<Player>();
			broadcastPlayers.add((Player)room.getUnits().getUnit(user.getUUID()));
		} else {
			broadcastPlayers = room.getUnits().getPlayerList();
		}			
		
		for(Player player : broadcastPlayers) {
			params.put("gamestate",room.getGameState());
			params.put("layout", room.getBoard().getLayout());			
			params.put("players", players);
			params.put("myuuid", player.getUuid());
			params.put("myindex", player.getIndex());
			params.put("playerindex", room.getPlayerTurnIndex());
			params.put("turn", room.getGameTurn());
			params.put("drank", room.hasDrank());
			params.put("treasures", room.getTreasures().getTreasures());
			params.put("monsters", room.getUnits().getMonsterList());
			params.put("moved", room.hasMoved());
			params.put("moving", room.isMoving());
			
			WSAction action = new WSAction("gamestarted",params);
			
			room.getManager().sendMessage(action, player.getUser());
		}	
	}
	
	public void broadcastEndCombat(WSUser user) {
		WSActionParams params = new WSActionParams();

		WSAction action = new WSAction("endcombat",params);
		
		if(user == null) {
			room.broadcastAction(action);
		} else {
			room.getManager().sendMessage(action, user);
		}
	}
	
	public void beginHeroSelection() {
		
		WSActionParams params = new WSActionParams();
		WSAction action = new WSAction("heroselectionstart",params);
		
		params.put("playerindex", this.room.getPlayerTurnIndex());
		
		this.room.broadcastAction(action);
	}
	
	public void requestMoveState(Player player,Integer maxdistance) {
		//TODO if from spell do nto consume the walk state
		this.room.setMoving(true);
		
		if(maxdistance == -1) {
			//Get movement value from the collection
			Integer value = player.getMovementValues().remove(0);
			//player.setMovement(value + player.getBonusMovement());
			player.setStepLeft(value + player.getBonusMovement());
			//discard the value in the graveyard
			player.getMovementDiscard().add(value);
			//the the collection empty shuffle the graveyard back in the collection
			if(player.getMovementValues().size() == 0) {
				player.getMovementValues().addAll(player.getMovementDiscard());
				player.getMovementDiscard().clear();
				Collections.shuffle(player.getMovementValues());
			}
		} else {
			player.setStepLeft(maxdistance);
		}
		
		WSActionParams params = new WSActionParams();
		params.put("stepleft", player.getStepLeft());					
		params.put("moving", true);
		params.put("moved", false);
		
		WSAction action = new WSAction("onMoveState",params);
		this.room.getManager().sendMessage(action, player.getUser());	
	}
	
	public void requestMonsterMoveState(Player player) {
		Monster monster = this.room.getControlledMonster().getMonster();
		
		monster.setStepLeft(monster.getMaxMovement());
		
		WSActionParams params = new WSActionParams();
		params.put("id",monster.getUuid());
		params.put("movement", monster.getStepLeft());
		params.put("moving", true);
		params.put("movementCallback", "MonsterMovementRequest");
		params.put("finishCallback", "FinishMonsterMovementState");
		WSAction action = new WSAction("onMonsterMoveState",params);
		this.room.getManager().sendMessage(action, player.getUser());
	}
	
	public void updateCurrentPlayerControls() {
		
		WSActionParams params = new WSActionParams();
		WSAction action = new WSAction("updatecontrols",params);
							
		GameControls controls = this.room.getControls();
		
		controls.refresh();
			
		params.put("controlingmonster", controls.isControlingmonster());
		params.put("monstercanmove", controls.isMonstercanmove());
		params.put("monstercanattack", controls.isMonstercanattack());
		params.put("monstercanskip", controls.isMonstercanskip());
		
		params.put("canmove", controls.isCanmove());
		params.put("canattack", controls.isCanattack());
		params.put("candrink", controls.isCandrink());
		params.put("candropcoin", controls.isCandropcoin());
		params.put("canopenchest", controls.isCanopenchest());
		params.put("cancastspell", controls.isCancastspell());
		params.put("cancastnovicefree", controls.isCancastnovicefree());		
		params.put("cancasefreespell", controls.isCancastfreespell());		
		params.put("canvisitmerchant", controls.isCanvisitmerchant());
		params.put("cancontrolmonster", controls.isCancontrolmonster());		
		params.put("canendturn", controls.isCanendturn());
		
		this.room.broadcastAction(action);
	}
}
