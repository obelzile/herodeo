package com.khaosstar.herodeo.model.game.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.entites.Cell;
import com.khaosstar.herodeo.model.game.entites.Cell.CellType;
import com.khaosstar.herodeo.model.game.entites.Monster;
import com.khaosstar.herodeo.model.game.entites.MonsterType;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Position;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.model.game.services.SpellEngine;

public class UnitCollection implements Iterable<Unit>, List<Unit>  {
	private List<Unit> units;
	
	private GameRoom room;
	
	private Integer playerCount = 0;
	private Integer monsterCount = 0;
	
	public UnitCollection(GameRoom room) {
		
		this.room = room;
		this.units = new ArrayList<Unit>();
	}
	
	public Integer getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(Integer playerCount) {
		this.playerCount = playerCount;
	}

	public Integer getMonsterCount() {
		return monsterCount;
	}

	public void setMonsterCount(Integer monsterCount) {
		this.monsterCount = monsterCount;
	}

	@Override
	public Iterator<Unit> iterator() {
		return units.iterator();
	}
	
	@Override
	public boolean add(Unit e) {
		if(e instanceof Player) 
			playerCount++;
		if(e instanceof Monster)
			monsterCount++;
		return units.add(e);
	}

	@Override
	public void add(int index, Unit element) {
		if(element instanceof Player) 
			playerCount++;
		if(element instanceof Monster)
			monsterCount++;
		units.add(index,element);		
	}

	@Override
	public boolean addAll(Collection<? extends Unit> c) {
		for(Object o : c) {
			if(o instanceof Player) 
				playerCount++;
			if(o instanceof Monster)
				monsterCount++;
		}
		return units.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends Unit> c) {
		for(Object o : c) {
			if(o instanceof Player) 
				playerCount++;
			if(o instanceof Monster)
				monsterCount++;
		}
		return units.addAll(index,c);
	}

	@Override
	public void clear() {
		playerCount = 0;
		monsterCount = 0;
		units.clear();		
	}

	@Override
	public boolean contains(Object o) {
		return units.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return units.containsAll(c);
	}

	@Override
	public Unit get(int index) {
		return units.get(index);
	}

	@Override
	public int indexOf(Object o) {
		return units.indexOf(o);
	}

	@Override
	public boolean isEmpty() {
		return units.isEmpty();
	}

	@Override
	public int lastIndexOf(Object o) {
		return units.lastIndexOf(o);
	}

	@Override
	public ListIterator<Unit> listIterator() {
		return units.listIterator();
	}

	@Override
	public ListIterator<Unit> listIterator(int index) {
		return units.listIterator(index);
	}

	@Override
	public boolean remove(Object o) {
		boolean result = units.remove(o);
		if(result) {
			if(o instanceof Player) 
				playerCount--;
			if(o instanceof Monster)
				monsterCount--;			
		}
		
		return result;
	}

	@Override
	public Unit remove(int index) {
		Unit u = units.remove(index);
		if(u != null) {
			if(u instanceof Player) 
				playerCount--;
			if(u instanceof Monster)
				monsterCount--;
		}
		
		return units.remove(index);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		for(Object o : c) {
			if(o instanceof Player) 
				playerCount--;
			if(o instanceof Monster)
				monsterCount--;
		}
		
		return units.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		
		//recount 
		return units.retainAll(c);
	}

	@Override
	public Unit set(int index, Unit element) {
		//recount
		return units.set(index, element);
	}

	@Override
	public int size() {
		return units.size();
	}

	@Override
	public List<Unit> subList(int fromIndex, int toIndex) {
		return units.subList(fromIndex, toIndex);
	}

	@Override
	public Object[] toArray() {
		return units.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return units.toArray(a);
	}

	public Unit getUnit(String uuid) {
		for(Unit u : units) {
			if(u.getUuid().equals(uuid)) {
				return u;
			}
		}
		return null;
	}
	
	
	
	public Unit getUnit(Position position) {
		for(Unit u : units) {
			if(u.getPosition().equals(position)) {
				return u;
			}
		}
		return null;
	}
	
	public List<Position> getUnitsPositionInRange(Unit unit,Integer range) {
		List<Position> result = new ArrayList<Position>();
		
		for(Unit u : units) {			
			if(!u.equals(unit)) {
				Integer xDelta = Math.abs(u.getPosition().getX() - unit.getPosition().getX());
				Integer yDelta = Math.abs(u.getPosition().getY() - unit.getPosition().getY());
				//TODO add LOS
				if((xDelta <= range && yDelta == 0) || (yDelta <= range && xDelta == 0)) {
					result.add(u.getPosition());
				}	
			}
		}

		return result;
	}
	
	public Unit getRangeAttackUnit(Unit unit, Integer direction) {
		int directionX = 0;
		int directionY = 0;
		
		switch(direction) {
			case 0: //north
				directionY = -1;
				break;
			case 1: // east
				directionX = 1;
				break;
			case 2: //south
				directionY = 1;
				break;
			case 3: //west
				directionX = -1;
				break;
		}
		
		Position position = (Position) unit.getPosition().clone();
		Cell cell = null;
		position.add(directionX, directionY);
		while((cell = this.room.getBoard().getCell(position)) != null && !cell.hasLayer(CellType.WALL)) {	
			Unit u = getUnit(position);
			if(u != null && !u.getEffects().contains(SpellEngine.enumEffect.RANGE_IMMUNITY))
				return u;
				
			position.add(directionX, directionY);
		}
			
		
		return null;
	}
		
	public List<Position> getRangeAttackPositions(Unit unit) {
		List<Position> positions = new ArrayList<Position>();
		
		for(int i=0;i<4;i++) {
			Unit u = getRangeAttackUnit(unit,i);
			if(u != null)
				positions.add(u.getPosition());
		}
		
		return positions;
	}
	
	public List<Unit> getUnitsInRange(Position position,Integer range) {
		List<Unit> result = new ArrayList<Unit>();
		
		for(Unit u : units) {
			Integer xDelta = Math.abs(u.getPosition().getX() - position.getX());
			Integer yDelta = Math.abs(u.getPosition().getY() - position.getY());
			//TODO add LOS
			if((xDelta <= range && yDelta == 0) || (yDelta <= range && xDelta == 0)) {
				result.add(u);
			}	
		}

		return result;
	}
	
	public List<Position> getOpponentsPosition(String playeruuid) {
		List<Position> result = new ArrayList<Position>();
		for(Unit u : units) {
			if(u instanceof Player && !u.getUuid().equals(playeruuid)) {
				result.add(u.getPosition());
			}
		}
		return result;
	}
	
	public List<Unit> getOpponents(String playeruuid) {
		List<Unit> result = new ArrayList<Unit>();
		//filterUnitType == Player / Monster
		for(Unit u : units) {
			if(u instanceof Player && !u.getUuid().equals(playeruuid)) {
				result.add(u);
			}
		}
		return result;
	}	
	
	public List<Player> getPlayerList() {
		List<Player> players = new ArrayList<Player>();
		
		for(Unit u : units) {
			if(u instanceof Player)
				players.add((Player)u);
		}
		
		return players;
	}
	
	public List<Monster> getMonsterList() {
		List<Monster> monsters = new ArrayList<Monster>();
		
		for(Unit u : units) {
			if(u instanceof Monster)
				monsters.add((Monster)u);
		}
		
		return monsters;
	}
	
	public List<Position> getMonsterPositionList() {
		List<Position> positions = new ArrayList<Position>();
		
		for(Unit u : units) {
			if(u instanceof Monster)
				positions.add(u.getPosition());
		}
		
		return positions;
	}
	
	public Player getPlayerIndex(Integer index) {
		Player player = null;
		if(index >= 0 && index < playerCount) {
			for(Unit u : units) {
				if(u instanceof Player && ((Player)u).getIndex() == index) {
					player = (Player)u;
					break;
				}
			}
		}
		return player;
	}
	
	public Player getPreviousPlayer(Integer index) {
		if(index == 0) {
			index = playerCount-1;
		} else {
			index = index-1;
		}

		return getPlayerIndex(index);
	}

	public Player getNextPlayer(Integer index) {
		if(index >= playerCount-1) {
			index = 0;
		} else {
			index = index+1;
		}

		return getPlayerIndex(index);
	}
	
	public boolean reconnectPlayer(WSUser user) {
		boolean result = false;
		for(Unit u : units) {				
			if(u.getUuid().equals(user.getUUID())) {
				u.setUser(user);
				room.getRoomMembers().put(user.getUUID(), user);
				user.setChatRoomUUID(room.getUuid());
				result = true;
			}
		}
		return result;
	}
	
	public void setMonstersTypeInt(Integer id) {
		List<MonsterType> types = this.room.getMutableMonsters();
		
		for(MonsterType mt : types) {
			if(mt.getId() == id) {
				setMonstersType(mt);
				break;
			}				
		}
	}
	
	public void setMonstersType(MonsterType type) {
		this.room.setCurrentMonsterType(type);
		
		for(Unit u : units) {
			if(u instanceof Monster) {
				((Monster)u).setType(type);
			}
		}
	}	
	
	public List<Position> getPlayersPositionInRange(Position position,Integer range) {
		List<Position> result = new ArrayList<Position>();
						
		List<Player> players = getPlayerList();
		
		for(Player player : players) {
				Integer xDelta = Math.abs(player.getPosition().getX() - position.getX());
				Integer yDelta = Math.abs(player.getPosition().getY() - position.getY());
				//TODO add LOS
				if((xDelta <= range && yDelta == 0) || (yDelta <= range && xDelta == 0)) {
					result.add(player.getPosition());
				}			
		}
		return result;
	}
	
	public List<Player> getPlayersInRange(Position position,Integer range) {
		List<Player> result = new ArrayList<Player>();
		
		List<Player> players = getPlayerList();
		
		for(Player player : players) {
				Integer xDelta = Math.abs(player.getPosition().getX() - position.getX());
				Integer yDelta = Math.abs(player.getPosition().getY() - position.getY());
				//TODO add LOS
				if((xDelta <= range && yDelta == 0) || (yDelta <= range && xDelta == 0)) {
					result.add(player);
				}			
		}
		return result;
	}
	
	public List<Position> getOpponentsPositionInRange(Player from,Integer range) {
		List<Position> result = new ArrayList<Position>();
		
		List<Player> players = getPlayerList();
		
		for(Player player : players) {
			if(player.getIndex() != from.getIndex()) {
				Integer xDelta = Math.abs(player.getPosition().getX() - from.getPosition().getX());
				Integer yDelta = Math.abs(player.getPosition().getY() - from.getPosition().getY());
				//TODO add LOS
				if((xDelta <= range && yDelta == 0) || (yDelta <= range && xDelta == 0)) {
					result.add(player.getPosition());
				}			
			}
		}
		return result;
	}

	
	public List<Player> getOpponentsInRange(Player from,Integer range) {
		List<Player> result = new ArrayList<Player>();
		
		List<Player> players = getPlayerList();
		
		for(Player player : players) {
			if(player.getIndex() != from.getIndex()) {
				Integer xDelta = Math.abs(player.getPosition().getX() - from.getPosition().getX());
				Integer yDelta = Math.abs(player.getPosition().getY() - from.getPosition().getY());
				//TODO add LOS
				if((xDelta <= range && yDelta == 0) || (yDelta <= range && xDelta == 0)) {
					result.add(player);
				}			
			}
		}
		return result;
	}
	
	public static List<Position> getPositionListFromUnits(List<? extends Unit> units) {
		List<Position> positions = new ArrayList<Position>();
		
		for(Unit unit : units) {
			positions.add(unit.getPosition());
		}
		return positions;
	}
}
