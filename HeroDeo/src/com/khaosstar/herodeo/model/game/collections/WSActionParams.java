package com.khaosstar.herodeo.model.game.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.gson.internal.StringMap;
import com.khaosstar.herodeo.model.game.entites.Position;

import sun.org.mozilla.javascript.internal.Scriptable;

public class WSActionParams implements Map{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HashMap map = new HashMap();
	
	public Position getAsPosition(String key) {
		Position position = null;
		
		if(map.containsKey(key) && map.get(key) instanceof com.google.gson.internal.StringMap) {
			StringMap value = (StringMap) map.get(key);
			if(value.containsKey("x") && value.containsKey("y")) {
				try {
					position = new Position(((Double)value.get("x")).intValue(),((Double)value.get("y")).intValue());
				} catch(Exception ex) {
					int i =0;
				}
			}
		}
		
		return position;
	}
		
	public String getAsString(String key) {
		return (String) map.get(key);
	}

	public Integer getAsInteger(String key) {
		if(map.containsKey(key)) {
			if(map.get(key).getClass() == Double.class) {
				return ((Double)map.get(key)).intValue();
			} else if(map.get(key).getClass() == String.class) {
				return Integer.parseInt((String)map.get(key));
			} else {
				return (Integer)map.get(key);
			}
		}
		return null;
	}
	
	public Double getAsDouble(String key) {
		return (Double) map.get(key);		
	}

	@Override
	public void clear() {
		map.clear();		
	}

	@Override
	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	@Override
	public Set entrySet() {
		return map.entrySet();
	}

	@Override
	public Object get(Object key) {
		return map.get(key);
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public Set keySet() {
		return map.keySet();
	}

	@Override
	public Object put(Object key, Object value) {
		return map.put(key, value);
	}

	@Override
	public void putAll(Map m) {
		map.putAll(m);		
	}

	@Override
	public Object remove(Object key) {
		return map.remove(key);
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public Collection values() {
		return map.values();
	}
}
