package com.khaosstar.herodeo.model.game.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.model.game.entites.MonsterType;

@XmlRootElement(name="monsters")
public class MonsterTypeCollection implements Iterable<MonsterType> {

	@Expose private List<MonsterType> monsterTypes;
	private List<MonsterType> mutableMonsterTypes;

	public MonsterTypeCollection() {
		
	}

	public List<MonsterType> getMonsterTypeList() {
		return monsterTypes;
	}

	@XmlElement(name = "monster")
	public void setMonsterTypeList(List<MonsterType> monsterTypes) {
		this.monsterTypes = monsterTypes;
		this.mutableMonsterTypes = new ArrayList<MonsterType>(monsterTypes);
		Iterator<MonsterType> iter = mutableMonsterTypes.iterator();
		while(iter.hasNext()) {
			MonsterType mt = iter.next();
			if(mt.isCanbemutate() == false)
				iter.remove();
		}
	}	
	
	public MonsterType getMonsterType(int id) {		
		for(MonsterType mt : monsterTypes) {
			if(mt.getId() == id) {
				return mt;
			}
		}
		
		return null;
	}
	
	public List<MonsterType> getMutableMonsterTypes() {
		return mutableMonsterTypes;
	}

	public void setMutableMonsterTypes(List<MonsterType> mutableMonsterTypes) {
		this.mutableMonsterTypes = mutableMonsterTypes;
	}

	@Override
	public Iterator<MonsterType> iterator() {
		return monsterTypes.iterator();
	}
}