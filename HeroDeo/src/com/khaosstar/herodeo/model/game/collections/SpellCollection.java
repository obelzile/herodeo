package com.khaosstar.herodeo.model.game.collections;

import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.model.game.entites.Spell;

/**
 * 
 * Collection of Spell
 *
 */
@XmlRootElement(name="spells")
public class SpellCollection implements Iterable<Spell> {

	@Expose private List<Spell> spells;
	
	public SpellCollection() {
		
	}

	public List<Spell> getSpellList() {
		return spells;
	}

	@XmlElement(name = "spell")
	public void setSpellList(List<Spell> value) {
		this.spells = value;
	}
	
	public Spell getSpell(int id) {
		
		for(Spell spell : spells) {
			if(spell.getId() == id) {
				return spell;
			}
		}
		
		return null;
	}

	@Override
	public Iterator<Spell> iterator() {
		return spells.iterator();
	}
}
