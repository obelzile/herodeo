package com.khaosstar.herodeo.model.game.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.model.game.entites.Item;

/**
 * 
 * Collection of Item
 *
 */
@XmlRootElement(name="items")
public class ItemCollection implements Iterable<Item> {

	@Expose private List<Item> items;	
	
	public ItemCollection() {
		
	}

	public List<Item> getItemsType(int type) {
		List<Item> equipments = new ArrayList<Item>();
		
		for(Item item : items) {
			if(item.getType() == type) {
				equipments.add(item);
			}
		}
		return equipments;
	}
	
	public List<Item> getItems() {
		return items;
	}

	@XmlElement(name = "item")
	public void setItems(List<Item> items) {
		this.items = items;
	}
		
	public Item getItem(int id) {
		for(Item i : items) {
			if(i.getId() == id) {
				return i;
			}
		}
		return null;
	}

	@Override
	public Iterator<Item> iterator() {
		return items.iterator();
	}	
}
