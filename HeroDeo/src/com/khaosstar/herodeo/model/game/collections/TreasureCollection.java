package com.khaosstar.herodeo.model.game.collections;

import java.util.ArrayList;
import java.util.Iterator;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.entites.Cell;
import com.khaosstar.herodeo.model.game.entites.Cell.CellType;
import com.khaosstar.herodeo.model.game.entites.Position;

public class TreasureCollection implements Iterable<Position> {

	private ArrayList<Position> treasures;
	private GameRoom game;
	
	public TreasureCollection(GameRoom game) {
		this.treasures = new ArrayList<Position>();
		this.game = game;
	}

	public boolean addTreasure(Cell cell) {
		boolean result = false;

		if(!treasures.contains(cell.getPosition())) {		
			if(game.getBoard().hasCellType(cell.getPosition(), CellType.REDENERGY) || game.getBoard().hasCellType(cell.getPosition(), CellType.BLUEENERGY)) {
				treasures.add((Position) cell.getPosition().clone());
				game.getBoard().getCell(cell.getPosition()).addLayer(CellType.CHEST);
				result = true;
			}
		}
		
		return result;
	}
	
	public boolean removeTreasure(Position position) {
		boolean result = false;
		
		if(treasures.contains(position)) {
			treasures.remove(position);
			//TODO give gold or equipement
		}
		
		return result;
	}
	
	public ArrayList<Position> getTreasures() {
		return treasures;
	}

	public void setTreasures(ArrayList<Position> treasures) {
		this.treasures = treasures;
	}

	public void broadcastTreasures() {
		WSActionParams params = new WSActionParams();
		
		params.put("treasures", this.treasures);
		
		WSAction action = new WSAction("treasuresupdate",params);
		
		game.broadcastAction(action);
	}

	@Override
	public Iterator<Position> iterator() {
		return this.treasures.iterator();
	}
	
}
