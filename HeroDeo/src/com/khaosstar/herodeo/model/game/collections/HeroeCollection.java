package com.khaosstar.herodeo.model.game.collections;

import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.model.game.entites.Hero;

/**
 * 
 * Collection of Hero
 *
 */
@XmlRootElement(name="heroes")
public class HeroeCollection implements Iterable<Hero> {

	@Expose private List<Hero> heroes;

	public HeroeCollection() {
		
	}

	public List<Hero> getHeroesList() {
		return heroes;
	}

	@XmlElement(name = "hero")
	public void setHeroesList(List<Hero> heroesList) {
		this.heroes = heroesList;
	}	
	
	public Hero getHero(int id) {
		
		for(Hero hero : heroes) {
			if(hero.getId() == id) {
				return hero;
			}
		}
		
		return null;
	}

	@Override
	public Iterator<Hero> iterator() {
		return heroes.iterator();
	}
}
