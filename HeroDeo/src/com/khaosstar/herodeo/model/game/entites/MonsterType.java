package com.khaosstar.herodeo.model.game.entites;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import com.khaosstar.herodeo.model.game.services.SpellEngine;

public class MonsterType {
		
	private Integer id;
	private String name;
	private String description;
	private Integer maxPV;
	private Integer strength;
	private Integer protection;
	private Integer maxmovement;
	private Integer price;
	private boolean canbemutate;
	private boolean ismonster;
	private boolean candodge;
	private boolean respawn;
		
	private ArrayList<SpellEngine.enumEffect> effects;
	
	public MonsterType() {
		
	}

	public Integer getId() {
		return id;
	}

	@XmlAttribute(name = "id")
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	@XmlElement(name = "name")
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	@XmlElement(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMaxPV() {
		return maxPV;
	}

	@XmlElement(name = "maxpv")
	public void setMaxPV(Integer maxPV) {
		this.maxPV = maxPV;
	}

	public Integer getStrength() {
		return strength;
	}

	@XmlElement(name = "strength")
	public void setStrength(Integer strength) {
		this.strength = strength;
	}

	public Integer getProtection() {
		return protection;
	}

	@XmlElement(name = "protection")
	public void setProtection(Integer protection) {
		this.protection = protection;
	}

	public Integer getMaxmovement() {
		return maxmovement;
	}

	@XmlElement(name = "maxmovement")
	public void setMaxmovement(Integer maxmovement) {
		this.maxmovement = maxmovement;
	}

	public Integer getPrice() {
		return price;
	}

	@XmlElement(name = "price")
	public void setPrice(Integer price) {
		this.price = price;
	}

	public boolean isCanbemutate() {
		return canbemutate;
	}

	@XmlElement(name = "canbemutate")
	public void setCanbemutate(boolean canbemutate) {
		this.canbemutate = canbemutate;
	}

	public boolean isIsmonster() {
		return ismonster;
	}

	@XmlElement(name = "ismonster")
	public void setIsmonster(boolean ismonster) {
		this.ismonster = ismonster;
	}
	
	public boolean canDodge() {
		return candodge;
	}

	@XmlElement(name = "candodge")
	public void setCandodge(boolean candodge) {
		this.candodge = candodge;
	}

	public boolean canRespawn() {
		return respawn;
	}

	@XmlElement(name = "respawn")
	public void setRespawn(boolean respawn) {
		this.respawn = respawn;
	}

	public ArrayList<SpellEngine.enumEffect> getEffects() {
		if(effects == null)
			effects = new ArrayList<SpellEngine.enumEffect>();
		return effects;
	}

	@XmlElementWrapper( name="effects" )
	@XmlElement(name = "effect")
	public void setEffects(ArrayList<SpellEngine.enumEffect> effects) {
		this.effects = effects;
	}
	
}
