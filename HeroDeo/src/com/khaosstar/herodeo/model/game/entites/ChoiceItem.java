package com.khaosstar.herodeo.model.game.entites;

import com.google.gson.annotations.Expose;

/**
 * 
 * Entity used to hold information about a choice requested to the client
 *
 */
public class ChoiceItem {
	@Expose private String text;
	@Expose private String value;
	@Expose private boolean enabled;
	
	public ChoiceItem(String text, String value) {
		this.text = text;
		this.value = value;
		this.enabled = true;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}	
}
