package com.khaosstar.herodeo.model.game.entites;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.khaosstar.herodeo.model.entities.WSUser;

/** 
 * 
 * Entity representing an instance of spellcast. holds information to the current state of the spell cast.
 * Allow async spell cast to request more information to the user before resolving the spellcast
 *
 */
public class SpellCast {

	private WSUser user;
	private Player caster;
	private Spell spell;
	private Integer spellCost;
	private Unit target = null;
	private Position targetPosition = null;
	private List<Unit> targets = null;
	private List<Position> targetsGround = null;
	private List<Object> choices = null;
	private Integer choice;
	private boolean canceled;
	private boolean casting;
	private String message;
	private Integer step;
	private Map<String,Object> data;
	
	public SpellCast(WSUser user,Player caster, Spell spell) {
		this.user = user;
		this.caster = caster;
		this.step = 0;
		this.spell = spell;
		this.spellCost= spell.getCost(); 
		this.canceled = false;
		this.choices = new ArrayList<Object>();
		this.data = new HashMap<String,Object>();
		this.casting = true;
	}
	
	public Object getDataValue(String key) {
		return this.data.get(key);
	}
	
	public void setDataValue(String key, Object value) {
		this.data.put(key,value);
	}
	
	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public Unit getTarget() {
		return target;
	}

	public void setTarget(Unit target) {
		this.target = target;
	}

	public Position getTargetPosition() {
		return targetPosition;
	}

	public void setTargetPosition(Position targetPosition) {
		this.targetPosition = targetPosition;
	}

	public List<Object> getChoices() {
		return choices;
	}

	public void setChoices(List<Object> choices) {
		this.choices = choices;
	}
	
	public void addChoice(Integer choice) {
		this.choices.add(choice);
	}

	public Player getCaster() {
		return caster;
	}

	public void setCaster(Player caster) {
		this.caster = caster;
	}

	public WSUser getUser() {
		return user;
	}

	public void setUser(WSUser user) {
		this.user = user;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getSpellCost() {
		return spellCost;
	}

	public void setSpellCost(Integer spellCost) {
		this.spellCost = spellCost;
	}

	public Spell getSpell() {
		return spell;
	}

	public void setSpell(Spell spell) {
		this.spell = spell;
	}

	public List<? extends Unit> getTargets() {
		if(targets == null)
			targets = new ArrayList<Unit>();
		return targets;
		
	}

	public void setTargets(List<Unit> targets) {
		this.targets = targets;
	}

	public void addTarget(Unit target) {
		if(this.targets == null) {
			this.targets = new ArrayList<Unit>();
		}
		
		this.targets.add(target);
	}
	
	public List<Position> getTargetsPosition() {
		return targetsGround;
	}

	public void setTargetsPosition(List<Position> targetsGround) {
		this.targetsGround = targetsGround;
	}

	public void addTargetPosition(Position target) {
		if(this.targetsGround == null) {
			this.targetsGround = new ArrayList<Position>();
		}
		
		this.targetsGround.add(target);
	}
	
	public Integer getChoice() {
		return choice;
	}

	public void setChoice(Integer choice) {
		this.choice = choice;
	}
	
	public void setChoice(String choice) {
		try {
			this.choice = Integer.parseInt(choice);
		} catch(NumberFormatException ex) {
			
		}
	}

	public boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}

	public boolean isCasting() {
		return casting;
	}

	public void setCasting(boolean casting) {
		this.casting = casting;
	}
	
}
