package com.khaosstar.herodeo.model.game.entites;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.model.game.services.SpellEngine;

/**
 * 
 * Buff entity
 * 
 * result of an active spell
 *
 */
public class Buff {
	SpellCast spellcast;
	@Expose Spell spell;
	@Expose Integer duration;
	@Expose Integer choice;
	@Expose Integer value;

	public Buff(SpellCast spellcast,Integer duration) {
		this.spellcast = spellcast;
		this.duration = duration;
		this.spell= spellcast.getSpell(); 
	}
	
	public SpellCast getSpellcast() {
		return spellcast;
	}

	public void setSpellcast(SpellCast spellcast) {
		this.spellcast = spellcast;
	}

	public void update() {
		
		if(!spellcast.getCaster().getEffects().contains(SpellEngine.enumEffect.STOP_TIME)) {
			if(this.duration == 0) {
				//timeout();
			} else if(this.duration > 0)
				this.duration--;
		}
	}

	public Spell getSpell() {
		return spell;
	}

	public void setSpell(Spell spell) {
		this.spell = spell;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getChoice() {
		return choice;
	}

	public void setChoice(Integer choice) {
		this.choice = choice;
	}	
		
	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
}
