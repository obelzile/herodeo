package com.khaosstar.herodeo.model.game.entites;

import com.google.gson.annotations.Expose;

public class Position implements Cloneable {

	@Expose private Integer x;
	@Expose private Integer y;
	
	public Position(Integer x , Integer y) {
		this.x = x;
		this.y = y;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}
	
	public void setPosition(Integer x,Integer y) {
		this.x = x;
		this.y = y;
	}
	
	public void add(Integer x,Integer y) {
		this.x += x;
		this.y += y;
	}
	
	@Override
	public boolean equals(Object obj) {
		 if(obj == this)
			 return true;
		 if(obj == null || obj.getClass() != this.getClass())
			 return false;
		 
		 Position position = (Position) obj;
		 return this.x == position.getX() && this.y == position.getY();
	}
	
	 @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((x == null) ? 0 : x.hashCode());
		result = prime * result
				+ ((y == null) ? 0 : y.hashCode());
		return result;
	}
	 
	public Object clone() {
		Object o = null;
		try {
			// On r�cup�re l'instance � renvoyer par l'appel de la 
			// m�thode super.clone()
			o = super.clone();
		} catch(CloneNotSupportedException cnse) {
			// Ne devrait jamais arriver car nous impl�mentons 
			// l'interface Cloneable
			cnse.printStackTrace(System.err);
		}
		// on renvoie le clone
		return o;
	}
}
