package com.khaosstar.herodeo.model.game.entites;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.holders.HolderGameAsset;
import com.khaosstar.herodeo.model.entities.WSUser;

/**
 * 
 * Player entity
 *
 */
public class Player extends Unit {
	@Expose private Integer index;	
	@Expose private boolean ready;	
	@Expose private Integer bonusMaxPV;
	@Expose private Integer currentPM; // Magic Point
	@Expose private Integer currentPO; // Gold Piece
	@Expose private Integer bonusStrength;
	@Expose private Integer bonusProtection;
	@Expose private Integer bonusMovement;
	@Expose private Integer movement;
	@Expose private Integer sacredStone;	
	@Expose private Position startPosition;

	@Expose private SpellBook spellBook;
	
	private HashMap<String,String> onAttackScripts;
	private HashMap<String,String> onKillScripts;
	private HashMap<String,String> onCastScripts;
	private HashMap<String,String> onPreCastScripts;
	private HashMap<String,String> onPreCombatScripts;
	private HashMap<String,String> onReceivingSpellScripts;
		
	private ArrayList<Integer> movementValues;
	private ArrayList<Integer> movementDiscard;	
	
	@Expose ArrayList<Item> equipements;
	@Expose ArrayList<Item> reliques;
	
	private SpellCast currentSpell;
		
	private String lastuseruuid;
	
	public Player(WSUser u) {
		
		this.spellBook = new SpellBook();
		
		this.equipements= new ArrayList<Item>();
		this.reliques= new ArrayList<Item>();
			
		
		onAttackScripts = new HashMap<String,String>();
		onKillScripts = new HashMap<String,String>();
		onCastScripts = new HashMap<String,String>();
		onPreCastScripts = new HashMap<String,String>();
		onReceivingSpellScripts = new HashMap<String,String>();
				
		super.setName(u.getNickname());
		this.setUuid(u.getUUID());
		this.setUser(u);
		this.ready = false;
		this.setId(-1);
		this.currentPM = 100;
		this.currentPO= 100; 
		super.setMaxPV(0);
		this.bonusStrength = 0;
		this.setStrength(0);
		this.bonusProtection = 0;
		this.setProtection(0);
		this.setMaxMovement(0);
		this.bonusMovement = 0;
		this.movement = 0;
		this.sacredStone = 1;  
		this.setPosition(null);			
	}
	
	public List<Item> getInventory() {
		List<Item> items = new ArrayList<Item>();
		
		items.addAll(equipements);
		items.addAll(reliques);
		
		return items;
	}
	
	public void removeItem(Item item) {
		equipements.remove(item);
		reliques.remove(item);
	}
	
	public void filterEquipments(List<Item> items) {
		Iterator<Item> iter = items.iterator();
		while(iter.hasNext()) {
			Item item = iter.next();
			if(equipements.contains(item))
				iter.remove();
		}
	}
	
	public SpellCast getCurrentSpell() {
		return currentSpell;
	}

	public void setCurrentSpell(SpellCast currentSpell) {
		this.currentSpell = currentSpell;
	}
	
	public HashMap<String, String> getOnPreCombatScripts() {
		return onPreCombatScripts;
	}

	public void setOnPreCombatScripts(HashMap<String, String> onPreCombatScripts) {
		this.onPreCombatScripts = onPreCombatScripts;
	}

	public HashMap<String, String> getOnPreCastScripts() {
		return onPreCastScripts;
	}

	public void setOnPreCastScripts(HashMap<String, String> onPreCastScripts) {
		this.onPreCastScripts = onPreCastScripts;
	}

	public HashMap<String, String> getOnAttackScripts() {
		return onAttackScripts;
	}

	public void setOnAttackScripts(HashMap<String, String> onAttackScripts) {
		this.onAttackScripts = onAttackScripts;
	}

	public HashMap<String, String> getOnKillScripts() {
		return onKillScripts;
	}

	public void setOnKillScripts(HashMap<String, String> onKillScripts) {
		this.onKillScripts = onKillScripts;
	}

	public HashMap<String, String> getOnCastScripts() {
		return onCastScripts;
	}

	public void setOnCastScripts(HashMap<String, String> onCastScripts) {
		this.onCastScripts = onCastScripts;
	}

	public HashMap<String, String> getOnReceivingSpellScripts() {
		return onReceivingSpellScripts;
	}

	public void setOnReceivingSpellScripts(
			HashMap<String, String> onReceivingSpellScripts) {
		this.onReceivingSpellScripts = onReceivingSpellScripts;
	}	
		
	public SpellBook getSpellBook() {
		return spellBook;
	}

	public void setSpellBook(SpellBook spellBook) {
		this.spellBook = spellBook;
	}

	public ArrayList<Item> getEquipements() {
		return equipements;
	}

	public void setEquipements(ArrayList<Item> equipements) {
		this.equipements = equipements;
	}

	public ArrayList<Item> getReliques() {
		return reliques;
	}

	public void setReliques(ArrayList<Item> reliques) {
		this.reliques = reliques;
	}

	public ArrayList<Integer> getMovementDiscard() {
		return movementDiscard;
	}

	public void setMovementDiscard(ArrayList<Integer> movementDiscard) {
		this.movementDiscard = movementDiscard;
	}

	public ArrayList<Integer> getMovementValues() {
		return movementValues;
	}

	public void setMovementValues(ArrayList<Integer> movementValues) {
		this.movementValues = movementValues;
	}
	
	//************* STRENGTH
	
	public Integer getBonusStrength() {
		return bonusStrength;
	}

	public void setBonusStrength(Integer bonusStrength) {
		this.bonusStrength = bonusStrength;
	}

	public void addBonusStrength(Integer bonus) {
		this.bonusStrength += bonus;
	}

	public void removeBonusStrength(Integer bonus) {
		this.bonusStrength -= bonus;		
	}
		
	//************* PROTECTION
	
	public Integer getBonusProtection() {
		return bonusProtection;
	}

	public void setBonusProtection(Integer bonusProtection) {
		this.bonusProtection = bonusProtection;
	}

	public void addBonusProtection(Integer bonus) {
		this.bonusProtection += bonus;
	}
	
	public void removeBonusProtection(Integer bonus) {
		this.bonusProtection -= bonus;
	}
	
	
	//************* BONUS MOVEMENT
		
	public void addBonusMovement(Integer bonus) {
		this.bonusMovement += bonus;
	}
	
	public void removeBonusMovement(Integer bonus) {
		this.bonusMovement -= bonus;
	}
	
	public Integer getBonusMovement() {
		return bonusMovement;
	}

	public void setBonusMovement(int bonusMovement) {
		this.bonusMovement = bonusMovement;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getHeroId() {
		return this.getId();
	}

	public void setHeroId(Integer heroId) {
		this.setId(heroId);
		
		Hero hero = HolderGameAsset.getHeroManagerInstance().getHero(this.getId());
		if(hero == null)
			this.setId(-1);
		else {
			this.setMaxPV(hero.getMaxPV());
			this.setCurrentPV(this.getMaxPV());
			this.setStrength(hero.getStrength());
			this.setProtection(hero.getProtection());
			
			this.spellBook.setPersonalSpells(HolderGameAsset.getSpellManagerInstance().getHeroSpells(this.getId()));			
			this.spellBook.setDeckSpells(HolderGameAsset.getSpellManagerInstance().getSpells().getSpellList());
			this.spellBook.getDeckSpells().removeAll(this.spellBook.getPersonalSpells());
								
			this.spellBook.shuffleDeck();
			
			initMovementValues();
		}				
	}
	
	private void initMovementValues() {
		this.movementValues = new ArrayList<Integer>();
		this.movementDiscard = new ArrayList<Integer>();
		
		Hero hero = HolderGameAsset.getHeroManagerInstance().getHero(this.getId());
		
		for(Hero h : HolderGameAsset.getHeroManagerInstance().getHeroes().getHeroesList()) {
			if(h.getId() == hero.getId())
				continue;
			if(hero.getMovementType().equals("Lent")) {
				if(!h.getMovementType().equals("Rapide")) {
					this.movementValues.add(h.getMovement());
				}
			} else if(hero.getMovementType().equals("Rapide")) {
				if(!h.getMovementType().equals("Lent")) {
					this.movementValues.add(h.getMovement());
				}				
			} else {
				this.movementValues.add(h.getMovement());
			}
		}
				
		Collections.shuffle(this.movementValues);
	}
	
	public boolean isSpellActive(Spell spell) {
		return this.getSpellBook().isSpellActive(spell);
	}
		
	public Position getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(Position startPosition) {
		this.startPosition = startPosition;
	}

	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}

	public String getLastuseruuid() {
		return lastuseruuid;
	}

	public void setLastuseruuid(String lastuseruuid) {
		this.lastuseruuid = lastuseruuid;
	}
	
	//************* PM

	public Integer getCurrentPM() {
		return currentPM;
	}
	
	public void setCurrentPM(Integer currentPM) {
		this.currentPM = currentPM;
	}

	public void addPM(Integer pm) {
		this.currentPM += pm;
	}
	
	public void removePM(Integer pm) {
		this.currentPM -= pm;
		if(this.currentPM < 0)
			this.currentPM = 0;
	}
	
	public Integer getMovement() {
		return movement;
	}

	public void setMovement(Integer movement) {
		this.movement = movement;
	}
	
	public Integer getBonusMaxPV() {
		return bonusMaxPV;
	}

	public void setBonusMaxPV(Integer bonusMaxPV) {
		this.bonusMaxPV = bonusMaxPV;
	}

	public Integer getCurrentPO() {
		return currentPO;
	}

	public void setCurrentPO(Integer currentPO) {
		this.currentPO = currentPO;
	}
	
	public void addPM(int value) {
		this.currentPM += value;
		if(this.currentPM < 0)
			this.currentPM = 0;
	}
	
	public void addPO(int value) {
		this.currentPO += value;
	}
	
	public void removePO(int value) {
		this.currentPO -= value;
		if(this.currentPO < 0) {
			this.currentPO = 0;
		}
	}
 
	public Integer getSacredStone() {
		return sacredStone;
	}

	public void setSacredStone(Integer sacredStone) {
		
		if(this.sacredStone - 1 > 1) {
			this.bonusStrength -= this.sacredStone - 1;
			this.bonusProtection -= this.sacredStone - 1;
			this.bonusMovement -= this.sacredStone - 1;
		}
		
		if(sacredStone - 1 > 1) {
			this.bonusStrength += sacredStone - 1;
			this.bonusProtection += sacredStone - 1;
			this.bonusMovement += sacredStone - 1;
		}
		
		this.sacredStone = sacredStone;		
	}

	public HashMap<String,Object> getPlayerForJson() {
		HashMap<String,Object> params = new HashMap<String,Object>();
		
		params.put("user",this.getUser().getNickname());
		params.put("id", this.getId());
		params.put("currentPV", this.getCurrentPV());
				
		return params;
	}

	public boolean canAttack(Player player) {
		//TODO if player == this   add equal override
		//TODO add range attack
		boolean result = false;
		
		Integer x = player.getPosition().getX() - this.getPosition().getX();
		Integer y = player.getPosition().getY() - this.getPosition().getY();
		if((x == 0 && Math.abs(y) == 1) || (y == 0 && Math.abs(x) == 1)) {
			result = true;
		}
		
		return result;
	}

	@Override
	public Integer getTotalStrength() {
		return this.getStrength() + this.getBonusStrength();
	}

	@Override
	public Integer getTotalProtection() {
		return this.getProtection() + this.getBonusProtection();
	}
}
