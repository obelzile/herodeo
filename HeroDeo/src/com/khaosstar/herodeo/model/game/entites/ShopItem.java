package com.khaosstar.herodeo.model.game.entites;

import com.google.gson.annotations.Expose;

/**
 * 
 * Item Entity used in the item shop
 *
 */
public class ShopItem {
	
	@Expose private String id;
	@Expose private Integer type;
	@Expose private String image;
	@Expose private String name;
	@Expose private String description;
	@Expose private Integer cost;
	@Expose private boolean enabled;
	
	public static final int SHOPITEM_EQUIPMENT = 0;
	public static final int SHOPITEM_RELIQUE = 1;
	public static final int SHOPITEM_SCROLL = 2;
	
	public ShopItem() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}		
}
