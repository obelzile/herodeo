package com.khaosstar.herodeo.model.game.entites;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.utility.HeroUtility;

/**
 * 
 * Class that hold the player spells
 * Spells in Hand
 * Spells in the deck
 * Active spell 
 *
 */
public class SpellBook {
	
	@Expose private ArrayList<Spell> personalSpells;
	private ArrayList<Spell> handSpells;
	private ArrayList<Spell> deckSpells;
	@Expose private ArrayList<Buff> activeSpells;
	
	public SpellBook() {
		handSpells = new ArrayList<Spell>();
		activeSpells = new ArrayList<Buff>();
	}

	public ArrayList<Spell> getDeckSpells() {
		return deckSpells;
	}
	
	public void setDeckSpells(List<Spell> list) {
		this.deckSpells = new ArrayList<Spell>(list);		
	}
	
	public ArrayList<Spell> getPersonalSpells() {
		return personalSpells;
	}

	public void setPersonalSpells(ArrayList<Spell> personalSpell) {
		this.personalSpells = new ArrayList<Spell>(personalSpell);
	}

	public ArrayList<Spell> getHandSpells() {
		return handSpells;

	}

	public void setHandSpells(ArrayList<Spell> handSpell) {
		this.handSpells = handSpell;
	}
			
	public ArrayList<Buff> getActiveSpells() {
		return activeSpells;
	}

	public void setActiveSpells(ArrayList<Buff> activeSpells) {
		this.activeSpells = activeSpells;
	}
	
	public void filterAvailable(ArrayList<Spell> spells) {
		Iterator<Spell> iter = spells.iterator();
		while(iter.hasNext()) {
			Spell spell = iter.next();
			if(this.handSpells.contains(spell))
				iter.remove();
		}
	}

	public boolean isSpellActive(Spell spell) {
		return this.activeSpells.contains(spell);
	}
	
	public boolean isPersonalSpell(Spell spell) {
		boolean result = false;
		
		if(this.personalSpells.contains(spell))
			result = true;
		
		return result;
	}
	
	
	public boolean discardSpell(Spell spell) {
		boolean result = false;
		if(this.handSpells.contains(spell)) {
			result = true;
			this.deckSpells.add(spell);
			this.handSpells.remove(spell);
		}
		return result;
	}
	
	/**
	 *  Draw count card fro mthe top of the deck
	 * @param count card to draw
	 * @return true if successful 
	 */
	public boolean drawCard(int count) {
		boolean result = false;
		
		for(int i=0;i<count;i++) {
			if(this.deckSpells.size() > 0) {
				this.handSpells.add(this.deckSpells.get(0));
				this.deckSpells.remove(0);
				result = true;
			} else { // could not draw count of cards
				result = false;
				break;
			}
		}
		return result;
	}
	
	/** 
	 * Search for a spell and draw it
	 * @param spell spell to draw
	 * @return true if successful 
	 */
	public boolean drawSpell(Spell spell) {
		boolean result = false;
		
		if(this.deckSpells.contains(spell)) {
			
			this.deckSpells.remove(spell);
			this.handSpells.add(spell);
			
			result = true;
		} 
		
		return result;
	}
	
	/** 
	 * Shuffle the deck array
	 */
	public void shuffleDeck() {
		Collections.shuffle(this.deckSpells,HeroUtility.random);
		Collections.shuffle(this.deckSpells,HeroUtility.random);
		Collections.shuffle(this.deckSpells,HeroUtility.random);
	}
	
	/**
	 * Return a list of novice spells the player can cast
	 * @param maxManaCost filter spell that cost more then X mana, -1 not filtered 
	 * @return arraylist of novice spell the player currently can cast 
	 */
	public ArrayList<Spell> getNoviceSpell(int maxManaCost) {
		ArrayList<Spell> result = new ArrayList<Spell>();
				
		for(Spell s : personalSpells) {			
			if(s.getLevel().equals("Novice")  
				&& maxManaCost == -1 ? true : s.getCost() <= maxManaCost) {
				result.add(s);
			}
		}
		
		for(Spell s : handSpells) {
			if(s.getLevel().equals("Novice")  				
				&& maxManaCost == -1 ? true : s.getCost() <= maxManaCost) {
				result.add(s);
			}
		}
				
		for(Buff buff : activeSpells) {
			result.remove(buff.getSpell());
		}
		
		return result;
	}
	
	/** 
	 * Return a list of spells the player can cast
	 * @param maxManaCost filter spell that cost more then X mana, -1 not filtered
	 * @return arraylist of spell the player currently can cast 
	 */
	public ArrayList<Spell> getCastableSpells(int maxManaCost) {
		ArrayList<Spell> result = new ArrayList<Spell>();
		
		for(Spell s : personalSpells) {			
			if(maxManaCost == -1 ? true : s.getCost() <= maxManaCost ) {
				result.add(s);
			}
		}
		
		for(Spell s : handSpells) {
			if(maxManaCost == -1 ? true : s.getCost() <= maxManaCost ) {
				result.add(s);
			}
		}
		
		for(Buff buff : activeSpells) {
			result.remove(buff.getSpell());
		}
		
		return result;
	}
	
	/** 
	 * Return the number of spells the player can cast
	 * @param maxManaCost filter spell that cost more then X mana, -1 not filtered
	 * @return arraylist of spell the player currently can cast 
	 */
	public Integer getCastableSpellsCount(int maxManaCost) {
		Integer count = 0;
		
		for(Spell s : personalSpells) {			
			if(maxManaCost == -1 ? true : s.getCost() <= maxManaCost ) {
				count++;
			}
		}
		
		for(Spell s : handSpells) {
			if(maxManaCost == -1 ? true : s.getCost() <= maxManaCost ) {
				count++;
			}
		}

		//TODO filter active spell
		
		
		return count;
	}
	
	/** 
	 * Return a spell if the player can cast it
	 * @param spellId
	 * @return
	 */
	public Spell getSpell(int spellId) {
		Spell spell = null;
		
		for(Spell s : this.personalSpells) {
			if(s.getId() == spellId) {
				spell = s;
				break;
			}
		}

		if(spell == null) {
			for(Spell s : this.handSpells) {
				if(s.getId() == spellId) {
					spell = s;
					break;
				}			
			}
		}
		
		return spell;
	}
	
	/**
	 * Find if the user can cast a spell
	 * @param spellId
	 * @return
	 */
	public boolean hasSpell(int spellId) {
		boolean result = false;
		
		for(Spell spell : this.personalSpells) {
			if(spell.getId() == spellId) {
				result = true;
				break;
			}
		}

		if(result == false) {
			for(Spell spell : this.handSpells) {
				if(spell.getId() == spellId) {
					result = true;
					break;
				}			
			}
		}
					
		return result;
	}
}
