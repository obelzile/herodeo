package com.khaosstar.herodeo.model.game.entites;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import com.google.gson.annotations.Expose;

/**
 * 
 * Spell entity
 *
 */
public class Spell {

	@Expose private Integer id;
	@Expose private Integer heroId;
	@Expose private String name;
	@Expose private String description;
	@Expose private Integer cost;
	@Expose private String duration;
	@Expose private String level;

	private List<String> initScripts;
	private String onApplyScript;
	private String onExpireScript;

	public Spell() {
		
	}
		
	public String getOnApplyScript() {
		if(onApplyScript ==  null)
			onApplyScript = "";
		return onApplyScript;
	}

	@XmlElement(name = "onapply")
	public void setOnApplyScript(String onApplyScript) {
		this.onApplyScript = onApplyScript;
	}

	public String getOnExpireScript() {
		if(onExpireScript == null)
			onExpireScript = "";
		return onExpireScript;
	}

	@XmlElement(name = "onexpire")
	public void setOnExpireScript(String onExpireScript) {
		this.onExpireScript = onExpireScript;
	}

	public List<String> getInitScripts() {
		if(initScripts == null) {
			initScripts = new ArrayList<String>();
		}
		return initScripts;
	}
	
	@XmlElementWrapper( name="initscripts" )
	@XmlElement(name = "step")
	public void setInitScripts(List<String> scriptSteps) {
		this.initScripts = scriptSteps;
	}

	@XmlAttribute(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHeroId() {
		return heroId;
	}

	@XmlElement(name = "heroid")
	public void setHeroId(Integer heroId) {
		this.heroId = heroId;
	}

	public String getName() {
		return name;
	}

	@XmlElement(name = "name")
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	@XmlElement(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCost() {
		return cost;
	}

	@XmlElement(name = "cost")
	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public String getDuration() {
		return duration;
	}

	@XmlElement(name = "duration")
	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getLevel() {
		return level;
	}

	@XmlElement(name = "level")
	public void setLevel(String level) {
		this.level = level;
	}
	
	@Override public String toString() {
		return this.getName();
	}
}
