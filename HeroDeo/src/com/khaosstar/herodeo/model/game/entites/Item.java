package com.khaosstar.herodeo.model.game.entites;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.utility.HeroUtility;

/**
 * 
 * Item entity
 *
 */
public class Item {
	
	@Expose private Integer id;
	@Expose private String name;
	@Expose private String description;
	@Expose private String image;
	@Expose private Integer cost;		
	@Expose private Integer type;
	private String onApplyScript;
	private String onExpireScript;
	private String onPreCastScript;
	private String onPreCombatScript;
	private String onKillScript;
	
	public static final int EQUIPMENT = 0;
	public static final int RELIQUE = 1;
	
	public Item() {
		onApplyScript = "";
		onExpireScript = "";
		onPreCastScript = "";
		onPreCombatScript = "";
		onKillScript = "";	
	}
	
	public String getOnApplyScript() {
		if(onApplyScript ==  null)
			onApplyScript = "";
		return onApplyScript;
	}

	@XmlElement(name = "onapply")
	public void setOnApplyScript(String onApplyScript) {
		this.onApplyScript = onApplyScript;
	}

	public String getOnExpireScript() {
		if(onExpireScript == null)
			onExpireScript = "";
		return onExpireScript;
	}

	@XmlElement(name = "onexpire")
	public void setOnExpireScript(String onExpireScript) {
		this.onExpireScript = onExpireScript;
	}
	
	public Integer getId() {
		return id;
	}

	@XmlAttribute(name = "id")
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@XmlElement(name = "name")
	public void setName(String name) {		
		this.name = HeroUtility.encodeHTML(name);
	}
	
	public String getDescription() {
		return description;
	}

	@XmlElement(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	@XmlElement(name = "image")
	public void setImage(String image) {
		this.image = image;
	}

	public Integer getCost() {
		return cost;
	}

	@XmlElement(name = "cost")
	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Integer getType() {
		return type;
	}

	@XmlElement(name = "type")
	public void setType(Integer type) {
		this.type = type;
	}
	
	@Override public String toString() {
		return this.getName();
	}

	public String getOnPreCastScript() {
		return onPreCastScript;
	}

	@XmlElement(name = "onprecast")
	public void setOnPreCastScript(String onPreCastScript) {
		this.onPreCastScript = onPreCastScript;
	}

	public String getOnKillScript() {
		return onKillScript;
	}

	@XmlElement(name = "onkill")
	public void setOnKillScript(String onKillScript) {
		this.onKillScript = onKillScript;
	}

	public String getOnPreCombatScript() {
		return onPreCombatScript;
	}

	@XmlElement(name = "onprecombat")
	public void setOnPreCombatScript(String onPreCombatScript) {
		this.onPreCombatScript = onPreCombatScript;
	}	
	
	
}
