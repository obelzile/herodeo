package com.khaosstar.herodeo.model.game.entites;

import com.google.gson.annotations.Expose;

/**
 * 
 * Monster entity
 *
 */
public class Monster extends Unit {
	
	@Expose private MonsterType monsterType;	
	@Expose private Integer price;
	private boolean ismonster;
	private boolean attacked;
	private boolean moved;
	private boolean candodge;
	private boolean respawn;
	
	public Monster(MonsterType type,Position position) {
		setType(type);
		this.setPosition(position);
		this.setUuid(java.util.UUID.randomUUID().toString());
	}
	
	public MonsterType getMonsterType() {
		return monsterType;
	}
	
	public void setType(MonsterType type) {
		this.setId(type.getId());
		this.getEffects().clear();
		
		this.setName(type.getName());
		this.setMaxPV(type.getMaxPV());
		this.setMaxMovement(type.getMaxmovement());
		this.setProtection(type.getProtection());
		this.setStrength(type.getStrength());
		this.setPrice(type.getPrice());
		this.setIsMonster(type.isIsmonster());
		this.setCandodge(type.canDodge());
		this.setRespawn(type.canRespawn());
		this.setEffects(type.getEffects());
	}
		
	public boolean hasAttacked() {
		return attacked;
	}

	public void setAttacked(boolean attacked) {
		this.attacked = attacked;
	}

	public boolean hasMoved() {
		return moved;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	public boolean isMonster() {
		return ismonster;
	}

	public void setIsMonster(boolean ismonster) {
		this.ismonster = ismonster;
	}
	
	public boolean canDodge() {
		return candodge;
	}

	public void setCandodge(boolean candodge) {
		this.candodge = candodge;
	}

	public boolean canRespawn() {
		return respawn;
	}

	public void setRespawn(boolean respawn) {
		this.respawn = respawn;
	}

	@Override
	public Integer getTotalStrength() {
		return this.getStrength();
	}

	@Override
	public Integer getTotalProtection() {
		return this.getProtection();
	}
}
