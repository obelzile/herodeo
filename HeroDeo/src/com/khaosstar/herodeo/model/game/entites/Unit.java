package com.khaosstar.herodeo.model.game.entites;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.services.SpellEngine;

public abstract class Unit {
	
	private WSUser user;
	@Expose private Integer id;
	@Expose private String type; //Used client side 
	@Expose private String uuid;
	@Expose private String name;
	@Expose private Position position;
	@Expose private Integer maxPV;
	@Expose private Integer currentPV;
	@Expose private Integer strength;
	@Expose private Integer protection;
	@Expose private Integer maxMovement;
	@Expose private Integer stepLeft;
	@Expose private List<Position> attackTargets;
	private ArrayList<SpellEngine.enumEffect> effects;
	
	public Unit() {
		name = "";
		//ZERO position
		maxPV = 0;
		currentPV = 0;
		strength = 0;
		protection = 0;
		maxMovement = 0;
		stepLeft = 0;
		this.effects = new ArrayList<SpellEngine.enumEffect>();
		this.attackTargets = new ArrayList<Position>();
		this.type = this.getClass().getSimpleName();
	}
			
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getStepLeft() {
		return stepLeft;
	}

	public void setStepLeft(Integer stepLeft) {
		this.stepLeft = stepLeft;
	}

	//***  special EFFECTS
	public boolean hasEffect(SpellEngine.enumEffect effect) {
		for(SpellEngine.enumEffect e : effects) {
			if(e.equals(effect)) {
				return true;
			}
		}
		return false;
	}
	
	public void addEffect(SpellEngine.enumEffect effect) {
		effects.add(effect);
	}
	
	public void removeEffect(SpellEngine.enumEffect effect) {
		effects.remove(effect);
	}
	
	public ArrayList<SpellEngine.enumEffect> getEffects() {
		return effects;
	}
	
	public void setEffects(ArrayList<SpellEngine.enumEffect> effects) {
		this.effects = effects;
	}	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	
	//************* PV
	
	public Integer getCurrentPV() {
		return currentPV;
	}

	public void setCurrentPV(Integer currentPV) {
		this.currentPV = currentPV;
		if(this.currentPV < 0)
			this.currentPV = 0;
		if(this.currentPV > this.getMaxPV())
			this.currentPV = this.getMaxPV();
	}	
	
	public void addPV(Integer value) {
		this.currentPV += value;
		if(this.currentPV > this.getMaxPV())
			this.currentPV = this.getMaxPV();
	}
	
	public void removePV(Integer value) {
		this.currentPV -= value;
		if(this.currentPV < 0)
			this.currentPV = 0;
	}
	
	//********** Max PV

	public Integer getMaxPV() {
		return maxPV;
	}
	public void setMaxPV(Integer maxPV) {
		this.maxPV = maxPV;
		if(this.currentPV > this.maxPV) {
			this.currentPV = this.maxPV;
		}
	}
	
	public void addMaxPV(int value) {
		this.maxPV += value;
	}
	
	public void removeMaxPV(int value) {
		this.maxPV -= value;
		if(this.currentPV > this.maxPV) {
			this.currentPV = this.maxPV;
		}
	}
	
	public Integer getStrength() {
		return strength;
	}
	public void setStrength(Integer strength) {
		this.strength = strength;
	}
	public Integer getProtection() {
		return protection;
	}
	public void setProtection(Integer protection) {
		this.protection = protection;
	}
	public Integer getMaxMovement() {
		return maxMovement;
	}
	public void setMaxMovement(Integer maxMovement) {
		this.maxMovement = maxMovement;
	}

	public WSUser getUser() {
		return user;
	}

	public void setUser(WSUser user) {
		this.user = user;
	}
		
	public List<Position> getAttackTargets() {
		return attackTargets;
	}

	public void setAttackTargets(List<Position> attackTargets) {
		this.attackTargets = attackTargets;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public abstract  Integer getTotalStrength();
	public abstract  Integer getTotalProtection();
}
