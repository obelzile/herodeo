package com.khaosstar.herodeo.model.game.entites;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.google.gson.annotations.Expose;

/**
 * 
 * Hero entity
 *
 */
public class Hero {

	@Expose private Integer id;
	@Expose private String name;
	@Expose private String description;
	@Expose private Integer maxPV;
	@Expose private Integer strength;
	@Expose private Integer protection;
	@Expose private Integer movement;
	@Expose private String movementType;	
	@Expose private String color;
	@Expose private String cssclass;
	
	public Hero() {
		
	}

	public Integer getId() {
		return id;
	}

	@XmlAttribute(name = "id")
	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@XmlElement(name = "name")
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	@XmlElement(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMaxPV() {
		return maxPV;
	}

	@XmlElement(name = "maxPV")
	public void setMaxPV(Integer maxPV) {
		this.maxPV = maxPV;
	}

	public Integer getStrength() {
		return strength;
	}

	@XmlElement(name = "strength")
	public void setStrength(Integer strength) {
		this.strength = strength;
	}

	public Integer getProtection() {
		return protection;
	}

	@XmlElement(name = "protection")
	public void setProtection(Integer protection) {
		this.protection = protection;
	}

	public Integer getMovement() {
		return movement;
	}

	@XmlElement(name = "movement")
	public void setMovement(Integer movement) {
		this.movement = movement;
	}

	public String getColor() {
		return color;
	}

	@XmlElement(name = "color")
	public void setColor(String color) {
		this.color = color;
	}

	public String getCssclass() {
		return cssclass;
	}

	public void setCssclass(String cssclass) {
		this.cssclass = cssclass;
	}

	public String getMovementType() {
		return movementType;
	}

	@XmlElement(name = "movementType")
	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}
	
}
