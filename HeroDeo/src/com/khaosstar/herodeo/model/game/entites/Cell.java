package com.khaosstar.herodeo.model.game.entites;

import java.util.ArrayList;
import java.util.Set;

public class Cell {
	private Position position;
	private ArrayList<CellType> layers;
	private int flag = 0;
	
	public Cell(Position position,Integer baseLayer) {
		this.position = position;
		this.layers = new ArrayList<CellType>();
		this.layers.add(CellType.getTypeForInt(baseLayer));
	}
	
	public Cell(CellType baseLayer) {
		layers = new ArrayList<CellType>();
		layers.add(baseLayer);
	}
	
	public void addLayer(CellType layer) {
		layers.add(layer);
	}
	
	public void addLayer(Integer layer) {
		layers.add(CellType.getTypeForInt(layer));
	}
	
	public void removeLayer(CellType layer) {
		layers.remove(layer);
	}
	
	public boolean hasLayer(CellType layer) {
		return layers.contains(layer);
	}
	
	public boolean hasLayerOfAny(Set<CellType> types) {
		for(CellType c : types) {
			if(layers.contains(c))
				return true;				
		}
		return false;
	}		
	
	public void addFlag(int flag) {
		this.flag |= flag;  
	}
	
	public void removeFlag(int flag) {
		this.flag &= ~flag;
	}
	
	public int hasFlag(int flag) {
		return this.flag & flag;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}	
	
	public enum CellFlag {
		AllowWalk(1),
		AllowFlier(2);
				
		private final int value;
		
		private CellFlag(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
	
	public enum CellType {
		//UNKNOWN(-1),
		WALL(0),
		FLOOR(1),
		PIT(2),
		WELL(3),
		TRAP(4),
		CHEST(5),
		REDCHEST(6),
		START0(7),
		START1(8),
		START2(9),
		START3(10),
		BLUEENERGY(11),
		REDENERGY(12);
		
		private static CellType[] allValues = values();
		
		public static CellType  getTypeForInt(int num){
		    try{
		    return allValues[num];
		    }catch(ArrayIndexOutOfBoundsException e){
		       //return UNKNOWN;
		    	return null;
		    }
		}
		
		private final int value;
		
		private CellType(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
}
