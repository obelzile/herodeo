package com.khaosstar.herodeo.model.game.entites;

import java.util.ArrayList;

public class MonsterControl {
	private boolean controlling;
	private Player controller;
	private Position initialPosition;
	private Monster monster;
	private boolean attacked;
	private boolean moved;
	private ArrayList<Position> movementStep; // steps in the current movement state
	private Player nextPlayerPermission;
	
	public MonsterControl(Player controller,Monster monster) {
		this.controlling = true;
		this.controller = controller;		
		this.monster = monster;
		this.initialPosition = monster.getPosition();
		this.attacked = false;
		this.moved = false;
		this.movementStep = new ArrayList<Position>();
		this.nextPlayerPermission= controller; 
	}
	
	public boolean isControlling() {
		return controlling;
	}

	public void setControlling(boolean controlling) {
		this.controlling = controlling;
	}

	public Player getNextPlayerPermission() {
		return nextPlayerPermission;
	}

	public void setNextPlayerPermission(Player nextPlayerPermission) {
		this.nextPlayerPermission = nextPlayerPermission;
	}

	public Player getController() {
		return controller;
	}

	public void setController(Player controller) {
		this.controller = controller;
	}

	public Position getInitialPosition() {
		return initialPosition;
	}

	public void setInitialPosition(Position initialPosition) {
		this.initialPosition = initialPosition;
	}

	public Monster getMonster() {
		return monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public boolean hasAttacked() {
		return attacked;
	}

	public void setAttacked(boolean attacked) {
		this.attacked = attacked;
	}

	public boolean hasMoved() {
		return moved;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	public ArrayList<Position> getMovementStep() {
		return movementStep;
	}

	public void setMovementStep(ArrayList<Position> movementStep) {
		this.movementStep = movementStep;
	}
		
}
