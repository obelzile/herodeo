package com.khaosstar.herodeo.model.game.services;

import java.util.Collection;
import java.util.List;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.entites.Player;

public class HeroScriptEngine {

	private ScriptEngineManager mgr;
    private ScriptEngine scriptEngine;
    private Bindings bindings;
    
    private GameRoom room;
    
    public HeroScriptEngine(GameRoom room) {
    	
    	this.room = room;
    	
		this.mgr = new ScriptEngineManager();
	    this.scriptEngine = mgr.getEngineByExtension("js");
	    this.bindings = new SimpleBindings();
	   
	    this.bindings.put("room", room);
	    
	    String gamepackagesimport = "importPackage(com.khaosstar.herodeo.model.game.entites);" +
	    		"importPackage(com.khaosstar.herodeo.model.game.managers);" +
	    		"importPackage(com.khaosstar.herodeo.model.game.collections);" +
	    		"importPackage(com.khaosstar.herodeo.model.game);" +
	    		"importPackage(com.khaosstar.herodeo.model.game.services);" +
	    		"importPackage(com.khaosstar.herodeo.holders);" +
	    		"importPackage(java.util);";
	    
	    //Import all game package to avoid having to import them in the scripts
	    try {
			this.scriptEngine.eval(gamepackagesimport, this.bindings);
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void clearBindings() {
    	this.bindings.clear();
    }
    
    public void setBinding(String name,Object value) {
    	bindings.put(name, value);
    }
    
    public Object getBinding(String name) {
    	return bindings.get(name);
    }
    
    public void executeSpellScript(String script,Bindings bindings) {
    	
    }

    public void executeScripts(Collection<String> collection) {
    	for(String s : collection) {
    		executeScript(s);
    	}
    }
    
    public void executeScripts(String[] scripts) {
    	for(String s : scripts) {
    		executeScript(s);
    	}
    }

    public Object executeScript(Player caster ,String script) {
    	this.bindings.put("caster", caster);
    	return executeScript(script);
    }
    
    public Object executeScript(String script) {
    	Object result = null;
		try {
			result = scriptEngine.eval(script, bindings);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		return result;
    }
    
    public void executeScript(String script,Bindings bindings) {
		
    	bindings.put("room", this.room);
    	
    	try {
    		scriptEngine.eval(script, bindings);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
    }
    

}
