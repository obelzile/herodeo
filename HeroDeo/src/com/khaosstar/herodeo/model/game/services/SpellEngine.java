package com.khaosstar.herodeo.model.game.services;

import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import sun.org.mozilla.javascript.internal.Context;
import sun.org.mozilla.javascript.internal.ScriptableObject;


import com.google.gson.Gson;
import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Buff;
import com.khaosstar.herodeo.model.game.entites.MonsterControl;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Position;
import com.khaosstar.herodeo.model.game.entites.Spell;
import com.khaosstar.herodeo.model.game.entites.SpellCast;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.model.game.managers.CombatManager;
import com.khaosstar.herodeo.utility.HeroUtility;


/**
 * 
 * Class that handle the spell casting and spell effects
 *
 */
public class SpellEngine {
        
	private GameRoom room;
	
	public final static int DURATION_INSTANT = -3;
	public final static int DURATION_PERMANENT = -2;	
	public final static int DURATION_TILLDEATH = -1;
	
	public SpellEngine(GameRoom room) {
		this.room = room;
		
		this.room.getScriptEngine().setBinding("spellengine",this);
	}
	
	public void beginSpellCast(WSUser user,WSActionParams params) {
		Player player = this.room.getCurrentPlayer();
		if(player.getUser().equals(user)) {
			Spell spell = player.getSpellBook().getSpell(params.getAsInteger("spellid"));			
						
			if(!player.isSpellActive(spell)) {				
				SpellCast spellcast = new SpellCast(user,player,spell);
				
				this.room.getScriptEngine().clearBindings();
				this.room.getScriptEngine().setBinding("spellengine",this);
				this.room.getScriptEngine().setBinding("room",this.room);
				this.room.getScriptEngine().setBinding("spellcast",spellcast);
				this.room.getScriptEngine().setBinding("caster",spellcast.getCaster());
				
				if(spell.getInitScripts().size() > 0) {
					player.setCurrentSpell(spellcast);
					executeInitScripts(spellcast);
				} else {
					finishSpellCast(spellcast);
					player.setCurrentSpell(null); //done with the spell
				}
			}
		}
		
	}
	
	public void executeInitScripts(WSUser user,WSActionParams params) {
		Player player = this.room.getCurrentPlayer();
		if(player.getUser().equals(user) && player.getCurrentSpell() != null) {					
			this.room.getScriptEngine().setBinding("params",params);
			executeInitScripts(player.getCurrentSpell());
		}
	}
	
	public void executeInitScripts(SpellCast spellcast) {
		boolean proceedNextStep = true;		
		
		while(spellcast.getStep() < spellcast.getSpell().getInitScripts().size() && proceedNextStep == true) {
			proceedNextStep = false;
			this.room.getScriptEngine().setBinding("proceedNextStep",proceedNextStep);			
			this.room.getScriptEngine().executeScript(spellcast.getSpell().getInitScripts().get(spellcast.getStep()));
			spellcast.setStep(spellcast.getStep()+1);			
			proceedNextStep = (boolean)this.room.getScriptEngine().getBinding("proceedNextStep");
		}

		if(spellcast.isCanceled()) {
			cancelSpell(spellcast);			
		} else if(spellcast.getStep() >= spellcast.getSpell().getInitScripts().size()) {
			finishSpellCast(spellcast);
			spellcast.getCaster().setCurrentSpell(null); //done with the spell
		}			
	}
	
	public void finishSpellCast(SpellCast spellcast) {
		
		
		this.room.getScriptEngine().executeScripts(spellcast.getCaster().getOnPreCastScripts().values().toArray(new String[]{}));			
		
		if(spellcast.getCaster().getCurrentPM() >= spellcast.getSpellCost()) {
			
			boolean result = castSpell(spellcast.getCaster(),spellcast);
			
			if(result) {
				WSActionParams params = new WSActionParams();
				WSAction action = new WSAction("",params);
				params.put("nickname", spellcast.getCaster().getName());
				params.put("message", "cast " + spellcast.getSpell().getName());
				params.put("result", true);
				action.setAction("castspell");
				this.room.broadcastAction(action);
			}
			
			room.Net.outbound.broadcastPlayers(null);
		}
	}	
	
	public void cancelSpell(SpellCast spellcast) {
		spellcast.getCaster().setCurrentSpell(null);
		//TODO send cancel message;
	}	
	
	public void setSpellTarget(WSUser user,WSActionParams params) {
		Player player = this.room.getCurrentPlayer();
		if(player.getUser().equals(user)) {
			
		}		
	}
	
	public void spellMonsterMoveState(Player caster,MonsterControl monster) {
		WSActionParams params = new WSActionParams();
	
		
		params.put("id",monster.getMonster().getUuid());
		params.put("movement", monster.getMonster().getStepLeft());
		params.put("moving", true);
		params.put("movementCallback", "ExecuteInitScripts");
		params.put("finishCallback", "ExecuteInitScripts");
		WSAction action = new WSAction("onMonsterMoveState",params);
		
		this.room.getManager().sendMessage(action, caster.getUser());
	}
	
	
	public void spellMonsterInitAttack(SpellCast spellcast, MonsterControl monstercontrol,Unit target) throws Exception {
				
		if(target == null) {
			List<Position> targets = (List<Position>)spellcast.getDataValue("targets");
			if(targets.size() == 1) {
				target = room.getUnits().getUnit(targets.get(0));
			} else if(targets.size() > 1) {
				spellRequestTarget(spellcast,targets);
				return;
			} else {
				//no possible target should not be here;
				return;
			}			
		}
		
		if(target != null) {
			CombatManager combat = new CombatManager(this.room,spellcast.getCaster(),monstercontrol.getMonster(),target);
			spellcast.setDataValue("combat", combat);
			this.room.setCombat(combat);
			
			combat.run();
		}		
	}
	
	public void spellRequestTarget(SpellCast spellcast,List<Position> targets) {

		WSActionParams params = new WSActionParams();
		WSAction action = new WSAction("",params);

		if(targets.size() > 0) {
			params.put("targets", targets);
			params.put("replyto", "ExecuteInitScripts");
			action.setAction("requesttarget");		
		} else {
			cancelSpell(spellcast);
		}

		this.room.getManager().sendMessage(action, spellcast.getUser());	
	}
	
	public void spellRequestChoice(SpellCast spellcast,List<Object> choices) {
		WSActionParams params = new WSActionParams();
		WSAction action = new WSAction("",params);
		
		if(choices.size() > 0) {
			params.put("choices", choices);
			params.put("replyto", "ExecuteInitScripts");
			action.setAction("requestchoice");		
		} else {
			cancelSpell(spellcast);
		}
		
		this.room.getManager().sendMessage(action, spellcast.getUser());
	}
	
	
	public void castSpell(WSUser user,WSActionParams params) {
		Player player = this.room.getCurrentPlayer();
		if(player.getUser().equals(user)) {
			Spell spell = player.getSpellBook().getSpell(params.getAsInteger("spellid"));
			boolean result = false;
			if(spell != null) {
								
				//TODO check free cast
				
				SpellCast spellcast = new SpellCast(user,player,spell);
				
				this.room.getScriptEngine().setBinding("spellengine",this);
				this.room.getScriptEngine().setBinding("room",this.room);
				this.room.getScriptEngine().setBinding("spellcast",spellcast);
				this.room.getScriptEngine().setBinding("caster",player);
				
				this.room.getScriptEngine().executeScripts(player.getOnPreCastScripts().values().toArray(new String[]{}));			
					
				if(player.getCurrentPM() >= spellcast.getSpellCost()) {
					
					result = castSpell(player,spellcast);
					
					if(result) {
						WSAction action = new WSAction("",params);
						params.put("nickname", player.getName());
						params.put("message", "cast " + spell.getName());
						params.put("result", true);
						action.setAction("castspell");
						this.room.broadcastAction(action);
					}
					
					room.Net.outbound.broadcastPlayers(null);
				}
			}	
		}
	}
		
	public boolean castSpell(Player caster,SpellCast spellcast) {
		boolean result = true;
		
		//calculate duration 
		int duration = getSpellDuration(spellcast.getSpell()); 
				
		caster.removePM(spellcast.getSpellCost());
		
		Buff buff = null;				
		
		//script from target onReceivingSpell
		if(spellcast.getTargets().size() > 0) {
			for(Unit u : spellcast.getTargets()) {
				if(u instanceof Player) {
					this.room.getScriptEngine().executeScripts(((Player)u).getOnReceivingSpellScripts().values().toArray(new String[]{}));
				}
			}			
		}
					
		if(!spellcast.isCanceled()) {
			if(duration > DURATION_INSTANT) {							
				buff = new Buff(spellcast,duration);
				buff.setChoice(spellcast.getChoice());
				caster.getSpellBook().getActiveSpells().add(buff);	
			}
					
			if(!spellcast.getSpell().getOnApplyScript().equals("")) {				
				this.room.getScriptEngine().executeScript(spellcast.getSpell().getOnApplyScript());
			}	
			
			//consume if it's not a personal spell
			if(!caster.getEffects().contains(SpellEngine.enumEffect.SCROLL_KEEPER)) {
				if(!caster.getSpellBook().isPersonalSpell(spellcast.getSpell())) {
					caster.getSpellBook().discardSpell(spellcast.getSpell());
				}
			}
			
		}	
				
		return result;
	}	
	
	public void newTurn(Player player) {
				
		Iterator<Buff> iter = player.getSpellBook().getActiveSpells().iterator();
		while (iter.hasNext()) {
			Buff buff = iter.next();
			buff.update();
			if(buff.getDuration() == 0) {
				removeSpell(player,buff);
				iter.remove();
			}		   
		}
	}
	
	
	public void cancelAllBuff() {
		for(Player p : this.room.getUnits().getPlayerList()) {
			cancelPlayerBuff(p);
		}
	}
	
	public void cancelPlayerBuff(Player player) {
		Iterator<Buff> iter = player.getSpellBook().getActiveSpells().iterator();
		while (iter.hasNext()) {
			Buff buff = iter.next();
			removeSpell(player,buff);
			iter.remove();
		}
		
	}
	
	public void removeSpell(Player player,Buff buff) {
		
		if(!buff.getSpellcast().getSpell().getOnExpireScript().equals("")) {
			
			this.room.getScriptEngine().setBinding("spellcast",buff.getSpellcast());
			this.room.getScriptEngine().setBinding("caster",buff.getSpellcast().getCaster());
			
			this.room.getScriptEngine().executeScript(buff.getSpellcast().getSpell().getOnExpireScript());
		}	
	
	}
		
	public Integer getSpellDuration(Spell spell) {
		Integer result = 0;
		
		//<duration>instant</duration> <!-- instant, permanant, tilldeath, random, X -->
		switch(spell.getDuration()) {
			case "instant":
				result = -3;
				break;
			case "permanent":
				result = -2;
				break;
			case "tilldeath":
				result = -1;
				break;
			case "random":
				result = HeroUtility.random.nextInt(6)+1;
				break;
			default: // X turn
				try {
					result = Integer.parseInt(spell.getDuration());
				} catch(NumberFormatException nfe) {
					result = 0;
				} 
		}
		
		return result;
	}

    @XmlType
    @XmlEnum(Integer.class)
	public enum enumEffect {
		@XmlEnumValue("0") RANGE_IMMUNITY(0),
		@XmlEnumValue("1") MONSTER_IMMUNITY(1),
		@XmlEnumValue("2") SPELL_IMMUNITY(2),
		@XmlEnumValue("3") MINOR_IMMUNITY(3),
		@XmlEnumValue("4") TRAP_IMMUNITY(4),
		@XmlEnumValue("5") COUNTERATTACK_IMMUNITY(5),
		@XmlEnumValue("6") ATTACK_IMMUNITY(6),
		@XmlEnumValue("7") PORTABLE_MERCHANT(7),
		@XmlEnumValue("8") RANGE_ATTACK(8),
		@XmlEnumValue("9") SCROLL_KEEPER(9),
		@XmlEnumValue("10") MONSTER_CONTROL(10),
		@XmlEnumValue("11") STOP_TIME(11),
		@XmlEnumValue("12") ATTACK_ANYWHERE(12),
		@XmlEnumValue("13") FLY(13);
		
		private static enumEffect[] allValues = values();
		
		public static enumEffect  getTypeForInt(int num){
		    try{
		    return allValues[num];
		    }catch(ArrayIndexOutOfBoundsException e){
		    	return null;
		    }
		}
		
		private final int value;
		
		private enumEffect(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
}
