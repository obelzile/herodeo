package com.khaosstar.herodeo.model.game;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.khaosstar.herodeo.model.game.entites.Cell;
import com.khaosstar.herodeo.model.game.entites.Cell.CellFlag;
import com.khaosstar.herodeo.model.game.entites.Cell.CellType;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Position;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.model.game.services.SpellEngine;
import com.khaosstar.herodeo.model.services.IOService;

//TODO redo layout loading

public class GameBoard {
	private GameRoom room;
	private String layout;
	private String name;
	private String description;
	private int sizeWidth;
	private int sizeHeight;
	private Cell board[][];
	
	
	//TODO  add starting position tile info
	
	public GameBoard(GameRoom room) {
		this.room = room;
		this.layout = "data/layouts/testlayout.xml";	
	}

	public String getLayout() {
		return layout;		
	}
	
	public void setLayout(String layoutFile) {
		this.layout = "data/layouts/" + layoutFile;		 	
	}	
	
	public Cell getCell(Position position) {
		return board[position.getY()][position.getX()];
	}
	
	public List<Cell> getCellsOfType(CellType type) {
		return getCellsOfTypes(EnumSet.of(type));
	}
	
	public List<Position> getPositionsOfTypes(Set<CellType> types) {
		ArrayList<Position> positions = new ArrayList<Position>();
		
		for(int y=0;y<board.length;y++) {
			for(int x=0;x<board[y].length;x++) {
				for(CellType eType : types) {
					if(board[y][x].hasLayer(eType)) {
						positions.add(board[y][x].getPosition());
						break;
					}						
				}
			}
		}
		
		return positions;
	}
	
	public List<Cell> getCellsOfTypes(Set<CellType> types) {
		ArrayList<Cell> cells = new ArrayList<Cell>();
		
		for(int y=0;y<board.length;y++) {
			for(int x=0;x<board[y].length;x++) {
				for(CellType eType : types) {
					if(board[y][x].hasLayer(eType)) {
						cells.add(board[y][x]);
						break;
					}						
				}
			}
		}
		
		return cells;
	}
	
	public boolean movementRequest(Unit unit,Position position) {
		boolean result = false;
		
		if(board[position.getY()][position.getX()].hasFlag(CellFlag.AllowFlier.getValue()) > 0) {
			if(unit.getEffects().contains(SpellEngine.enumEffect.FLY)) {
				result = true;
			}
		} else if(board[position.getY()][position.getX()].hasFlag(CellFlag.AllowWalk.getValue()) > 0) {
			result = true;
		}
		
		return result;
	}
	
	public Position getStartingPosition(int index) {
	
		switch(index) {
			case 0:
				return findCellType(CellType.START0);
			case 1:
				return findCellType(CellType.START3);
			case 2:
				return findCellType(CellType.START2);
			case 3:
				return findCellType(CellType.START2);		
		}
		return null;
	}
	
	public Position findCellType(CellType type) {		
		for(Integer y=0;y<this.sizeHeight;y++) {
			for(Integer x=0;x<this.sizeWidth;x++) {
				if(board[y][x].hasLayer(type)) {
					return new Position(x,y);
				}					
			}
		}
		return null;
	}
	
	public boolean hasAnyOfCellType(Position position,Set<CellType> celltypes) {
		boolean result = false;
		
		for(CellType type : celltypes) {
			if(hasCellType(position,type)) {
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	public boolean hasCellType(Position position,CellType type) {		
		return this.board[position.getY()][position.getX()].hasLayer(type);	
	}
	
	public List<Position> queryPositionInRadius(Position position,Integer radius,Set<CellType> exclude) {
		List<Position> positions = new ArrayList<Position>();
		
		if(position.getX() > this.sizeWidth-1 || position.getY() > this.sizeHeight-1) {
			return positions;
		}
		
		//loop the radius
		for(int r=1;r<=radius;r++) {
			//loop 4 direction
			for(int p=0;p<4;p++) {
				try {
					switch(p) {
						case 0:							
							if(!this.board[position.getY()][position.getX()-r].hasLayerOfAny(exclude)) {
								positions.add(this.board[position.getY()][position.getX()-r].getPosition());
							}
							break;
						case 1:
							if(!this.board[position.getY()-r][position.getX()].hasLayerOfAny(exclude)) {
								positions.add(this.board[position.getY()-r][position.getX()].getPosition());
							}
							break;
						case 2:
							if(!this.board[position.getY()][position.getX()+r].hasLayerOfAny(exclude)) {
								positions.add(this.board[position.getY()][position.getX()+r].getPosition());
							}
							break;
						case 3:
							if(!this.board[position.getY()+r][position.getX()].hasLayerOfAny(exclude)) {
								positions.add(this.board[position.getY()+r][position.getX()].getPosition());
							}
							break;				
					}
				} catch(Exception ex) {
					
				}
			}
		}
		
		return positions;
	}
	
	public List<Cell> queryCellsInRadius(Position position,Integer radius,Set<CellType> exclude) {
		List<Cell> cells = new ArrayList<Cell>();
		
		if(position.getX() > this.sizeWidth-1 || position.getY() > this.sizeHeight-1) {
			return cells;
		}
		
		//loop the radius
		for(int r=1;r<=radius;r++) {
			//loop 4 direction
			for(int p=0;p<4;p++) {
				try {
					switch(p) {
						case 0:							
							if(!this.board[position.getY()][position.getX()-r].hasLayerOfAny(exclude)) {
								cells.add(this.board[position.getY()][position.getX()-r]);
							}
							break;
						case 1:
							if(!this.board[position.getY()-r][position.getX()].hasLayerOfAny(exclude)) {
								cells.add(this.board[position.getY()-r][position.getX()]);
							}
							break;
						case 2:
							if(!this.board[position.getY()][position.getX()+r].hasLayerOfAny(exclude)) {
								cells.add(this.board[position.getY()][position.getX()+r]);
							}
							break;
						case 3:
							if(!this.board[position.getY()+r][position.getX()].hasLayerOfAny(exclude)) {
								cells.add(this.board[position.getY()+r][position.getX()]);
							}
							break;				
					}
				} catch(Exception ex) {
					
				}
			}
		}
				
		return cells;
	}
			
	//TODO redo layout to deserialize on a class	
	public boolean applyLayout() {
		boolean result = false;
		
		InputStream xml = IOService.getResource(this.layout);
		
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Document doc = null;
		try {
			doc = docBuilder.parse(xml);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Element layout = (Element) doc.getElementsByTagName("layout").item(0);
		
		this.name = getValue("name",layout);
		//this.name = doc.getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue();
		this.description = getValue("description",layout);
		//this.description = doc.getElementsByTagName("description").item(0).getChildNodes().item(0).getNodeValue();		
		Element size = getElement("size",layout);
		
		//Element size = (Element) doc.getElementsByTagName("size").item(0);
		String width = size.getAttribute("width");
		String height = size.getAttribute("height");
		
		this.sizeWidth = Integer.parseInt(width);
		this.sizeHeight = Integer.parseInt(height);
		
		board = new Cell[this.sizeHeight][this.sizeWidth];
		
		//read layer0
		Element layer = getElement("layer0",layout);
		//NodeList listNode = doc.getElementsByTagName("layer0");
		NodeList rows = getElements("row",layer);
		//NodeList rows = ((Element)listNode.item(0)).getElementsByTagName("row");		
		
		for(int r=0;r<rows.getLength();r++) {
			Node row =rows.item(r);
			if(row.getNodeType() == Node.ELEMENT_NODE) {
				Element erow = (Element)row;
				NodeList cells = getElements("cell",erow);
				//NodeList cells = ((Element)row).getElementsByTagName("cell");
				for(int c=0;c<cells.getLength();c++) {
					//getValue("cell",cells.item(c));
					Element cell  = (Element) cells.item(c);
					String type = getValue("type",cell);
					String flag = getValue("flag",cell);
										
					board[r][c] = new Cell(new Position(c,r),Integer.parseInt(type));
					board[r][c].addFlag(Integer.parseInt(flag));
					//System.out.println(type);
				}
			}		
		}
		
		//read layer0
		Element layer1 = getElement("layer1",layout);
		//listNode = doc.getElementsByTagName("layer1");
		NodeList cells = getElements("cell",layer1);
		//NodeList cells = ((Element)listNode.item(0)).getElementsByTagName("cell");	
		for(int r=0;r<cells.getLength();r++) {
			Node cell =cells.item(r);
			if(cell.getNodeType() == Node.ELEMENT_NODE) {
				int x = Integer.parseInt(getValue("x",(Element)cell));
				int y = Integer.parseInt(getValue("y",(Element)cell));
				//int type = Integer.parseInt(getValue("type",(Element)cell));
				//int flag = Integer.parseInt(getValue("flag",(Element)cell));
				//int x = Integer.parseInt(((Element)cell).getElementsByTagName("x").item(0).getChildNodes().item(0).getNodeValue());
				//int y = Integer.parseInt(((Element)cell).getElementsByTagName("y").item(0).getChildNodes().item(0).getNodeValue());
				int type = Integer.parseInt(((Element)cell).getAttribute("type"));
				board[y][x].addLayer(type);
				//board[y][x].addFlag(flag);				
			}			
		}
		
		
		try {
			xml.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return result;
	}
	
	private static String getValue(String tag, Element element) {
		NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodes.item(0);
		return node.getNodeValue();
	}

	private static Element getElement(String tag, Element element) {
		NodeList nodes = element.getElementsByTagName(tag);
		if(nodes.getLength() > 0) {
			return (Element)nodes.item(0);
		} else {
			return null;
		}
	}
		
	private static NodeList getElements(String tag, Element element) {
		NodeList nodes = element.getElementsByTagName(tag);
		return nodes;
	}
}

