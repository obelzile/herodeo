package com.khaosstar.herodeo.model.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.khaosstar.herodeo.holders.HolderGameAsset;
import com.khaosstar.herodeo.model.entities.Room;
import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.collections.TreasureCollection;
import com.khaosstar.herodeo.model.game.collections.UnitCollection;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Cell;
import com.khaosstar.herodeo.model.game.entites.Cell.CellType;
import com.khaosstar.herodeo.model.game.entites.Hero;
import com.khaosstar.herodeo.model.game.entites.Item;
import com.khaosstar.herodeo.model.game.entites.Monster;
import com.khaosstar.herodeo.model.game.entites.MonsterControl;
import com.khaosstar.herodeo.model.game.entites.MonsterType;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Position;
import com.khaosstar.herodeo.model.game.entites.ShopItem;
import com.khaosstar.herodeo.model.game.entites.Spell;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.model.game.managers.AttackManager;
import com.khaosstar.herodeo.model.game.managers.CombatManager;
import com.khaosstar.herodeo.model.game.managers.GameActionManager;
import com.khaosstar.herodeo.model.game.managers.GameManager;
import com.khaosstar.herodeo.model.game.managers.MovementManager;
import com.khaosstar.herodeo.model.game.net.Net;
import com.khaosstar.herodeo.model.game.services.HeroScriptEngine;
import com.khaosstar.herodeo.model.game.services.SpellEngine;
import com.khaosstar.herodeo.utility.HeroUtility;

public class GameRoom extends Room {
		
	private Integer gameState; //GAMESTATE_HERO_SELECTION , GAMESTATE_RUNNING , GAMESTATE_GAMEOVER
	private Integer gameTurn; // current game turn
	private Integer playerTurnIndex; //current player 0 1 2 3 
	
	private MonsterType currentMonsterType;
	
	private UnitCollection units;
	
	public MovementManager MouvementManager;
	public AttackManager AttackManager;
	public GameActionManager GameActionManager;
	
	private GameControls controls;
	
	private GameBoard board; // game board
	
	private ArrayList<Position> movementStep; // steps in the current movement state
	private CombatManager combat; // combat manager
	private TreasureCollection treasures;
	
	public Net Net;		
	
	private MonsterControl controlledMonster; 	
		
	private SpellEngine spellEngine;
	
	private HeroScriptEngine scriptEngine;
	
	private boolean drank = false;
	private boolean moved = false;
	private boolean moving = false;
	
	private boolean attacked = false;
	private boolean freenovicespell = false;
	private boolean freespell = false;
	
	//Constants
	public static final int MAX_PLAYER = 4;
	public static final int HEALTH_FROM_WELL = 2; 
	
	public static final int GAMESTATE_HERO_SELECTION = 1;
	public static final int GAMESTATE_RUNNING = 2;
	public static final int GAMESTATE_GAMEOVER = 3;	
		
	public GameRoom(String uuid, String name, String password,GameManager manager) {
		super(manager);
		this.Net = new Net(this);
		this.uuid = uuid;
		this.lobbyName = name;
		this.password = password;
		this.board = new GameBoard(this);
		this.gameTurn = 0;
		this.playerTurnIndex = 0;
		this.gameState = 0;
		this.scriptEngine = new HeroScriptEngine(this);
		//this.players = new PlayerCollection(4,this);
		//this.monsters = new MonsterCollection(this);
		
		this.MouvementManager = new MovementManager(this);
		this.AttackManager = new AttackManager(this);
		this.GameActionManager = new GameActionManager(this);
		
		this.controls = new GameControls(this);
		
		this.units = new UnitCollection(this); 
		
		this.treasures = new TreasureCollection(this); 		
		this.spellEngine = new SpellEngine(this);
		this.controlledMonster = null;
								
		movementStep = new ArrayList<Position>();
		
		wsActionListener = new WSGameActionListener(this);		
	}
	
	public void requestTarget(Player player,List<Position> targets,String replyto) {

		WSActionParams params = new WSActionParams();
		WSAction action = new WSAction("requesttarget",params);

		if(targets.size() > 0) {
			params.put("targets", targets);
			params.put("replyto", replyto);	
		} else {
			
		}
		this.manager.sendMessage(action, player.getUser());	
	}
	
	public void sendGameMessage(Player player,String message) {
		WSActionParams params = new WSActionParams();
		
		params.put("nickname", player.getName());
		params.put("message", message);
		
		WSAction action = new WSAction("gamemessage",params);
		
		broadcastAction(action);
	}
	
	public boolean createMonster(Position position) {
		boolean result = false;
		if(this.getUnits().getMonsterCount() < 8) {
			Monster monster = new Monster(this.currentMonsterType,position);
			
			this.getUnits().add(monster);
			result = true;
		}
		return result;		
	}
	
	/**
	 * Perform the bonus phase
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void phaseBonus(Player player) {
		
		//reset monster movement for the next player;
		this.setControlledMonster(null);
		for(Monster monster : this.getUnits().getMonsterList()) {
			monster.setMoved(false);
			monster.setAttacked(false);
		}
		
		movementStep.clear();
		attacked = false;
		//movementStep.add(this.players[this.playerTurnIndex].getPosition());
		
		//Magic point
		Integer pm = player.getCurrentPM();
		switch(player.getSacredStone()) {
		case 0:
		case 1:
			player.setCurrentPM(pm+1);
			break;
		case 2:
			player.setCurrentPM(pm+2);			
			break;
		case 3:
			player.setCurrentPM(pm+3);			
			break;
		}
		
		// Effect duration -1		
		spellEngine.newTurn(player);
				
		// Destiny book
		Net.outbound.updateCurrentPlayerControls();		
	}
	
	public List<MonsterType> getMutableMonsters() {
		return HolderGameAsset.getMonsterTypesInstance().getMonsterTypes().getMutableMonsterTypes();			
	}
	
	public void addItemPlayer(Player player, Item item) {
		
		if(item.getType() == Item.EQUIPMENT) {
			if(!player.getEquipements().contains(item)) {				
				scriptEngine.executeScript(player,item.getOnApplyScript());
				player.getEquipements().add(item);			
			}
		} else {
			if(!player.getReliques().contains(item)) {			
				scriptEngine.executeScript(player,item.getOnApplyScript());
				player.getReliques().add(item);
				player.getOnPreCastScripts().put("item"+item.getId(), item.getOnPreCastScript());
			}			
		}
		
		if(!item.getOnPreCombatScript().equals(""))
			player.getOnPreCombatScripts().put("item"+item.getId(), item.getOnPreCombatScript());
		
		if(!item.getOnPreCastScript().equals(""))
			player.getOnPreCastScripts().put("item"+item.getId(), item.getOnPreCastScript());
		
		if(!item.getOnKillScript().equals(""))
			player.getOnKillScripts().put("item"+item.getId(), item.getOnKillScript());
	}
	
	public void removeItemPlayer(Player player, Item item) {
		
		if(item.getType() == Item.EQUIPMENT) {
			if(player.getEquipements().contains(item)) {				
				scriptEngine.executeScript(player,item.getOnExpireScript());
				player.getEquipements().remove(item);
			}
		} else {
			if(player.getReliques().contains(item)) {			
				scriptEngine.executeScript(player,item.getOnExpireScript());
				player.getReliques().remove(item);				
			}	
		}
		
		player.getOnPreCastScripts().remove("item"+item.getId());
		player.getOnKillScripts().remove("item"+item.getId());
		player.getOnPreCombatScripts().remove("item"+item.getId());
	}	
	
	public boolean canBuy(Player player,boolean override) {
		boolean result = false;
		
		if(!override) {
			if(player.getEffects().contains(SpellEngine.enumEffect.PORTABLE_MERCHANT)) {
				result = true;
			} else if(board.hasAnyOfCellType(player.getPosition(), EnumSet.of(CellType.START0,CellType.START1,CellType.START2,CellType.START3))) {
				result = true;
			}
		} else {
			result = override;
		}
		
		return result;
	}
	
	public void requestMerchant(Player player) {
		
		//can the player open the merchant
		if(!canBuy(player,false)) {
			return;
		}
		
		//TODO prevent player to buy when they can't
		WSActionParams params = new WSActionParams();
		
		ArrayList<ShopItem> shopitems = new ArrayList<ShopItem>();
		
		List<Item> equipments = HolderGameAsset.getItemManagerInstance().getItems().getItemsType(Item.EQUIPMENT);
		List<Item> reliques = HolderGameAsset.getItemManagerInstance().getItems().getItemsType(Item.RELIQUE);
					
		//Add equipment to shopping list and disable already owned equipments
		for(Item i : equipments) {
			
			ShopItem si = new ShopItem();
			si.setId("e"+i.getId());
			si.setType(ShopItem.SHOPITEM_EQUIPMENT);
			si.setName(i.getName());
			si.setDescription(i.getDescription());
			si.setCost(i.getCost());
			si.setImage(i.getImage());					
			si.setEnabled(!player.getEquipements().contains(i) && player.getCurrentPO() >= i.getCost());	
			
			shopitems.add(si);
		}
		
		for(Item i : reliques) {
			
			ShopItem si = new ShopItem();
			si.setId("r"+i.getId());
			si.setType(ShopItem.SHOPITEM_RELIQUE);
			si.setName(i.getName());
			si.setDescription(i.getDescription());
			si.setCost(i.getCost());
			si.setImage(i.getImage());
			
			boolean found = false;
			for(Player p : this.getUnits().getPlayerList()) {
				if(p.getReliques().contains(i)) {
					found = true;
				}
			}
			
			si.setEnabled(!found && player.getCurrentPO() >= i.getCost());	
			
			shopitems.add(si);
		}
		
		ShopItem si = new ShopItem();
		si.setId("s");
		si.setType(ShopItem.SHOPITEM_SCROLL);
		si.setName("Parchemin");
		si.setDescription("Donne un parchemin de votre pile.");
		si.setCost(1);
		si.setImage("parchemin.png");					
		si.setEnabled(player.getCurrentPO() >= 1);	
		
		shopitems.add(si);
										
		params.put("shoppingList", shopitems);
		
		WSAction action = new WSAction("merchantWindow",params);
		
		manager.sendMessage(action, player.getUser());
	}
	
	/**
	 * All player have chosen a hero, game ready to begin
	 * 
	 *
	 */
	public void gameBegin() {
		this.gameTurn = 0;
		this.playerTurnIndex = 0;
		
		this.setGameState(GAMESTATE_RUNNING);
		
		((WSGameActionListener)wsActionListener).newPlayerTurn(new WSActionParams());
	}
		
	////////////////////////
	//Command submit
	////////////////////////
	public void sendUserList(WSUser user) {
		
		WSActionParams params = new WSActionParams();
		
		WSUser[] users = this.roomMembers.values().toArray(new WSUser[0]);
		
		String[] nicknames = new String[users.length];
		
		for(int i=0;i<users.length;i++) {
			nicknames[i] = users[i].getNickname();
		}
		
		params.put("userlist", nicknames);
				
		WSAction action = new WSAction("userlist",params);
		
		//manager.sendMessage(action, user);
		broadcastAction(action);		
	}
	
	public void gameStart() {
		
		WSActionParams params = new WSActionParams();
		params.put("layout", board.getLayout());
				
		WSAction action = new WSAction("gamestart",params);
		
		broadcastAction(action);
			
	}
	
	public void playerMove(Player player) {
		WSActionParams params = new WSActionParams();
		params.put("uuid", player.getUser().getUUID());
		params.put("x", player.getPosition().getX().toString());
		params.put("y", player.getPosition().getX().toString());
		
		WSAction action = new WSAction("playermove",params);
		
		broadcastAction(action);
	}
		
	public void changePlayerControl() {
		WSActionParams params = new WSActionParams();
		params.put("turn", this.gameTurn.toString());
		params.put("playerIndex", this.playerTurnIndex.toString());
		
		WSAction action = new WSAction("changeplayerturn",params);
		
		broadcastAction(action);		
	}
		
	//Helper
	public boolean isPlayerTurn(WSUser user) {
		boolean result = false;
		
		if(units.getPlayerIndex(this.playerTurnIndex).getUser().getUUID().equals(user.getUUID()))
		{
			result = true;			
		}
		
		return result;		
	}
	
	public void setBoardLayout(WSUser user,WSActionParams params) throws ParserConfigurationException, SAXException, IOException {
		if(gameState == 0){
			
		}
	}		
	
	public void broadcastHeroData(WSUser user) {
		List<Hero> heroes = HolderGameAsset.getHeroManagerInstance().getHeroes().getHeroesList();
		
		WSActionParams params = new WSActionParams();
		params.put("heroes", heroes);
		
		WSAction action = new WSAction("herodata",params);
		
		if(user == null) {		
			broadcastAction(action);
		} else {
			manager.sendMessage(action, user);
		}
	}
	
	public void broadcastSpellsData(WSUser user) {
		List<Spell> spells = HolderGameAsset.getSpellManagerInstance().getSpells().getSpellList();
		
		WSActionParams params = new WSActionParams();
		params.put("spells", spells);
		
		WSAction action = new WSAction("spellsdata",params);
		
		if(user == null) {		
			broadcastAction(action);
		} else {
			manager.sendMessage(action, user);
		}
	}	
	

	public boolean userJoin(WSUser user) {
		boolean result = false;
		
		if(this.gameState == 0) {
			//Game in lobby mode not started
			if(!getRoomMembers().containsKey(user.getUUID())){
				
				getRoomMembers().put(user.getUUID(), user);
				user.setGameRoomUUID(this.uuid);
				result = true;
			}		
		} else {
			//put the user in the right player
			
			units.reconnectPlayer(user);
		}

		return result;
	}
	
	public void userLeave(WSUser user) {	
		//TODO handle player
		if(getRoomMembers().containsKey(user.getUUID())){
			getRoomMembers().remove(user);
			//broadcast
		}
	}
		
	public void filterOccupiedPosition(List<Position> positions) {
		
		Iterator<Position> iter = positions.iterator();
		
		while(iter.hasNext()) {
			Position p = iter.next();
			
			if(this.getUnits().getUnit(p) != null) {
				iter.remove();
				continue;
			}		
		}	
	}
	
	public boolean checkAttackPossibility() {
		return true;
	}
	
	public List<Position> getAttackPossibleTargets(Unit currentUnit) {
		List<Position> positions = null;
		
		if(currentUnit.getEffects().contains(SpellEngine.enumEffect.RANGE_ATTACK)) {
			if(currentUnit.getEffects().contains(SpellEngine.enumEffect.ATTACK_ANYWHERE)) {
				positions = new ArrayList<Position>();
				for(Unit u : this.getUnits()) {
					if(!u.equals(currentUnit)) {
						positions.add(u.getPosition());
					}
				}
			} else {			
				positions = this.getUnits().getRangeAttackPositions(currentUnit);
			}
		} else {
			positions = this.getUnits().getUnitsPositionInRange(currentUnit, 1);
		}
	
		return positions;
	}
	
	public void broadcastNewPlayerTurn(boolean refreshScene) {
		WSActionParams params = new WSActionParams();
		params.put("playerindex", this.playerTurnIndex);
		params.put("refreshscene", refreshScene);
		WSAction action = new WSAction("playerindexchanged",params);
		broadcastAction(action);	
	}
	
	public void startGame(WSUser user) {
		//if(this.gameState == 0) {
		this.setGameTurn(0);
		this.setPlayerTurnIndex(0);
		
		if(!user.getUUID().equals(this.getAdminuuid())) {
			//TODO not the admin prevent start game
		}
					
		if(this.getRoomMembers().size() > GameRoom.MAX_PLAYER) {
			// too many user in the room should be 4 max
		}			
		
		HolderGameAsset.getHeroManagerInstance().broadcastHeroes(this);
		HolderGameAsset.getSpellManagerInstance().broadcastSpells(this);
		HolderGameAsset.getItemManagerInstance().broadcastItems(this);				
		
		if(!this.getBoard().applyLayout()) {
			// error
		}
		
		List<Cell> cells = this.getBoard().getCellsOfTypes(EnumSet.of(CellType.REDENERGY,CellType.BLUEENERGY));
		
		for(Cell cell : cells) {
			this.getTreasures().addTreasure(cell);
		}
		
		//treasures.broadcastTreasures();
				
		//Temp list for shuffling
		List<Player> players = new ArrayList<Player>();			
		//Initialise Player
		for(WSUser member : this.getRoomMembers().values()) {
			Player player = new Player(member);
			players.add(player);				
		}
		Collections.shuffle(players,HeroUtility.random);
	
		int[] index; 
		if(players.size() == 2) {
			index = new int[]{0, 3, 1,2};
		} else if(players.size() == 3) {
			index = new int[]{0, 1, 3,2};
		} else {
			index = new int[]{0, 2, 1,3};
		}
		
		for(int i=0;i<players.size();i++) {
			players.get(i).setStartPosition(this.getBoard().getStartingPosition(index[i]));				
			players.get(i).setPosition(players.get(i).getStartPosition());
			players.get(i).setIndex(i);
		}
		
		this.getUnits().addAll(players);
			
		//Initialise Monster
		List<MonsterType> types = HolderGameAsset.getMonsterTypesInstance().getMonsterTypes().getMonsterTypeList();		
		MonsterType monstertype = types.get(HeroUtility.random.nextInt(types.size()));
		
		cells = this.getBoard().getCellsOfType(CellType.WELL);
		
		this.setCurrentMonsterType(monstertype);
		
		for(Cell cell: cells) {
			Monster monster = new Monster(this.currentMonsterType,cell.getPosition());
			
			this.getUnits().add(monster);
		}
				
		this.setGameState(1);
		
		this.Net.outbound.broadcastGameStateInfos(null);
								
		this.Net.outbound.beginHeroSelection();
		Net.outbound.updateCurrentPlayerControls();
	//}
	}
	
	/**
	 * User have join a game room
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void connectToRoom(WSUser user,String roomUUID, String roomPW) {

		WSActionParams params = new WSActionParams();
		WSAction action = new WSAction("",params);
		if(this.getPassword().equals(roomPW)) {
			//TODO retirer le user de la room qu'il est présentement
			this.userJoin(user);
			//user.setRoomUUID(roomUUID);
			
			this.sendUserList(user);

			params.put("room", this.getUuid());
			params.put("nickname", user.getNickname());
			
			if(this.getGameState() > 0) {
				HolderGameAsset.getHeroManagerInstance().broadcastHeroes(this);
				HolderGameAsset.getSpellManagerInstance().broadcastSpells(this);
				HolderGameAsset.getItemManagerInstance().broadcastItems(this);				
								
				this.Net.outbound.broadcastGameStateInfos(user);						
			}
			
			if(this.isHeroSelectionState()) { // hero selection
				action.setAction("heroselectionstart");
				action.setParams(null);
				this.getManager().sendMessage(action, user);
			} else if(this.isRunningState()) { // gameplay
				this.Net.outbound.broadcastPlayers(user);
				action.setAction("gamebegin");
				action.setParams(null);		
				this.getManager().sendMessage(action, user);				
			} else if(this.isGameOverState()) { //gameover
				
			}
			
			//WSAction action = new WSAction("userjoinroom",params);
			
			//broadcastAction(action);
			
			this.sendUserList(user);
			Net.outbound.updateCurrentPlayerControls();		
		}		
	}
	
	
	/////////////////////////
	// Properties
	/////////////////////////	
		
		
	public WSGameActionListener getWSGameActionListener() {
		return (WSGameActionListener)this.wsActionListener;
	}

	public MonsterControl getControlledMonster() {
		return controlledMonster;
	}

	public void setControlledMonster(MonsterControl controlledMonster) {
		this.controlledMonster = controlledMonster;
	}

	public HeroScriptEngine getScriptEngine() {
		return scriptEngine;
	}

	public void setScriptEngine(HeroScriptEngine scriptEngine) {
		this.scriptEngine = scriptEngine;
	}

	public TreasureCollection getTreasures() {
		return treasures;
	}

	public void setTreasures(TreasureCollection treasures) {
		this.treasures = treasures;
	}

	public CombatManager getCombat() {
		return combat;
	}

	public void setCombat(CombatManager combat) {
		this.combat = combat;
	}
	
	public boolean isMoving() {
		return moving;
	}

	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public SpellEngine getSpellEngine() {
		return spellEngine;
	}

	public void setSpellEngine(SpellEngine spellEngine) {
		this.spellEngine = spellEngine;
	}
	
	public boolean isHeroSelectionState() {
		return this.gameState == GAMESTATE_HERO_SELECTION;
	}
	
	public boolean isRunningState() {
		return this.gameState == GAMESTATE_RUNNING;
	}

	public boolean isGameOverState() {
		return this.gameState == GAMESTATE_GAMEOVER;
	}
			
	public Player getPreviousPlayer() {
		Integer index = 0;
		if(this.playerTurnIndex == 0) {
			index = this.getUnits().getPlayerCount()-1;
		} else {
			index = this.playerTurnIndex -1;
		}
		
		return this.getUnits().getPlayerIndex(index);
	}
	
	public Player getCurrentPlayer() {
		return this.getUnits().getPlayerIndex(this.playerTurnIndex);
	}
	
	public Player getNextPlayer() {
		Integer index = 0;
		if(this.playerTurnIndex == this.getUnits().getPlayerCount()-1) {
			index = 0;
		} else {
			index = this.playerTurnIndex +1;
		}
		
		return this.getUnits().getPlayerIndex(index);
	}
	
	public GameBoard getBoard() {
		return board;
	}

	public void setBoard(GameBoard board) {
		this.board = board;
	}

	public Integer getGameState() {
		return gameState;
	}

	public void setGameState(Integer gameState) {
		this.gameState = gameState;
		this.playerTurnIndex = 0;
		WSActionParams params = new WSActionParams();
		params.put("gamestate", this.gameState);
		WSAction action = new WSAction("gameStateChanged",params);
		broadcastAction(action);
	}
	
	public Integer getGameTurn() {
		return gameTurn;
	}

	public void setGameTurn(Integer gameTurn) {
		this.gameTurn = gameTurn;
		this.playerTurnIndex = 0;
		WSActionParams params = new WSActionParams();
		params.put("gameturn", this.gameTurn);
		WSAction action = new WSAction("turnchanged",params);
		broadcastAction(action);		
	}
	
	public Integer getPlayerTurnIndex() {
		return playerTurnIndex;
	}

	public void setPlayerTurnIndex(Integer playerTurnIndex) {
		this.playerTurnIndex = playerTurnIndex;		
	}		
	
	public boolean hasAttacked() {
		return attacked;
	}

	public void setAttacked(boolean attacked) {
		this.attacked = attacked;
	}

	public boolean hasDrank() {
		return drank;
	}

	public void setDrank(boolean drank) {
		this.drank = drank;
	}

	public boolean hasMoved() {
		return moved;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	public boolean isFreespell() {
		return freespell;
	}

	public void setFreespell(boolean freespell) {
		this.freespell = freespell;
	}
	
	public boolean isFreenovicespell() {
		return freenovicespell;
	}

	public void setFreenovicespell(boolean freenovicespell) {
		this.freenovicespell = freenovicespell;
	}

	public boolean isDrank() {
		return drank;
	}

	public boolean isAttacked() {
		return attacked;
	}

	public WSGameActionListener getWsActionListener() {
		return (WSGameActionListener)wsActionListener;
	}

	public void setWsActionListener(WSGameActionListener wsActionListener) {
		this.wsActionListener = wsActionListener;
	}

	public ArrayList<Position> getMovementStep() {
		return movementStep;
	}

	public void setMovementStep(ArrayList<Position> movementStep) {
		this.movementStep = movementStep;
	}

	public UnitCollection getUnits() {
		return units;
	}

	public void setUnits(UnitCollection units) {
		this.units = units;
	}
	
	public MonsterType getCurrentMonsterType() {
		return currentMonsterType;
	}

	public void setCurrentMonsterType(MonsterType currentMonsterType) {
		this.currentMonsterType = currentMonsterType;
	}

	public GameControls getControls() {
		return controls;
	}

	public void setControls(GameControls controls) {
		this.controls = controls;
	}
	
}


