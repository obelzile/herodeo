package com.khaosstar.herodeo.model.game.managers;

import java.util.Iterator;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Monster;
import com.khaosstar.herodeo.model.game.entites.MonsterControl;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Position;
import com.khaosstar.herodeo.model.game.entites.Cell.CellType;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.model.game.services.SpellEngine;

public class MovementManager {
	GameRoom gameroom;
	
	public MovementManager(GameRoom room) {
		this.gameroom = room;
	}
	
	/**
	 * Allow the user to move after triggering this state
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void requestMovementState(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				if(!this.gameroom.isMoving() && !this.gameroom.hasMoved()) {					
					this.gameroom.Net.outbound.requestMoveState(player,-1);
					this.gameroom.Net.outbound.updateCurrentPlayerControls();					
				}
			}
		}
	}
	
	/**
	 * movement request from the client. validate if moving is possible
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void movementRequest(WSUser user,WSActionParams params) {
		
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				boolean moved = false;
				
				WSAction action = new WSAction("",params);
							
				Position to = params.getAsPosition("position");
				Position from = player.getPosition();
				
				params.clear();
				//Canceling a move
				if(this.gameroom.getMovementStep().size() > 0 && this.gameroom.getMovementStep().get(this.gameroom.getMovementStep().size()-1).equals(to)) {
					moved = true;
					this.gameroom.getMovementStep().remove(this.gameroom.getMovementStep().size()-1);
					player.setStepLeft(player.getStepLeft()+1);			
					player.setPosition(to);
					this.gameroom.Net.outbound.broadcastUnitMove(player, null);
					
				} else if(this.gameroom.getMovementStep().contains(to)) {
					
				} else if(player.getStepLeft() > 0 && this.gameroom.getBoard().movementRequest(player, to)) {	
					
					//If there is a player in the target spot , do not move
					if(this.gameroom.getUnits().getUnit(to) == null) {					
						moved = true;
						this.gameroom.getMovementStep().add(from);
						player.setStepLeft(player.getStepLeft()-1);						
						player.setPosition(to);
						
						this.gameroom.Net.outbound.broadcastUnitMove(player, null);
					}
				}			
			}
		}
	}
	
	/**
	 * Prevent the user from moving after this move state finished
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void finishMovementState(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				if(this.gameroom.isMoving()) {
					this.gameroom.setMoving(false);
					player.setMovement(0);
					
					Iterator<Position> i = this.gameroom.getMovementStep().iterator();
					
					while(i.hasNext()) {
						Position p = i.next();
						
						if(this.gameroom.getBoard().hasCellType(p, CellType.TRAP)) {
							if(player.getEffects().contains(SpellEngine.enumEffect.FLY) || player.getEffects().contains(SpellEngine.enumEffect.TRAP_IMMUNITY)) {
								
							} else {
								player.removePV(1);
							}
						}
						
						i.remove();
					}
										
					this.gameroom.setMoved(true);
					
					params.clear();
					params.put("moving", false);
					params.put("moved", true);
					WSAction action = new WSAction("onMoveState",params);
					this.gameroom.getManager().sendMessage(action, user);
					
					//this.gameroom.getPlayers().broadcastPlayer(player.getIndex());
					this.gameroom.Net.outbound.broadcastUnit(player, null);
					
					player.setAttackTargets(this.gameroom.getAttackPossibleTargets(player));
									
					params.clear();
										
					params.put("playeruuid", player.getUuid());
					params.put("attacktargets", player.getAttackTargets());					
					
					action.setAction("attacktargetsupdate");
					this.gameroom.getManager().sendMessage(action, user);
					this.gameroom.Net.outbound.updateCurrentPlayerControls();
				}				
			}
		}
	}	
	
	
	public void finishMonsterMovement(Player player,MonsterControl monsterControl,boolean requirePermission,WSActionParams params) {
		//boolean approved = false;
		boolean approved = true;
		/*if(!requirePermission)
			approved = true;
		
		if(requirePermission == true) {						
			if(params.containsKey("choice")) {
				if(params.getAsInteger("choice") == 0) {
					monsterControl.getNextPlayerPermission().addPM(1);
					approved = true;
				}						
			}						
				
			monsterControl.setNextPlayerPermission(this.gameroom.getUnits().getNextPlayer(monsterControl.getNextPlayerPermission().getIndex()));
			if(approved == false && !monsterControl.getNextPlayerPermission().equals(player)) {
				params.clear();
				WSAction action = new WSAction("requestchoice",params);
				
				List<ChoiceItem> choices = new ArrayList<ChoiceItem>();
				choices.add(new ChoiceItem("Oui ( +1 PM )","0"));
				choices.add(new ChoiceItem("Non","1"));									

				params.put("choices", choices);
				params.put("title", "Approver déplacement?");
				params.put("replyto", "FinishMonsterMovementState");
					
				this.gameroom.getManager().sendMessage(action, monsterControl.getNextPlayerPermission().getUser());
				return;
			}
		}*/
		if(approved) {
			Monster monster = monsterControl.getMonster();
			monster.setStepLeft(0);
			monster.setMoved(true);
			monsterControl.setMoved(true);
						
			monsterControl.getMovementStep().clear();
			
			params.clear();
			params.put("moving", false);
			params.put("moved", true);
			WSAction action = new WSAction("onMonsterMoveState",params);
			this.gameroom.broadcastAction(action);
			//this.gameroom.getManager().sendMessage(action, player.getUser());
			
			this.gameroom.Net.outbound.broadcastMonsters(null);
		} else {
			Monster monster = monsterControl.getMonster();
			monster.setPosition(monsterControl.getInitialPosition());
			monster.setStepLeft(0);
			monster.setMoved(true);
			monsterControl.setMoved(true);
						
			monsterControl.getMovementStep().clear();
			
			params.clear();
			params.put("moving", false);
			params.put("moved", true);
			WSAction action = new WSAction("onMonsterMoveState",params);
			//this.gameroom.getManager().sendMessage(action, player.getUser());
			this.gameroom.broadcastAction(action);
			
			this.gameroom.Net.outbound.broadcastUnit(monster, null);
		}
		
	}
	
	public void finishMonsterMovementState(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user) || 
				this.gameroom.getUnits().getUnit(user.getUUID()).equals(this.gameroom.getControlledMonster().getNextPlayerPermission())) { // permission reply
				if(this.gameroom.getControlledMonster() != null && !this.gameroom.getControlledMonster().hasMoved()) {
					boolean requirePermission = true;
					if(this.gameroom.getUnits().getPlayerCount() < 2 || player.getEffects().contains(SpellEngine.enumEffect.MONSTER_CONTROL)) {	
						requirePermission = false;
					}
					finishMonsterMovement(player,this.gameroom.getControlledMonster(),requirePermission,params);
				}
			}
		}		
	}
		
	public void requestMonsterMovementState(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				if(this.gameroom.getControlledMonster() == null) {
					Integer x = (Integer)params.getAsInteger("x");
					Integer y = (Integer)params.getAsInteger("y");
					
					Position position = new Position(x,y);
					Unit unit = this.gameroom.getUnits().getUnit(position);
					if(unit != null) {
						//this.gameroom.setControlledMonster(monster);
					} else {
						return;
					}
				}
					
				/*if(!this.gameroom.getControlledMonster().hasMoved()) {
					Monster monster = null;//this.gameroom.getControlledMonster();
					
					monster.setStepLeft(monster.getMaxMovement());
					
					params.clear();
					params.put("index",this.gameroom.getMonsters().getMonsters().indexOf(monster));
					params.put("movement", monster.getStepLeft());
					params.put("moving", true);
					WSAction action = new WSAction("onMonsterMoveState",params);
					this.gameroom.getManager().sendMessage(action, player.getUser());
				}	*/			
			}
		}		
	}
	
	
	/**
	 * movement request from the client. validate if moving is possible
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public void monsterMovementRequest(Player player,MonsterControl monsterControl,WSActionParams params) {
		Monster monster = monsterControl.getMonster();
		boolean moved = false;
						
		Position to = params.getAsPosition("position");
		Position from = monster.getPosition();
		
		params.clear();
		//Canceling a move
		if(monsterControl.getMovementStep().size() > 0 && monsterControl.getMovementStep().get(monsterControl.getMovementStep().size()-1).equals(to)) {
			moved = true;
			monsterControl.getMovementStep().remove(monsterControl.getMovementStep().size()-1);
			monster.setStepLeft(monster.getStepLeft()+1);					
			monster.setPosition(to);
			
			this.gameroom.Net.outbound.broadcastUnitMove(monster, player.getUser());
			
		} else if(monsterControl.getMovementStep().contains(to)) {
			
		} else if(monster.getStepLeft() > 0 && this.gameroom.getBoard().movementRequest(monster, to)) {	
			
			//If there is a player in the target spot , do not move
			if(this.gameroom.getUnits().getUnit(to) == null) {					
				moved = true;
				monsterControl.getMovementStep().add(from);
				monster.setStepLeft(monster.getStepLeft()-1);						
				monster.setPosition(to);
				
				this.gameroom.Net.outbound.broadcastUnitMove(monster, player.getUser());
			}
		}
		
		/*if(moved) {
			this.gameroom.getPlayers().checkAttackPossibility();
			
			params.clear();
								
			params.put("playerindex", this.gameroom.getPlayerTurnIndex());
			params.put("attacktargets", player.getAttackTargets());					
			
			action.setAction("attacktargetsupdate");
			this.gameroom.getManager().sendMessage(action, user);
		}*/				
	}	
}
