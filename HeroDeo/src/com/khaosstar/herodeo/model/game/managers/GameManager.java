package com.khaosstar.herodeo.model.game.managers;

import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.managers.ActionManager;


public class GameManager extends ActionManager {

	public GameManager(String channel) {
		super(channel);
	}

	/**
	 * Create a new room
	 * @param uuid  id of the room
	 * @param name name of the room
	 * @param password password of the room optional
	 */
	@Override
	public boolean createRoom(String uuid, String name, String password) {
		boolean result = false;
		
		if(!hashRoom.containsKey(uuid)) {
			GameRoom room = new GameRoom(uuid, name, password,this);			
			hashRoom.put(uuid,room);
		}
			
		return result;
	}

}
