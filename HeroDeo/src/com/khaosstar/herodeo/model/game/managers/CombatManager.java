package com.khaosstar.herodeo.model.game.managers;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Monster;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Position;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.utility.HeroUtility;

public class CombatManager {
	private Player controller;
	private Unit attacker;
	private Unit defender;
	private GameRoom gameroom;
	private ArrayList<CombatHistory> history;
	private int attackDefenseType = 0; // bloque = 0  esquive = 1
	private int defenderDefenseType = 0; // bloque = 0 esquive = 1
	private int unitturn = 0;
		
	public CombatManager(GameRoom game,Player controller,Unit attacker,Unit defender) {
		//in the case of a player controlling a monster the controller will be different then the attacker
		this.controller = controller;  
		history = new ArrayList<CombatHistory>();
		this.gameroom = game;
		this.attacker = attacker;
		this.defender = defender;
		
		if(this.attacker instanceof Monster) {
			this.attacker.setCurrentPV(this.attacker.getMaxPV());
			this.gameroom.Net.outbound.broadcastUnit(this.attacker, null);
		}
		if(this.defender instanceof Monster) {
			this.defender.setCurrentPV(this.defender.getMaxPV());
			this.gameroom.Net.outbound.broadcastUnit(this.defender, null);
		}
				
	}
		
	/**
	 * Container class for the Combat history
	 *
	 */
	public class CombatHistory {
		@Expose private String attackerHistory;
		@Expose private String defenderHistory;
		
		public CombatHistory(String attacker,String defender) {
			this.attackerHistory = attacker;
			this.defenderHistory = defender;
		}
	}
	
	public void run() throws Exception {
		WSActionParams params = new WSActionParams();

		params.put("attackeruuid", attacker.getUuid());
		params.put("state",3); // 0 end combat 1 attack // 2 defense // 3 wait
		params.put("defenderuuid", defender.getUuid());
		WSAction action = new WSAction("startcombat",params);
		this.gameroom.getManager().sendMessage(action, this.controller.getUser());
		if(defender instanceof Monster) {
			performAttack();
		} else {
			params.put("state",2); // 0 end combat 1 attack // 2 defense // 3 wait
			this.gameroom.getManager().sendMessage(action, defender.getUser());
		}
	}
	
	public void setDefenseAction(WSActionParams params) throws Exception {
		if(unitturn % 2 == 0) {
			defenderDefenseType = (int)params.getAsInteger("defensetype");
		} else {
			attackDefenseType = (int)params.getAsInteger("defensetype");
		}
		
		performAttack();
	}
	
	/**
	 * User chose counterAttack
	 * @throws Exception 
	 *
	 */	
	public void counterAttack(WSActionParams params) throws Exception {
		
		Integer attackerNextState = 0; //0 End combat 1 Counter attack  2 defense 3 wait
		Integer defenderNextState = 0;
		
		if(unitturn % 2 == 0) {
			defenderNextState = 2;
			attackerNextState = 3;	
		} else {
			defenderNextState = 3;
			attackerNextState = 2;	
		}	
		
		params.put("playerturn", this.unitturn);
		params.put("defender", this.defender);
		params.put("attacker", this.attacker);	
		params.put("lasthistory", this.history.get(history.size()-1));
		
		WSAction action = new WSAction("counterattack",params);
		params.put("state",attackerNextState);
		gameroom.getManager().sendMessage(action, attacker.getUser());
		params.put("state",defenderNextState);	
		gameroom.getManager().sendMessage(action, defender.getUser());
		
		if(attacker instanceof Monster && attackerNextState == 2) {
			performAttack();
		} else if(defender instanceof Monster && defenderNextState == 2) {
			performAttack();
		}
	}
	
	
	/**
	 * End the combat and resolve player death
	 *
	 */
	public void endCombat() throws Exception {
		Unit killed = null;
		Unit killer = null;
		
		if(this.attacker.getCurrentPV() == 0) {
			killed = this.attacker;
			killer = this.defender;
		} else if(this.defender.getCurrentPV() == 0){
			killer = this.attacker;
			killed  = this.defender;			
		}
		
		if(killed != null && killer != null) {
			if(killed instanceof Player && killer instanceof Player) {
				playerKillPlayer((Player)killer,(Player)killed);
			} else if(killed instanceof Monster && killer instanceof Player) {
				playerKillMonster((Player)killer,(Monster)killed);
			} else if(killed instanceof Player && killer instanceof Monster){
				monsterKillPlayer((Monster)killer,(Player)killed);
			} else {
				throw new Exception("Error should not be here");
			}
		}

		this.gameroom.Net.outbound.broadcastEndCombat(null);

	}
	
	public void playerKillPlayer(Player killer,Player killed) {
		
		this.gameroom.getScriptEngine().setBinding("target",killed);
		this.gameroom.getScriptEngine().executeScripts(killer.getOnKillScripts().values());
		
		killed.getAttackTargets().clear();
		killer.getAttackTargets().clear();
		killed.setCurrentPV(killed.getMaxPV());
		killer.setSacredStone(killer.getSacredStone()+killed.getSacredStone());
		killed.setSacredStone(0);
		killer.addPO(killed.getCurrentPO());
		killed.setCurrentPO(0);
		killed.setCurrentPM(0);
		this.gameroom.getSpellEngine().cancelPlayerBuff(killed);
		//TODO transferer relique
		//TODO stop effect
		//TODO marqueur tresor*
		Position position = gameroom.getBoard().getStartingPosition(killed.getIndex());			
		if(gameroom.getUnits().getUnit(position) != null) {			
			for(int i=0;i<gameroom.getUnits().getPlayerCount();i++) {
				position = gameroom.getBoard().getStartingPosition(i);
				if(gameroom.getUnits().getUnit(position) != null)
					break;					
			}
		} 
		
		killed.setPosition(position);
		gameroom.Net.outbound.broadcastPlayer(killed.getIndex(), null);
		gameroom.Net.outbound.broadcastPlayer(killer.getIndex(), null);
		gameroom.Net.outbound.broadcastUnitMove(killed, null);	
	}
	
	public void playerKillMonster(Player killer,Monster killed) {
		
		this.gameroom.getScriptEngine().setBinding("target",killed);
		this.gameroom.getScriptEngine().executeScripts(killer.getOnKillScripts().values());
		
		killer.addPO(killed.getPrice());
		//place new mob on energy 
		gameroom.getUnits().remove(killed);
		
		gameroom.Net.outbound.broadcastRemoveUnit(killed, null);		
		gameroom.Net.outbound.broadcastPlayer(killer.getIndex(), null);
	}
	
	public void monsterKillPlayer(Monster killer,Player killed) {
		if(this.gameroom.getCurrentPlayer() == killed) {
			Player previous = this.gameroom.getPreviousPlayer();
			previous.setSacredStone(previous.getSacredStone()+killed.getSacredStone());
			killed.setSacredStone(0);			
		} else {
			Player current = this.gameroom.getCurrentPlayer();
			playerKillPlayer(current,killed);
		}
		this.gameroom.getSpellEngine().cancelPlayerBuff(killed);
	}		

	public void addHistory(String text) {
		if(unitturn % 2 == 0) {
			history.add(new CombatHistory("&nbsp;",text));
		} else {
			history.add(new CombatHistory(text,"&nbsp;"));
		}
	}
	
	
	/**
	 * Resolve an attack and calculate damage depending on dodging or blocking
	 * @throws Exception 
	 */
	public void performAttack() throws Exception {
		WSActionParams params = new WSActionParams();
		Integer attackerNextState = 0; //0 End combat 1 Counter attack  2 defense 3 wait
		Integer defenderNextState = 0;
		
		if(unitturn % 2 == 0) {
			defenderNextState = performAttack(attacker,defender,defenderDefenseType);			
			attackerNextState = 3;	
		} else {
			attackerNextState = performAttack(defender,attacker,attackDefenseType);			
			defenderNextState = 3;
		}
		
		params.put("playerturn", this.unitturn);
		params.put("defender", defender);
		params.put("attacker", attacker);	
		params.put("lasthistory", history.get(history.size()-1));
		
		WSAction action = new WSAction("attackperformed",params);
		params.put("state",attackerNextState);
		gameroom.getManager().sendMessage(action, attacker.getUser());
		params.put("state",defenderNextState);	
		gameroom.getManager().sendMessage(action, defender.getUser());
		
		unitturn++;
		
		if(defender instanceof Monster) {
			if(defenderNextState == 1) {
				counterAttack(params);
			} else if(defenderNextState == 0) {
				endCombat();
			}
		}
		
		if(attacker instanceof Monster) {
			if(attackerNextState == 1) {
				counterAttack(params);
			} else if(attackerNextState == 0) {
				endCombat();
			}
		}		
	}
	
	
	/**
	 * Resolve an attack and calculate damage depending on dodging or blocking
	 */
	public Integer performAttack(Unit attack, Unit defend,Integer defenseType) {
		Integer nextState = 0;
		
		boolean canCounterAttack = false;		
		Integer pvlost = 0;
	
		if(defend instanceof Monster) {
			if(defend.getCurrentPV() - (attack.getTotalStrength() - defend.getTotalProtection()) > 0) {
				defenseType = 0;
			} else { 
				defenseType = 1;
			}	
		}
		
		//Attacker turn
		if(defenseType == 0) {
			pvlost = attack.getTotalStrength() - defend.getTotalProtection();
			if(pvlost <= 0)
				pvlost = 1;
			addHistory("Bloque -" + pvlost.toString() + "PV");			
			nextState = 1;	
		} else {
			int esquive = HeroUtility.random.nextInt(6);
			if(esquive >= 4) {
				addHistory("Esquive");
				nextState = 1;	
			} else {
				pvlost = attack.getStrength();
				addHistory("-"+ pvlost +"PV");				
				nextState = 0;
			}
		}
		defend.setCurrentPV(defend.getCurrentPV() - pvlost);
		if(defend.getCurrentPV() <= 0) {
			nextState = 0;
		}
		return nextState;
	}
}

