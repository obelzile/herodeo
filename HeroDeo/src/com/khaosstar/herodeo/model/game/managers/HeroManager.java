package com.khaosstar.herodeo.model.game.managers;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.HeroeCollection;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Hero;
import com.khaosstar.herodeo.model.services.XMLReader;


/**
 * 
 * Load hero data fromthe WebContent/data/hero.xml file
 *
 */
public class HeroManager {

	private static HeroeCollection heroes;
		
	public HeroManager() {
		heroes = XMLReader.getHeroes();
	}
	
	public Hero getHero(int id) {
		return heroes.getHero(id);
	}
	
	public HeroeCollection getHeroes() {
		return heroes;
	}
		
	/**
	 * Broadcast hero data to all the players
	 * @param game gameroom containing the players to broadcast
	 */
	public void broadcastHeroes(GameRoom game) {
		
		WSActionParams params = new WSActionParams();
		
		params.put("heroes", heroes.getHeroesList());
		
		WSAction action = new WSAction("herosdata",params);
		
		game.broadcastAction(action);
	}
}
