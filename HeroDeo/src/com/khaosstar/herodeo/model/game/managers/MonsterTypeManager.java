package com.khaosstar.herodeo.model.game.managers;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.MonsterTypeCollection;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.MonsterType;
import com.khaosstar.herodeo.model.services.XMLReader;

public class MonsterTypeManager {

	private static MonsterTypeCollection monstertypes;
		
	public MonsterTypeManager() {
		monstertypes = XMLReader.getMonsterType();		
	}
	
	public MonsterType getMonsterType(int id) {
		return monstertypes.getMonsterType(id);
	}
	
	public MonsterTypeCollection getMonsterTypes() {
		return monstertypes;
	}
		
	/**
	 * Broadcast hero data to all the players
	 * @param game gameroom containing the players to broadcast
	 */
	public void broadcastMonsterTypes(GameRoom game) {
		
		WSActionParams params = new WSActionParams();
		
		params.put("monstertypes", monstertypes.getMonsterTypeList());
		
		WSAction action = new WSAction("monstertypesdata",params);
		
		game.broadcastAction(action);
	}
}