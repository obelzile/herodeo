package com.khaosstar.herodeo.model.game.managers;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.ItemCollection;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Item;
import com.khaosstar.herodeo.model.services.XMLReader;

/**
 * 
 * Load the Equipment and Relic from the WebContent/data/items.xml file 
 *
 */
public class ItemManager {
	
	private static ItemCollection items;
		
	public ItemManager() {
		items = XMLReader.getItems();
	}
	
	public Item getItem(int id) {
		return items.getItem(id);
	}
	
	public ItemCollection getItems() {
		return items;
	}	
		
	/**
	 * Broadcast items data to all the players
	 * @param game gameroom containing the players to broadcast
	 */
	public void broadcastItems(GameRoom game) {
		
		WSActionParams params = new WSActionParams();
		
		params.put("items", items.getItems());
		
		WSAction action = new WSAction("itemsdata",params);
		
		game.broadcastAction(action);
	}
}
