package com.khaosstar.herodeo.model.game.managers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.khaosstar.herodeo.holders.HolderGameAsset;
import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.ChoiceItem;
import com.khaosstar.herodeo.model.game.entites.Item;
import com.khaosstar.herodeo.model.game.entites.Monster;
import com.khaosstar.herodeo.model.game.entites.MonsterControl;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Position;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.utility.HeroUtility;

public class GameActionManager {
	private GameRoom gameroom;
	
	public GameActionManager(GameRoom room) {
		this.gameroom = room;
	}
	
	public void ramasserMarqueur(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				
				Iterator<Position> i = this.gameroom.getTreasures().iterator();
				
				boolean found = false;
				
				while(i.hasNext()) {
					Position p = i.next();
					if(p.equals(player.getPosition())) {
						i.remove();
						found = true;
					}						
				}
				String message = "Ramasse un coffre et obtient ";
				if(found) {				
					int value = HeroUtility.random.nextInt(6)+1;
					if(value%2 == 0) {
						player.addPO(value);
						this.gameroom.Net.outbound.broadcastUnit(player, null);
						//this.gameroom.getPlayers().broadcastPlayer(player.getIndex());
						message += value + " PO.";
					} else {
						List<Item> equipments = HolderGameAsset.getItemManagerInstance().getItems().getItemsType(Item.EQUIPMENT);
						equipments.removeAll(player.getEquipements());
						Item item = equipments.get(HeroUtility.random.nextInt(equipments.size()));
						player.getEquipements().add(item);
						//TODO when all equipment are owned
						
						message += "l\'equipement " + item.getName();
					}
					this.gameroom.Net.outbound.broadcastUnit(player, null);
					//this.gameroom.getPlayers().broadcastPlayer(player.getIndex());
					
					params.clear();
					params.put("position", player.getPosition());
					
					WSAction action = new WSAction("retirermarqueur",params);
					
					this.gameroom.broadcastAction(action);
					
					this.gameroom.sendGameMessage(player,message);
				}				
			}
		}				
	}
	
	public void buyItem(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				List<String> ids = (List<String>)params.get("shoppinglist");
				for(String i : ids) {					
					if(i.length() > 0) {
						String type = i.substring(0, 1);
						if(type.equals("s")) {

							//draw a card from the spell deck
							player.getSpellBook().drawCard(1);
							
							//TODO constant
							player.removePO(1);
							
						} else if(type.equals("e")) {
							if(i.length() > 1) {
								String idStr = i.substring(1);
								try {
									Integer id = Integer.parseInt(idStr);
									Item item = HolderGameAsset.getItemManagerInstance().getItem(id);
									if(!player.getEquipements().contains(item) && player.getCurrentPO() >= item.getCost()) {
										this.gameroom.addItemPlayer(player,item);
										player.removePO(item.getCost());										
									}																	
								} catch(NumberFormatException nfe) {
									
								}								
							}							
						} else if(type.equals("r")) {
							if(i.length() > 1) {
								String idStr = i.substring(1);
								try {
									Integer id = Integer.parseInt(idStr);
									Item item = HolderGameAsset.getItemManagerInstance().getItem(id);
									
									boolean found = false;
									for(Player p : this.gameroom.getUnits().getPlayerList()) {
										if(p.getReliques().contains(item)) {
											found = true;
											break;
										}										
									}
									
									if(!found && player.getCurrentPO() >= item.getCost()) {
										this.gameroom.addItemPlayer(player,item);
										player.removePO(item.getCost());										
									}																	
								} catch(NumberFormatException nfe) {
									
								}								
							}	
						}
					}
				}
				this.gameroom.Net.outbound.broadcastUnit(player, null);
			}
		}
	}
	
	public void monsterActionRequest(WSUser user,WSActionParams params) throws Exception {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user) && player.getCurrentSpell() == null && !this.gameroom.isMoving()) {				
				if(this.gameroom.getControlledMonster() == null) {
					Position target = params.getAsPosition("target");
					if(target != null) {
						Unit unit = this.gameroom.getUnits().getUnit(target);
						if(unit != null && unit instanceof Monster) {
							this.gameroom.setControlledMonster(new MonsterControl(player,(Monster)unit));							
						} else {
							return;
						}
					} else {						
						WSAction action = new WSAction("requesttarget",params);
						
						List<Position> positions = this.gameroom.getUnits().getMonsterPositionList();
						if(positions.size() > 0) {
							params.put("targets", positions);
							params.put("replyto", "MonsterActionRequest");
							
							this.gameroom.getManager().sendMessage(action, user);	
						} 
						
						return;
					}
				}
				
				if(!params.containsKey("choice")) {					
					/*params.clear();
					WSAction action = new WSAction("requestchoice",params);
					
					List<ChoiceItem> choices = new ArrayList<ChoiceItem>();
					if(!this.gameroom.getControlledMonster().hasMoved()) {
						choices.add(new ChoiceItem("Monster Move","0"));
					}
					if(!this.gameroom.getControlledMonster().hasAttacked() && this.gameroom.AttackManager.getMonsterAttackTargets(this.gameroom.getControlledMonster()).size() > 0) {
						choices.add(new ChoiceItem("Monster Attack","1"));	
					}								
					
					if(choices.size() > 0) {
						params.put("title", "Choix d'action");
						params.put("replyto", "MonsterActionRequest");
						params.put("choices", choices);
						
						this.gameroom.getManager().sendMessage(action, user);
					}*/
				} else {
					Integer choice = params.getAsInteger("choice");
					//Movement
					if(choice == 0) {
						if(!this.gameroom.getControlledMonster().hasMoved()) {
							Monster monster = this.gameroom.getControlledMonster().getMonster();
							
							monster.setStepLeft(monster.getMaxMovement());
							
							params.clear();
							params.put("id",monster.getUuid());
							params.put("movement", monster.getStepLeft());
							params.put("moving", true);
							params.put("movementCallback", "MonsterMovementRequest");
							params.put("finishCallback", "FinishMonsterMovementState");
							WSAction action = new WSAction("onMonsterMoveState",params);
							this.gameroom.getManager().sendMessage(action, player.getUser());
						}
					} else { // Attack
						if(!this.gameroom.getControlledMonster().hasAttacked()) {
							this.gameroom.AttackManager.initiateMonsterAttack(user, params);
						}						
					}
				}
				
				this.gameroom.Net.outbound.updateCurrentPlayerControls();
			}
		}
	}
	
	public void requestMonsterControlSkip(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user) && player.getCurrentSpell() == null) {				
				if(this.gameroom.getControlledMonster() != null) {
					this.gameroom.getControlledMonster().setControlling(false);
					this.gameroom.Net.outbound.updateCurrentPlayerControls();
				}
			}
		}
	}
}
