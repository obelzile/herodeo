package com.khaosstar.herodeo.model.game.managers;

import java.util.ArrayList;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.SpellCollection;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Spell;
import com.khaosstar.herodeo.model.services.XMLReader;


/**
 * 
 * Load the spells from the WebContent/data/spells.xml file 
 *
 */
public class SpellManager {
	private static SpellCollection spells;
		
	public SpellManager() {
		spells = XMLReader.getSpells();
	}

	public Spell getSpell(int id) {
		return spells.getSpell(id);
	}
	
	public SpellCollection getSpells() {
		return spells;
	}
	
	public ArrayList<Spell> getHeroSpells(int heroId) {
		ArrayList<Spell> list = new ArrayList<Spell>();
		
		for(Spell spell : spells.getSpellList()) {
			if(spell.getHeroId() == heroId) {
				list.add(spell);
			}
		}
		
		return list;
	}
		
	/**
	 * Broadcast spells data to all the players
	 * @param game gameroom containing the players to broadcast
	 */
	public void broadcastSpells(GameRoom game) {
		
		WSActionParams params = new WSActionParams();
		
		params.put("spells", spells.getSpellList());
		
		WSAction action = new WSAction("spellsdata",params);
		
		game.broadcastAction(action);
	}
}
