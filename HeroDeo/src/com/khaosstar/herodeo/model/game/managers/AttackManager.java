package com.khaosstar.herodeo.model.game.managers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.GameRoom;
import com.khaosstar.herodeo.model.game.collections.UnitCollection;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;
import com.khaosstar.herodeo.model.game.entites.Monster;
import com.khaosstar.herodeo.model.game.entites.MonsterControl;
import com.khaosstar.herodeo.model.game.entites.Player;
import com.khaosstar.herodeo.model.game.entites.Position;
import com.khaosstar.herodeo.model.game.entites.Unit;
import com.khaosstar.herodeo.model.game.services.SpellEngine;

public class AttackManager {
	GameRoom gameroom;
	
	public AttackManager(GameRoom room) {
		this.gameroom = room;
	}

	
	public boolean killMonster(Player player,Monster monster) {
		player.addPO(monster.getPrice());
		//place new mob on energy 
		gameroom.getUnits().remove(monster);
		
		gameroom.Net.outbound.broadcastRemoveUnit(monster, null);		
		gameroom.Net.outbound.broadcastPlayer(player.getIndex(), null);
		
		return true;
	}
	
	/**
	 * movement request from the client. validate if moving is possible
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 */
	public List<Player> getMonsterAttackTargets(MonsterControl monster) {
		List<Player> players = new ArrayList<Player>();
		
		players = this.gameroom.getUnits().getPlayersInRange(monster.getMonster().getPosition(), 1);
		Iterator<Player> iter = players.iterator();
		while(iter.hasNext()) {
			Player player = iter.next();
			if(player.getEffects().contains(SpellEngine.enumEffect.MONSTER_IMMUNITY)) {
				iter.remove();
			}
		}
		
		return players;
	}
	
	public void requestMonsterAttackState(WSUser user,WSActionParams params) {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				if(this.gameroom.getControlledMonster() == null) {
					Integer x = (Integer)params.getAsInteger("x");
					Integer y = (Integer)params.getAsInteger("y");
					
					Position position = new Position(x,y);
					Unit unit = this.gameroom.getUnits().getUnit(position);
					if(unit != null && unit instanceof Monster) {
						//this.gameroom.setControlledMonster(monster);
					} else {
						return;
					}
				}
					
				//if(!this.gameroom.getControlledMonster().hasAttacked()) {
					
				//}			
			}
		}		
	}	
	
	public void initiateMonsterAttack(WSUser user,WSActionParams params) throws Exception {
		if(this.gameroom.isRunningState()) {
			Player player = this.gameroom.getCurrentPlayer();
			if(player.getUser().equals(user)) {
				Player target = null;
				if(!params.containsKey("target")) {				
					List<Player> players = this.gameroom.AttackManager.getMonsterAttackTargets(this.gameroom.getControlledMonster());
					if(players.size() > 0) {
						if(players.size() > 1) {						
							this.gameroom.requestTarget(player,UnitCollection.getPositionListFromUnits(players),"InitiateMonsterAttack");
							return;
						} else {
							target = players.get(0);
						}
					}
				} else {
					Position position = params.getAsPosition("target");
					Unit unit = this.gameroom.getUnits().getUnit(position);
					if(unit != null && unit instanceof Player) {
						target = (Player)unit;
					}				
				}
							
				if(target != null) {
					CombatManager combat = new CombatManager(this.gameroom,player,this.gameroom.getControlledMonster().getMonster(),target);
					this.gameroom.setCombat(combat);
					combat.run();		
				}
			}
		}

	}
	
	/**
	 * Initiate combat between 2 player if they are in range
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void initiateAttack(WSUser user,WSActionParams params) throws Exception {
		if(this.gameroom.isRunningState()) {
			Player attacker = this.gameroom.getCurrentPlayer();
			if(attacker.getUser().equals(user)) {				
				if(attacker.getAttackTargets().size() != 0) {
					Position target = null;
				
					if(attacker.getAttackTargets().size() > 1) {
						if(!params.containsKey("target")) {
							this.gameroom.requestTarget(attacker,attacker.getAttackTargets(),"InitiateAttack");
							return;
						} else {
							target = params.getAsPosition("target");
						}
					} else {
						target = attacker.getAttackTargets().get(0);
					}				
					
					Unit defender = this.gameroom.getUnits().getUnit(target);
					
					CombatManager combat = new CombatManager(this.gameroom,attacker,attacker,defender);
					this.gameroom.setCombat(combat);
					combat.run();
							
				}					
			}
		}
	}
	
	/**
	 * end current combat
	 * and resolve any death
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void endCombat(WSUser user,WSActionParams params) throws Exception {
		if(this.gameroom.isRunningState()) {
			//Player attacker = this.players.getPlayer(this.playerTurnIndex);
			//if(attacker.getUser().equals()user)) {
						
			this.gameroom.getCombat().endCombat();
			
			for(Player player : this.gameroom.getUnits().getPlayerList()) {
				if(player.getSacredStone() == this.gameroom.getUnits().getPlayerCount()) {
					//this.gameroom.setGameState(3);
					//gameover
				}
			}
			
			params.clear();
			WSAction action = new WSAction("endcombat",null);			
			
			this.gameroom.broadcastAction(action);
			
			//TODO preven attack;
			this.gameroom.Net.outbound.broadcastPlayers(null);
			//}
		}		
	}

	/**
	 * Counter attack during combat
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void counterAttack(WSUser user,WSActionParams params) throws Exception {
		if(this.gameroom.isRunningState()) {
			//Player attacker = this.players.getPlayer(this.playerTurnIndex);
			//if(attacker.getUser().equals(user)) {
				this.gameroom.getCombat().counterAttack(params);
			//}
		}		
	}
	
	/**
	 * Set defensive action to block or esquive
	 * 
	 *
	 * @param  user  user that sent the action
	 * @param  params params sent with the action
	 * @throws Exception 
	 */
	public void setDefenseAction(WSUser user,WSActionParams params) throws Exception {
		if(this.gameroom.isRunningState()) {
			//Player player = this.gameroom.getCurrentPlayer();
			//if(attacker.getUser().equals(user)) {
				this.gameroom.getCombat().setDefenseAction(params);
			//}
		}
	}	
	
}
