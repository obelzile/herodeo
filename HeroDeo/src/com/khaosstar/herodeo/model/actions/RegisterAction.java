package com.khaosstar.herodeo.model.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.khaosstar.herodeo.model.entities.User;
import com.khaosstar.herodeo.model.managers.UserManager;

public class RegisterAction extends Action {

	public RegisterAction(String destinationOk, String destinationBad) {
		super(destinationOk, destinationBad);
		
	}

	@Override
	public boolean doTheJob(HttpServletRequest request,
			HttpServletResponse response) {
		
		User u = new User();
		
		String email = request.getParameter("email");
		String nickname = request.getParameter("nickname");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String pw = request.getParameter("pw");
		
		u.setEmail(email);
		u.setPassword(pw);
		u.setNickname(nickname);
		u.setFirstname(firstname);
		u.setLastname(lastname);
		
		UserManager.registerUser(u);
		
		destination = destinationOk;
		
		return true;
	}

}
