package com.khaosstar.herodeo.model.actions;


import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.khaosstar.herodeo.holders.HolderManager;
import com.khaosstar.herodeo.model.entities.Lobby;
import com.khaosstar.herodeo.model.game.managers.GameManager;
import com.khaosstar.herodeo.model.managers.ChatManager;

public class CreateLobbyAction extends Action {
	
	public CreateLobbyAction(String destinationOk, String destinationBad) {
		super(destinationOk, destinationBad);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doTheJob(HttpServletRequest request,
			HttpServletResponse response) {
		String mUUID, roomName, pw, adminUUID;
		Boolean isNewLobby = true;
		
		roomName = request.getParameter("roomName");
		pw = request.getParameter("pw");
		destination = destinationOk;
		
		ChatManager chat = (ChatManager) HolderManager.hashManagers.get("chat");
		GameManager game = (GameManager) HolderManager.hashManagers.get("game");
		
		ArrayList<Lobby> listLobbies = new ArrayList(HolderManager.hashManagers.get("chat").getHashRoom().values());
		
		//Validation mot de passe et nom du lobby pour joindre 
		for ( Lobby l : listLobbies) {
			if(l.getLobbyName().equals(roomName)) {
				isNewLobby = false;
								
				if (l.getPassword().equals(pw)) {		
					chat.createRoom(l.getUuid(), roomName, pw);					
					request.setAttribute("roomUUID", l.getUuid());
					request.setAttribute("roomName", l.getLobbyName());
					request.setAttribute("roomPW", pw);
					if (l.getLobbyName().equals("Global Chat")){
						destination = destinationBad;
					}
				} else {
					request.setAttribute("badroompassword", "true");
					destination = destinationBad;
				}
			}
		}

		//Si le lobby n'existe pas d�j� le cr�er
		if ( isNewLobby == true) {
			mUUID = java.util.UUID.randomUUID().toString();
			chat.createRoom(mUUID, roomName, pw);
			game.createRoom(mUUID, roomName, pw);
			request.setAttribute("roomUUID", mUUID);
			request.setAttribute("roomPW", pw);
			
			adminUUID = (String) request.getParameter("adminUUID");
			chat.getHashRoom().get(mUUID).setAdmin(adminUUID);
			
			request.setAttribute("isAdmin", true);
		}
		return true;
	}
}
