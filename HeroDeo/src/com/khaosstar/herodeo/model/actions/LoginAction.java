package com.khaosstar.herodeo.model.actions;

import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.khaosstar.herodeo.model.entities.User;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.managers.UserManager;

public class LoginAction extends Action {
	
	public LoginAction(String destinationOk,
			String destinationBad) {
		super(destinationOk, destinationBad);
	}

	@Override
	public boolean doTheJob(HttpServletRequest request,
			HttpServletResponse response) {
		boolean ok=true;
		
		User u;
		WSUser wsU;
		Cookie mCookie, mCookieEmail;
		
		String email = request.getParameter("email");
		String pw = request.getParameter("pw");

		u = UserManager.getMembreByLoginInfo(email, pw);
		HttpSession session = request.getSession(true);
		
		if (u != null) { 
			
			destination = destinationOk;
			HashMap<String, WSUser> hashOnlineUsers;
			
			ServletContext sc = request.getSession().getServletContext();
			if ( sc.getAttribute("hashOnlineUsers") == null) {
				hashOnlineUsers = new HashMap<String, WSUser>();	
			} else {
				hashOnlineUsers = (HashMap<String, WSUser>) sc.getAttribute("hashOnlineUsers");
			}	
			
			wsU = new WSUser(u);
			
			//On verifie qu'il n'est pas deja connecte
			for(WSUser user: hashOnlineUsers.values()){
				if(user.getEmail().equals(wsU.getEmail())){
					destination = destinationBad;
					return ok;
				}
			}
			
			hashOnlineUsers.put(wsU.getUUID(), wsU);
			sc.setAttribute("hashOnlineUsers", hashOnlineUsers);
			
			//Ajout des infos de l'utilisateur en session
			session.setAttribute("user_info", wsU);
			session.setAttribute("login_attempt", 0);
						
			mCookie = new Cookie("UUID", wsU.getUUID());
			mCookie.setMaxAge(30*60);
			response.addCookie(mCookie);
			
			mCookieEmail = new Cookie("UserEmail", u.getEmail());
			mCookieEmail.setMaxAge(30*60);
			response.addCookie(mCookieEmail);
		} else {
			destination = destinationBad;
		
			if (session.getAttribute("login_attempt") == null) 
				session.setAttribute("login_attempt", 1);
			else {
				int i = (int) session.getAttribute("login_attempt");
				i += 1;
				session.setAttribute("login_attempt", i);					
			}
		}	
		return ok;
	}
}
