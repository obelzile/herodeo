package com.khaosstar.herodeo.model.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class Action {
	protected String destinationOk;
	protected String destinationBad;
	protected String destination;
	
	public Action(String destinationOk, String destinationBad) {
		this.destinationOk = destinationOk;
		this.destinationBad = destinationBad;
	}
		
	//m�thode abstraite de toutes les actions
	public abstract boolean doTheJob(HttpServletRequest request, HttpServletResponse response);

	public String getDestination(){
		return destination;
	}	
}
