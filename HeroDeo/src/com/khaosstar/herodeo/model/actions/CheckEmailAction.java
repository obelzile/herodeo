package com.khaosstar.herodeo.model.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.khaosstar.herodeo.model.entities.User;
import com.khaosstar.herodeo.model.managers.UserManager;

public class CheckEmailAction extends Action {

	public CheckEmailAction(String destinationOk, String destinationBad) {
		super(destinationOk, destinationBad);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean doTheJob(HttpServletRequest request,
			HttpServletResponse response) {
		boolean ok=true;
		User u;
		
		String email = request.getParameter("email");
		
		u = UserManager.checkEmailAvailability(email);
		
		if (u != null) { 
			destination = destinationBad;
			System.out.println(u.getEmail() + "");
			response.setStatus(400);
		}
		else
		{
			destination = destinationOk;
			response.setStatus(200);
		}
		return ok;
	}
}
