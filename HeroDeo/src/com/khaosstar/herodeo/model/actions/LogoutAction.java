package com.khaosstar.herodeo.model.actions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.khaosstar.herodeo.model.entities.WSUser;

public class LogoutAction extends Action {

	public LogoutAction(String destinationOk, String destinationBad) {
		super(destinationOk, destinationBad);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean doTheJob(HttpServletRequest request,
			HttpServletResponse response) {
		HashMap<String, WSUser> hashOnlineUsers;
		String nickname;
		
		ServletContext sc = request.getSession().getServletContext();
		hashOnlineUsers = (HashMap<String, WSUser>) sc.getAttribute("hashOnlineUsers");
		nickname = (String)request.getParameter("nickname");
		
		for ( Iterator iter = hashOnlineUsers.entrySet().iterator(); iter.hasNext(); ) {
            @SuppressWarnings("rawtypes")
			Map.Entry ent = (Map.Entry) iter.next();
            //La cl� de la HashMap
            String key = (String) ent.getKey();
            //System.out.println("la cle " +cl�.toString());
            //La Valeur de la HashMap
            WSUser valeur = (WSUser) ent.getValue();
            //La Valeur du nikname
            String nicknameValeur = valeur.getNickname();
            
            //Traitement pour suprimer l utilisateur qui a fait la deconnexion de la liste hashOnlineUsers
            if (nicknameValeur.equals(nickname)) {
                 hashOnlineUsers.remove(key) ;            
            }
            
			// On remet l'arraylist en scope application
			sc.setAttribute("hashOnlineUsers", hashOnlineUsers);
				
			//On ferme la session
			HttpSession session = request.getSession();
			session.invalidate();
	
			destination = destinationOk;	
		}
		
		return false;	
	}
}
