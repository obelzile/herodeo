package com.khaosstar.herodeo.model.services;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSChannel;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;

public class CommandDeserializer implements JsonDeserializer<WSChannel> {

	public CommandDeserializer() {
	}

	/** 
	 *  deserialize a json string to a WSChannel object
	 */	
	@Override
	public WSChannel deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		// TODO error check
		JsonObject jo = (JsonObject)json;
		
		String channel = jo.get("channel").getAsString();
		JsonObject channelParams = jo.get("params").getAsJsonObject();
		String action = channelParams.get("action").getAsString();	
		JsonElement actionElement = channelParams.get("params");
				
		WSActionParams hashParams = context.deserialize(actionElement, WSActionParams.class);				
		
		return new WSChannel(channel,new WSAction(action,hashParams));
	}
}
