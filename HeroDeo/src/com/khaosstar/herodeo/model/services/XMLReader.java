package com.khaosstar.herodeo.model.services;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.khaosstar.herodeo.model.game.collections.HeroeCollection;
import com.khaosstar.herodeo.model.game.collections.ItemCollection;
import com.khaosstar.herodeo.model.game.collections.MonsterTypeCollection;
import com.khaosstar.herodeo.model.game.collections.SpellCollection;

public class XMLReader {

	public static MonsterTypeCollection getMonsterType() {
		MonsterTypeCollection monstertypes = null;
		
		try {

			InputStream xml = IOService.getResource("data/monsters.xml");			

			JAXBContext jaxbContext = JAXBContext.newInstance(MonsterTypeCollection.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			monstertypes = (MonsterTypeCollection) jaxbUnmarshaller.unmarshal(xml);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return monstertypes;
	}
	
	
	public static HeroeCollection getHeroes() {
		HeroeCollection heroes = null;
		
		try {

			InputStream xml = IOService.getResource("data/hero.xml");			

			JAXBContext jaxbContext = JAXBContext.newInstance(HeroeCollection.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			heroes = (HeroeCollection) jaxbUnmarshaller.unmarshal(xml);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return heroes;
	}
	
	public static SpellCollection getSpells() {
		SpellCollection spells = null;
		
		try {

			InputStream xml = IOService.getResource("data/spells.xml");			

			JAXBContext jaxbContext = JAXBContext.newInstance(SpellCollection.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			spells = (SpellCollection) jaxbUnmarshaller.unmarshal(xml);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return spells;
	}
	
	public static ItemCollection getItems() {
		ItemCollection items = null;
		
		try {

			InputStream xml = IOService.getResource("data/items.xml");			

			JAXBContext jaxbContext = JAXBContext.newInstance(ItemCollection.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			items = (ItemCollection) jaxbUnmarshaller.unmarshal(xml);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return items;
	}
}
