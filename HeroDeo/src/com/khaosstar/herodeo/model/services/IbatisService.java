package com.khaosstar.herodeo.model.services;

import java.io.IOException;
import java.io.Reader;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

public class IbatisService {

	private static SqlMapClient sqlMap;

	//***** configuration du sqlMap
	static{
		//config xml
		System.out.println("IbatisService.enclosing_method()");
		String ressource = "com/khaosstar/herodeo/model/services/sqlMapConfig.xml";		
		try {
			
			Reader reader = Resources.getResourceAsReader(ressource);
			
			sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
		
	public static SqlMapClient getSqlMap() {
		System.out.println("IbatisService.getSqlMap() $$$= " + sqlMap );
		return sqlMap;
	}
		
}
