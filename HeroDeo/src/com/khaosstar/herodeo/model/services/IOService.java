package com.khaosstar.herodeo.model.services;

import java.io.InputStream;

import javax.servlet.ServletContext;

public class IOService {
	private static ServletContext context;

	/**
	 * get the servlet context from a static variable accessible from everywhere in the project
	 * @return
	 */
	public static ServletContext getContext() {
		return context;
	}

	/**
	 * set the servlet context to a static variable accessible from everywhere in the project
	 * @return
	 */
	public static void setContext(ServletContext context) {
		IOService.context = context;
	}
	
	/**
	 * give an inputstream to a resource in the webcontent folder
	 * 
	 * @param resourcePath path to the resource
	 * @return InputStream
	 */
	public static InputStream getResource(String resourcePath) {
		return context.getResourceAsStream(resourcePath);
	}
}
