package com.khaosstar.herodeo.model.managers;

import java.io.IOException;
import java.nio.CharBuffer;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.khaosstar.herodeo.model.entities.Room;
import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSChannel;
import com.khaosstar.herodeo.model.entities.WSUser;

public abstract class ActionManager {

	private Gson gson;
	private String channel;
	protected HashMap<String, Room> hashRoom;
	
	public ActionManager(String channel) {
		this.channel = channel;
		this.hashRoom = new HashMap<String, Room>();
		//this.gson = new Gson();
		this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}
	
	public boolean processAction(WSUser user,WSAction action) {
		
		boolean result = false;
				
		if(this.hashRoom.containsKey(action.getRoomUUID())) {		
			result = this.hashRoom.get(action.getRoomUUID()).processAction(user, action);
		}
				
		return result;
				
	}
	
	public abstract boolean createRoom(String uuid, String name, String password);
	
	public void sendMessage(WSAction action,WSUser user) {
		if(user != null) {
			WSChannel packet = new WSChannel();
			packet.setChannel(channel);
			packet.setParams(action);
			
			//serialize
			try {
				
				if(!user.getSocket().isClosed()) {
					user.getSocket().getWsOutbound().writeTextMessage(CharBuffer.wrap(gson.toJson(packet)));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("SendMessageException: " + gson.toJson(packet));
				e.printStackTrace();
			}		
		}
	}

	public HashMap<String, Room> getHashRoom() {
		return this.hashRoom;
	}
	
	public void wsClosed(WSUser user) {
		Room room = hashRoom.get(user.getChatRoomUUID());
		if(room != null) {
			room.userLeft(user);
		}
	}
}
