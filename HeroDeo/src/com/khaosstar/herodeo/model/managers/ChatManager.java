package com.khaosstar.herodeo.model.managers;

import com.khaosstar.herodeo.model.entities.Lobby;
import com.khaosstar.herodeo.model.entities.Room;
import com.khaosstar.herodeo.model.entities.WSAction;
import com.khaosstar.herodeo.model.entities.WSUser;
import com.khaosstar.herodeo.model.game.collections.WSActionParams;

public class ChatManager extends ActionManager {


	public ChatManager(String channel) {
		super(channel);
		
		//Global Chat default lobby
		Lobby globalChat = new Lobby("default","Global Chat","", this);
		this.hashRoom.put("default", globalChat);
	}
	
	@Override
	public boolean createRoom(String uuid, String name, String password) {
		boolean result = false;
		
		if(!this.hashRoom.containsKey(uuid)) {
			Lobby room = new Lobby(uuid, name, password, this);
			this.hashRoom.put(uuid,room);
			
			//Avertir les users dans le global Chat de la presence de la room
			SendNewRoomToMainLobby(room);
			
		}
		return result;
	}
	
	
	public void SendNewRoomToMainLobby(Room room) {
		WSActionParams params = new WSActionParams();
		params.put("roomname", room.getLobbyName());
		
		if(room.getPassword() != null && room.getPassword().length()>0){
			params.put("passwordPresent", "true");
		}
		WSAction newroomcreated = new WSAction("newroomcreated", params);
		
		for (WSUser u : this.hashRoom.get("default").getRoomMembers().values()) {
			sendMessage(newroomcreated, u);
		}
	}
	
}
