package com.khaosstar.herodeo.model.managers;

import java.sql.SQLException;
import java.util.HashMap;

import com.khaosstar.herodeo.model.entities.User;
import com.khaosstar.herodeo.model.services.IbatisService;


public class UserManager {
	
	public static User getMembreByLoginInfo(String email, String password){
		User u = null;
		HashMap<String, String> params =  new HashMap<String, String>();
		params.put("email", email);
		params.put("pw", password);
		
		try {
			//System.out.println("UsagerManager.getUsagerByLoginInfo()222");
			u  = (User)IbatisService.getSqlMap().queryForObject("getUserByLoginInfo", params);
			//System.out.println("dsadsdsdsd");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return u;
	}
	
	public static User checkEmailAvailability(String email) {
		User u = null;

		try {
			u = (User)IbatisService.getSqlMap().queryForObject("checkEmailAvailability", email);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return u;
		
	}
	
	public static void registerUser(User u) {
		
	try {
			int userId = (Integer)IbatisService.getSqlMap().insert("addUser", u);
			u.setUserId(userId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

