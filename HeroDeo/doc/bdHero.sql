-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4234
-- Date/time:                    2013-02-18 13:20:45
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for bdoasis
CREATE DATABASE IF NOT EXISTS `bdhero` /*!40100 DEFAULT CHARACTER SET big5 */;
USE `bdoasis`;


-- Dumping structure for table bdoasis.user
CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `password` varchar(12) CHARACTER SET utf8 NOT NULL,
  `firstname` char(25) DEFAULT NULL,
  `lastname` char(25) DEFAULT NULL,
  `nickname` char(10) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=big5;

-- Dumping data for table bdoasis.user: ~15 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`userId`, `email`, `password`, `firstname`, `lastname`, `nickname`) VALUES
	(1, 'c', 'c', 'a', 'a', 'coco'),
	(14, 'a', 'a', 'a', 'a', 'allo'),
	(15, 'b', 'b', 'b', 'b', 'bobo'),
	(16, 'c@c.com', 'cccc', '', '', ''),
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
