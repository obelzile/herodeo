<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div id="combat" class="combat-popup">
	<div style="width: 100%; height: 120px; vertical-align:bottom; padding-top: 30px;">
		<div class="floatleft" id="stat-player-attacker">
			<h2>Attaquant</h2>
			<p id="attacker-name"></p>
			<p>PV : <span id="attacker-pv">0</span>/<span id="attacker-maxpv">0</span></p>
			<p>Force : <span id="attacker-strength">0</span></p>
			<p>Protection : <span id="attacker-protection">0</span></p>	
		</div>
		<div class="floatleft" id="avatar-player-attacker"><img id="avatar-player-attacker-img" src="images/avatars/avatar-0.png"/></div>
		<div id="attack-grid" class="floatleft">
			<h2>Historique</h2>
			<div id="history" style="width: 100%;height: 100px;">
				<div style="float:left; width: 50%"><ul id="attacker-history"></ul></div>
				<div style="float:left; width: 50%"><ul id="defender-history"></ul></div>
			</div>					
		</div>
		<div class="floatleft" id="avatar-player-defender"><img id="avatar-player-defender-img" src="images/avatars/avatar-3.png"/></div>
		<div class="floatleft" id="stat-player-defender">
			<h2>Defendeur</h2>
			<p id="defender-name"></p>
			<p>PV : <span id="defender-pv">0</span>/<span id="defender-maxpv">0</span></p>
			<p>Force : <span id="defender-strength">0</span></p>
			<p>Protection : <span id="defender-protection">0</span></p>
		</div>			
	</div>
	<div style="width: 100%; height: 50px; text-align: center;">
		<button id="start-combat">Commancer combat</button>
		<button id="block-attack">Bloque</button>
		<button id="dodge-attack">Esquive</button>
		<button id="counter-attack">Contre attaque</button>
		<button id="end-combat">Fin Combat</button>
		<p id="wait-notice">Wait while the player choose a defense action!</p>
	</div>
</div>