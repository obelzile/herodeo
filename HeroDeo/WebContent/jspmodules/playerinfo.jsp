<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="selection-border">
	<div class="playerinfo-box border">
		<div class="stat-box">
			<div class="avatar-wrapper"><img src=""/></div>
			<div class="sacredstone-wrapper">
				<div title="Sacred Stone" class="sacredstone sacredstone-nd"></div>
				<div title="Sacred Stone" class="sacredstone sacredstone-nd"></div>
				<div title="Sacred Stone" class="sacredstone sacredstone-nd"></div>
				<div title="Sacred Stone" class="sacredstone sacredstone-nd"></div>
			</div>
			<div class="nickname-wrapper">
				<span class="nickname"></span>
				<img class="" src="images/game/star.png">
			</div>
			<div class="class-wrapper"><span class="avatar-class"></span></div>
			<div class="attributes-wrapper">
				<div class="attribute"><div title="Life Point" class="heart"></div> <span class="currentPV">0</span>/<span class="maxPV">0</span></div>
				<div class="attribute attribute-border"><div title="Magic Point" class="mana"></div> <span class="currentPM">0</span></div>
				<div class="attribute attribute-border"><div title="Gold Coin" class="gold"></div> <span class="currentPO">0</span></div>
			</div>
			<div class="attributes2-wrapper">
				<div class="attribute"><div title="Strength" class="strength-img"></div> <span class="strength">0</span> +(<span class="bonusstrength">0</span>)</div>
				<div class="attribute attribute-border"><div title="Protection" class="protection-img"></div> <span class="protection">0</span> +(<span class="bonusprotection">0</span>)</div>
				<div class="attribute attribute-border"><div title="Movement" class="movement-img"></div> +<span class="bonusdeplacement">0</span></div>
			</div>
			<div class="spell-wrapper">
				<div class="attribute"><span class="spell0"></span></div>
				<div class="attribute attribute-border"><span class="spell1"></span></div>
				<div class="attribute attribute-border"><span class="spell2"></span></div>
			</div>
			<div class="inventory-wrapper">
				<ul>
					<li>test</li>
					<li>test</li>
					<li>test</li>
				</ul>
			</div>
		</div>
		<div class="asset-box">
			<div class="buff-wrapper">
				<ul>
					<li><div title="Buff Test 1 &#10;Turn left : 1" class="buff"><img src="images/game/buff.png"/><span class="turn">1</span></div></li>
					<li><img src="images/game/buff.png"/></li>
					<li><img src="images/game/buff.png"/></li>
					<li><img src="images/game/buff.png"/></li>
					<li><img src="images/game/buff.png"/></li>
					<li><img src="images/game/buff.png"/></li>
					<li>...</li>
				</ul>
			</div>
		</div>				
	</div>
</div>