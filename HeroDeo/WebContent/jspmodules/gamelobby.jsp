<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div id="startgame">

<%if (isAdmin != null && isAdmin == true) {%>
	<button onclick="JavaScript:startgame()">Start Game</button>
<%} %>		

	<dl>
		<dt>
			<select id="selectplayer0">
				<option>Open</option>
				<option>Close</option>
			</select>
		</dt>
		<dd>
			<input type="text" id="player0" readonly/>	
		</dd>	
		<dt>
			<select id="selectplayer1">
				<option>Open</option>
				<option>Close</option>
			</select>
		</dt>
		<dd>
			<input type="text" id="player1" readonly/>
				<%if (isAdmin != null && isAdmin == true) {%>
			<div class="kickPlayer"></div>
			<%} %>						
		</dd>	
		<dt>
			<select id="selectplayer2">
				<option>Open</option>
				<option>Close</option>
			</select>
		</dt>
		<dd>
			<input type="text" id="player2" readonly/>		
			<%if (isAdmin != null && isAdmin == true) {%>
			<div class="kickPlayer"></div>
			<%} %>				
		</dd>	
		<dt>
			<select id="selectplayer3">
				<option>Open</option>
				<option>Close</option>
			</select>
		</dt>
		<dd>
			<input type="text" id="player3" readonly/>	
			<%if (isAdmin != null && isAdmin == true) {%>
			<div class="kickPlayer"></div>
			<%} %>					
		</dd>									
	</dl>
</div>