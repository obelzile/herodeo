<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div id="spells" class="spells">
	<div class="popup-title"><h3>Spells</h3><div id="spell-close" class="popup-close"></div> </div>
	<div class="spell-scroll-wrapper">
		<div id="spell-container">
			<div id="spell-template" class="spell">
				<div class="spell-hero"><img src=""/></div>
				<div class="spell-name"></div>
				<div class="spell-cost"></div>
				<div class="spell-duration"></div>
				<div class="spell-description"></div>		
			</div>
		</div>					
	</div>
</div>