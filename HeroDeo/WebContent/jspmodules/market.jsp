<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div id="merchant" class="merchant">
	<div class="popup-title"><h3>Marchand</h3><div id="merchant-close" class="popup-close"></div></div>
	<div class="merchant-scroll-wrapper">
		<ul id="merchant-items">
		</ul>		
	</div>
	<div id="player-gold"><span>0</span><img src="images/game/coin.png"><button id="merchant-buy">Acheter</button></div>
</div>