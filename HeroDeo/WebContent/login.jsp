<%@page import="sun.reflect.ReflectionFactory.GetReflectionFactoryAction"%>
<%@page import="org.apache.coyote.ActionCode"%>
<%@page import="com.khaosstar.herodeo.model.actions.RegisterAction"%>
<%@page import="com.khaosstar.herodeo.model.actions.LoginAction"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
	<Title>HeroDeo Login</Title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/popups.css" />
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
	<link href="http://fonts.googleapis.com/css?family=Arvo" rel="stylesheet" type="text/css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<script src="js/validation.js"></script>
	<script src="js/jquery.flip.min.js"></script>
	</head>
	<%  
	if (session != null && session.getAttribute("user_info") != null ) {
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/mainlobby.jsp");
		rd.forward(request, response);
		return;
	}
			
	 Integer i = (Integer)session.getAttribute("login_attempt");
	if ( i == null) {
		i = 0;
	}%>
		
	<body>
	<header>
		<div id="leftPanel" class="slidingPanel"><h1 class="slidingPanelText" id="oasis" style="padding-right: 30px; text-align: center;">HERODEO</h1></div>
		<div id="journey"><h2>l'aventure commence</h2></div>	
	</header>

		<div id="forms">
			<form id="loginForm" action="go" method="post">
				<fieldset id="inputs">
					<input id="emailInput" type="text" placeholder="Courriel" maxlength="30"  name="email" autofocus required>
					<input id="passwordInput" type="password" maxlength="12" name="pw" placeholder="Mot de passe" required>
				</fieldset>
				<fieldset id="actions">
					<div id="loginBt" class="loginLabels">Log In</div>
					<div id="signupBt" class="loginLabels">Sign up &#33;</div>
				</fieldset>
			<input type="hidden" name="action" value="<%=LoginAction.class.getSimpleName()%>" />
			</form>
			
		<form id="registerForm" action="go" method="post">
				<fieldset id="inputs">
					<label>Email :</label><input id="emailInputRegister" type="text" maxlength="30"  name="email" autofocus ><div id="validationCheckEmail"></div>  
					<label>Nickname :</label><input id="nickInputRegister" type="text" maxlength="30"  name="nickname" > 
					<label>Firstname :</label><input id="firstnameInputRegister" type="text" maxlength="30"  name="firstname">
					<label>Lastname :</label><input id="lastnameInputRegister" type="text" maxlength="30"  name="lastname" autofocus >
					<label>Password :</label><input id="passwordInputRegister" type="password" maxlength="12" name="pw" ><div id="validationCheckPW"></div>  
					<label>Confirm password :</label><input id="passwordInputRegister2nd" type="password" maxlength="12" >
				</fieldset>
				<fieldset id="actions">
					<input type="submit" id="continuerBt" class="button gray" value="Continuer">
				</fieldset>
			<input type="hidden" name="action" value="<%=RegisterAction.class.getSimpleName()%>" />
			</form>
	</div>
	<% if (session != null && (i > 0 && i < 3)) {%>
	<div id="loginErrorBox" class="boxRed">The email or password you have entered is invalid</div>
	<%} else if (i > 3) {%>
	<div id="loginErrorBox" class="boxRed">Did you forget your password ?</div>
	<%}%>
	<div id="spacer"></div>     
			
	       


	</body>
		
</html>