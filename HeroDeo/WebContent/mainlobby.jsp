<%@page import="com.khaosstar.herodeo.model.entities.WSUser"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.khaosstar.herodeo.model.actions.CreateLobbyAction"%>
<%@page import="com.khaosstar.herodeo.model.actions.LogoutAction"%>
<%@page import="com.khaosstar.herodeo.model.entities.User"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<% 
WSUser connectedUser = null;
HashMap<String, WSUser> hashOnlineUsers;
String userNickname;
Boolean badRoomPassword = false;

if (session == null || session.getAttribute("user_info") == null ) {
	RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
	rd.forward(request, response);
}
else {
	
	connectedUser = (WSUser)session.getAttribute("user_info");
	userNickname = connectedUser.getNickname();
	hashOnlineUsers = (HashMap<String, WSUser>)getServletContext().getAttribute("hashOnlineUsers");
	if(request.getAttribute("badroompassword") != null && (String)request.getAttribute("badroompassword") == "true" ) {
		badRoomPassword = true;
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>OASIS - Main Lobby</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/lobby.css" />
<link rel="stylesheet" href="css/main.css" />
<link rel="stylesheet" href="css/popups.css" />
<link href="Styles/msgBoxLight.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/global.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="js/jquery.msgBox.js" type="text/javascript"></script>
<script src="js/websocket.js" type="text/javascript"></script>
<script src="js/lobby.js" type="text/javascript"></script>

<script>
var userNickname = "<%=userNickname%>";
var badRoomPassword = "<%=badRoomPassword%>"
</script>
<title>Insert title here</title>
</head>
<body>
<header style="height: auto;">
	<nav id="mainNav" class="clearfix">
		<div id="mainLogo">OASIS</div>
		
			<ul>
				<li class="menuItems" id="createBt">create</li>
				<li class="menuItems">join</li>
				<li class="menuItems gearsIcon"></li>
			</ul>
		
	</nav>
	<div class="gradient"></div>
	<div id="popupMain" class="textPopup">
           <form id="createGameForm" action="go" method="post">
           <input type="hidden" name="action" value="<%=CreateLobbyAction.class.getSimpleName()%>" >
			<fieldset id="inputs">
				<label>Nom :</label><input id="nameInputRegister" type="text" maxlength="20"  name="roomName" autofocus >
				<label>Mot de passe :</label><input id="passwordInputRegister" type="password" maxlength="12" name="pw" > 
				<label>Confirmation du mot de passe :</label><input id="passwordInputRegister2nd" type="password" maxlength="12" >
			</fieldset>
			<fieldset id="actions">
				<input type="submit" class="button gray" value="Continuer">
			</fieldset>
			<input type="hidden" name="adminUUID" value="<%=connectedUser.getUUID()%>"/>
		</form>
		<form id="settingsForm" action=""> 
			<input id="checkBoxNewGamePopup" type="checkbox" checked="checked" name="popupVisible" value="popup"><Label for="popup">Show notifications</Label>
			<input type="hidden" name="action" value="<%=LogoutAction.class.getSimpleName()%>" />
			<input type="hidden" name="nickname" value="<%=connectedUser.getNickname()%>" />
			<button id="logoutBt" type="Submit">Deconnexion</button>
		</form>
		
		 <div id="onlineRooms">
			<ul>
			</ul>
		</div>
		
	</div>
	
</header>
<div class="wrapper">
		<div id='popupGame' class="popup north">
		<ul><li></li></ul>
		</div>
       <div id="chatWindow">
	      <div id="message">
	     	<ul>
	     	
	     	</ul>
	     </div>
		 <div id="onlineUsers">
	    	<h2>Utilisateur en ligne</h2>
			<ul>
			
			</ul>
		</div>
		 <input id="chatBar" type="text" value=""/>    
    </div>   

 </div>
 
 <form style="height: 0px; width: 0px;"id="connectLobbyForm" action="go" method="post">
		 	<input id="inputLobbyName" name="roomName" type="hidden" value=""/>
		 	<input id="inputLobbyPW" name="pw" type="hidden" value=""/>
		 	<input name="action" type="hidden" value="<%=CreateLobbyAction.class.getSimpleName()%>"/>
</form>
</body>
</html>
<%}%>
