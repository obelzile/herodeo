
function replacer(key, value) {
    if (typeof value === 'number' && !isFinite(value)) {
        return String(value);
    }
    return value;
}

//*******WSocketChannel*********
function WSocketChannel(websocket,c) {
	this.actions = {};
	this.ws = websocket;
	this.channel = c;	
	this.onopen = null;
	this.onmessage = null;
	this.onclose = null;
	this.onerror = null;
}

WSocketChannel.prototype.sendMessage = function(action,params) {
	if(this.ws.readyState != 1)
		return;
		
	var actionObj = {};
	actionObj.action = action;
	actionObj.params = params;
	
	var jsonObj = {};
	jsonObj.channel = this.channel;
	jsonObj.params = actionObj;
	
	var jsonString = JSON.stringify(jsonObj,replacer);
	
	this.ws.send(jsonString);
};

WSocketChannel.prototype.registerAction = function(action,callback) {
	if(action === "") {
		throw new Error("Invalide channel name");
	}
	
	if(!this.actions.hasOwnProperty(action)) {			
		this.actions[action] = callback;
	}		
};

WSocketChannel.prototype.processMessage = function(data) {
	if(!data.hasOwnProperty("action") && !data.hasOwnProperty("params")) {
		throw new Error("Error in response format " + JSON.stringify(data, replacer));
	}
	
	if(!this.actions.hasOwnProperty(data.action)) {
		//invalid action
	} else {
		//call the action callback
		this.actions[data.action](data.params);
	}
};

//*******WSocketChannel*********


//*******WSocket*********
function WSocket(url) {
	this.url = url;
	this.channels = {};
	this.ws = new WebSocket(url);
	
	this.ws.onclose = function(e) {
		
		for(var channel in this.channels) {
			if(!(this.channels[channel].onclose === undefined)) {
				this.channels[channel].onclose();
			}
		};
		
		console.log("onclose " + e);		
	};
	
	this.ws.onerror = function(e){
		
		for(var channel in this.channels) {
			if(!(this.channels[channel].onerror === undefined)) {
				this.channels[channel].onerror();
			}
		};
		
		console.log("onerror " + e);		
	};
}

WSocket.prototype.processOpen = function(e) {
	for(var channel in this.channels) {
		if(!(this.channels[channel].onopen === null)) {
			this.channels[channel].onopen();
		}
	};
	
	console.log("onOpen " + e);	
};

WSocket.prototype.processMessage = function(data) {
	if(!data.hasOwnProperty("channel") && !data.hasOwnProperty("params")) {
		throw new Error("Error in response format");
	}
	
	for(var channel in this.channels) {
		if(channel == data.channel) {
			if(!(this.channels[channel].onmessage === null)) {
				this.channels[channel].onmessage(data.params);
			}
			this.channels[channel].processMessage(data.params);
		}
	};		
};

WSocket.prototype.processClose = function(e) {
	for(var channel in this.channels) {
		if(!(this.channels[channel].onclose === null)) {
			this.channels[channel].onclose();
		}
	};
	
	console.log("onclose " + e);	
};

WSocket.prototype.processError = function(e) {
	for(var channel in that.channels) {
		if(!(that.channels[channel].onerror === null)) {
			that.channels[channel].onerror();
		}
	};
	
	console.log("onerror " + e);	
};

WSocket.prototype.registerChannel = function(channel) {
	if(channel === "") {
		throw new Error("Invalide channel name");
	}
	
	if(!this.channels.hasOwnProperty(channel)) {			
		this.channels[channel] = new WSocketChannel(this.ws,channel);
	}	
	return this.channels[channel];
};

WSocket.prototype.closeChannel = function(channel) {
	if(channel === "") {
		throw new Error("Invalide channel name");
	}
	
	if(this.channels.hasOwnProperty(channel)) {			
		this.channels[channel] = null;
	}	
};

WSocket.prototype.getChannel = function(channel) {
	return this.channels[channel];
};
//*******WSocket*********

//*******WSockets*********
var WSockets = (function() {
	
	var wsConnections = {};	
	
	return {
		open:function(url,channel) {
			
			if(url === null || url === "") {
				throw new Error("Invalid url");
			} 
			
			if(wsConnections.hasOwnProperty(url)) {
				wsConnections[url].registerChannel(channel);
			} else {
				wsConnections[url] = new WSocket(url);
				wsConnections[url].ws.onopen = onOpen;
				wsConnections[url].ws.onmessage = onMessage;
				wsConnections[url].ws.onclose = onClose;
				wsConnections[url].ws.onerror = onError;
				wsConnections[url].registerChannel(channel);
			}			

			return wsConnections[url].getChannel(channel);
		},
		getConnections:function() {
			return wsConnections;
		}
	
	};
	
	function onOpen(e) {
		console.log('onopen');
		if(wsConnections.hasOwnProperty(this.url)) {
			wsConnections[this.url].processOpen(e);
		}			
	}
	
	function onMessage(e){		
		if(wsConnections.hasOwnProperty(this.url)) {
			data = JSON.parse(e.data);
			wsConnections[this.url].processMessage(data);
		}		
	};
	
	function onClose(e) {
		console.log('onclose');
		
		if(wsConnections.hasOwnProperty(this.url)) {
			data = JSON.parse(e.data);
			wsConnections[this.url].processClose(data);
		}	
	};
	
	function onError(e){
		if(wsConnections.hasOwnProperty(this.url)) {
			data = JSON.parse(e.data);
			wsConnections[this.url].processError(e);
		}		
	};	
	
})();
//*******WSockets*********
