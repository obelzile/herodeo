
var chat;
var game;

$( function() {
	
	onLoad();
	
	  $("#chatBar").keypress( function(e) {
		  var message = $( "#chatBar" ).val( );
		  if (message.length > 0 ) {
			  if(e.which == 13) {				  
				  sendMessage();
				  $(this).val("");
				  $(this).focus();	
			  }
		  }
	  });
	  
	  $("#logoutBt").click( function() {
		  leaveRoom();
		  $("#connectLobbyForm").submit();
	  });
	  
});

function onNewUser(e) {
	console.log("onNewUser: " + JSON.stringify(e, replacer));
	$("#onlineUsers").find("ul").append("<li>"+e.nickname+"</li>");
}

function onUserQuit(e) {
	console.log("onUserQuit: " + JSON.stringify(e, replacer));
	$("#onlineUsers ul li:contains('" + userNickname + "')").fadeOut("slow", function () {
		$(this).remove();
	});
}

function onChatMessage(e) {
	console.log("onChatMessage: " + JSON.stringify(e, replacer));
	if (userNickname == e.nickname) {
		$("#message").find("ul").append("<li class='red'>"+e.nickname + " : " + e.message+"</li>");
	}
	else {
		$("#message").find("ul").append("<li>"+e.nickname + " : " + e.message+"</li>");
	}
}

function onLoad() {


	chat = WSockets.open("ws://"+ip+"/HeroDeo/goWS?UUID=" + useruuid,"chat");
	//chat = WSockets.open("ws://localhost:8080/HeroDeo/goWS?UUID=" + useruuid,"chat");
	chat.onopen = onConnect;
	//alert(userUUID);
	chat.registerAction("chatmessage",onChatMessage);
	chat.registerAction("newroomuser", onNewUser);
	chat.registerAction("userquit", onUserQuit);
	//game = WSockets.open("ws://localhost:8080/HeroDeo/goWS","game");	
	//game.onmessage = onGameMessage;
}

function onConnect() { 
	
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	chat.sendMessage("ConnectToRoom",params);	
}

function sendMessage() {	
	var params = {};
	
	params.message = $("#chatBar").val();
	params.roomuuid = roomuuid;
	
	chat.sendMessage("ChatMessage",params);

	//game.sendMessage(message);
}

function leaveRoom() {
	var params = {};
	
	params.nickname = userNickname;
	params.roomuuid = roomuuid;
	
	chat.sendMessage("leaveRoom", params);
	
}
