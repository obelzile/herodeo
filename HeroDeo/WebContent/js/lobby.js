var UUID;
var nickname;
var chat;
var game;
var userSelected = null;
var arePopupAllowed = false;
var lastMenuItemSelected;


UUID =  readCokie("UUID");

$(function() {
	
	onLoad();
	$("#popupMain").hide();
	
	if (badRoomPassword === "true") {
		$.msgBox({
		    title: "Sorry...",
		    content: "Bad password!",
		    autoclose: true,
		    type: "error",
		    showButtons:false,
		    opacity:0.8
		});
	}
	
	$(".kickPlayer").click(function(){
		var userNickname = $(this).parent().find("input").val();
		var params = {};
		params.nickname = userNickname;
		chat.sendMessage("KickUser", params);	
	});
	
	
	nickname = $('input[name=nickname]').val();

	bindEvents();
	
  setTimeout( function() {
	  arePopupAllowed = true;
  }, 5000);  
});

function onNewUser(e) {
	console.log("onNewUser: " + JSON.stringify(e, replacer));
	if (!$('#onlineUsers ul li').is(':contains('+e.nickname+')')){
		$("#onlineUsers").find("ul").append("<li>"+e.nickname+"</li>");
	} else if(arePopupAllowed === true){
		$("#popupGame ul li").text(e.nickname + " has joined the lobby");
		$("#popupGame").hide()
			.fadeIn()
			.delay(2000)
			.fadeOut('slow');
	}
}

function onUserQuit(e) {
	console.log("onUserQuit: " + JSON.stringify(e, replacer));
	$("#onlineUsers ul li:contains('" + e.nickname + "')").fadeOut("slow", function () {
		$(this).remove();
	});
}

function onChatMessage(e) {
	console.log("onChatMessage: " + JSON.stringify(e, replacer));
	if (userNickname == e.nickname) {
		$("#message").find("ul").append("<li class='red'>"+e.nickname + " : " + e.message+"</li>");
	}
	else {
		$("#message").find("ul").append("<li>"+e.nickname + " : " + e.message+"</li>");
	}
}

function onNewRoomCreated(e) {
	console.log("onNewRoomCreated: " + JSON.stringify(e, replacer));
	liclass = "";
	
	if (e.passwordPresent == "true") {
		liclass = "password";
	}
	
	$("#onlineRooms").find("ul").append("<li class='"+liclass+"'>"+e.roomname+"</li>");
	
	if ( arePopupAllowed === true) {
	$("#popupGame ul li").text("A new lobby has been created: " + e.roomname );
	$("#popupGame").hide()
		.fadeIn()
		.delay(2000)
		.fadeOut('slow');
	}
	
	if (liclass == "password") {
		$("#onlineRooms ul li").on('click', function (event) {
			var currentLi = $(this);
			$.msgBox({ type: "prompt",
			    title: "Salle priv�e",
			    inputs: [
			    { header: "Mot de passe", type: "password", name: "password" },],
			    buttons: [
			    { value: "Continuer" }, {value:"Annuler"}],
			    success: function (result, values) {
		        	if(result === "Continuer") {
			    	$(values).each(function (index, input) {
			       
			            $("#inputLobbyName").val(currentLi.text());
						$("#inputLobbyPW").val(input.value);
						$("#connectLobbyForm").submit();
						leaveRoom();
			        });
		        	}
			    }
			});
			
		});	
	}
	else {
		$("#onlineRooms ul li").on('click', function (event) {
			$("#inputLobbyName").val($(this).text());
			$("#inputLobbyPW").val("");
			$("#connectLobbyForm").submit();
			leaveRoom();
		});	
	}
}

function onEmptyRoom(e) {
	console.log("onRoomEmpty: " + JSON.stringify(e, replacer));
	$("#onlineRooms ul li:contains('"+e.roomname+"')").remove();
}

function onLoad() {

	chat = WSockets.open("ws://"+ip+"/HeroDeo/goWS?UUID=" + UUID,"chat");
	chat.onopen = onConnect;
	chat.registerAction("chatmessage",onChatMessage);
	chat.registerAction("newroomuser", onNewUser);
	chat.registerAction("userquit", onUserQuit);
	chat.registerAction("onlineuserlist", onNewUser);
	chat.registerAction("onlinelobbieslist", onNewRoomCreated);
	chat.registerAction("newroomcreated", onNewRoomCreated);
	chat.registerAction("roomempty", onEmptyRoom);
	
	chat.registerAction("userkicked", onUserKicked);
	chat.registerAction("userquit", onUserQuit);
	//game = WSockets.open("ws://localhost:8080/HeroDeo/goWS","game");	
	//game.onmessage = onGameMessage;
}


function onUserKicked(e) {
	console.log("onUserKicked: " + JSON.stringify(e, replacer));
	$("#connectLobbyForm").submit();
}

function onUserQuit(e) {
	console.log("onUserKicked: " + JSON.stringify(e, replacer));
	$("body").find(e.nickname).val("");
}

function onConnect() { 
	
	params = {};
	params.roomuuid = "default";
	chat.sendMessage("ConnectToRoom",params);	
}

function sendMessage() {	
	var params = {};
	params.roomuuid = "default";
	params.message = $("#chatBar").val();
	
	if(userSelected !== null ) {
		params.recipient = userSelected.text();
	}
	
	chat.sendMessage("ChatMessage",params);

	//game.sendMessage(message);
}

function leaveRoom() {
	var params = {};
	params.roomuuid = "default";
	params.nickname = userNickname;
	
	chat.sendMessage("leaveRoom", params);	
}

function readCokie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function bindEvents() {
	
	  $("#onlineUsers ul").on('click', 'li', function(e) { 
			userSelected = $(this);
			if (userSelected.hasClass('userSelected')){
				userSelected.removeClass('userSelected');
				userSelected = null;
			}
			else {
				userSelected.addClass("userSelected").siblings().removeClass("userSelected");
			}
		});
	  
	  $("#mainNav ul li").on('click', function(e) {
		 var menuItemSelected = $(this);
		 
		  if(menuItemSelected.hasClass('menuItemSelected')){
			  menuItemSelected.removeClass('menuItemSelected');
			  $("#popupMain").slideUp('fast');
		  } else {
			  menuItemSelected.addClass("menuItemSelected");
			  menuItemSelected.siblings().removeClass('menuItemSelected');
			  $("#popupMain").slideDown('fast');
			
			  switch (menuItemSelected.text()) {
			  case 'create':
				  $("#popupMain").find("#createGameForm").show().siblings().hide();
				  break;
				  
			  case 'join':
				  $("#popupMain").find("#onlineRooms").show().siblings().hide();
				  break;
			  default:
				  $("#popupMain").find("#settingsForm").show().siblings().hide();
				  break;
			  }
		  }  

		lastMenuItemSelected = menuItemSelected;
	  });
			 
	  $("#popupMain").on('click', '.submit', function(e){ 
		  e.preventDefault();
		  $.post('go', $('#createGameForm').serialize());
		  console.log($('#createGameForm').serialize());
	  });
	  
	  $("#popupMain").on('click', '#checkBoxNewGamePopup' , function(){ 
		  var checkBox = $(this);
		  
		  if (checkBox.is(":checked")){
			  arePopupAllowed = true;
		  } else {
			  arePopupAllowed = false;
		  }
		 // console.log("Val de popupallowed" + arePopupAllowed);
	  });
	  
	  $("#chatBar").keypress( function(e) {
		  var message = $( "#chatBar" ).val( );
		  if (message.length > 0 ) {
			  if(e.which == 13) {
				  	sendMessage();
	                $(this).val("");
	                $(this).focus();
			  }
		  }
	  });
	  
	  
}


