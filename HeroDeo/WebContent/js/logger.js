
var Logger = (function(){
	
	var loggerlevel = 0;
	
	var setLevel = function(level) {
		loggerlevel = level;
		
		var dt = new Date();	
		console.log(dt.toLocaleTimeString() + " - Logger.setLevel: " + level);
	};
	
	var log = function(level,message) {
		if(loggerlevel >= level) {
			var dt = new Date();				
			console.log(dt.toLocaleTimeString() + " - " + message);
		}
	};
	
	return {
		enum: Object.freeze({"notice":1, "warning":2, "error":3}),
		setLevel: setLevel,
		log : log
	};
})();