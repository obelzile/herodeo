function Unit(data) {
	this.id = data.id;
	this.type = data.type;
	this.uuid = data.uuid;
	this.name = data.name;
	this.position = data.position;
	this.currentPV = data.currentPV;
	this.maxPV = data.maxPV;
	this.strength = data.strength;
	this.protection = data.protection;
	this.maxMovement = data.maxMovement;
	this.stepLeft = data.stepLeft;
	this.attackTargets = data.attackTargets;
	this.entity = null;	
		
	this.dispose = function() {	
		if(this.entity != null) {
			this.entity.disposeTooltip();
			this.entity.destroy();
		}
	};
}

Unit.prototype.update = function(data) {
	this.id = data.id;
	this.type = data.type;
	this.uuid = data.uuid;
	this.name = data.name;
	this.position = data.position;
	this.currentPV = data.currentPV;
	this.maxPV = data.maxPV;
	this.strength = data.strength;
	this.protection = data.protection;
	this.maxMovement = data.maxMovement;
	this.stepLeft = data.stepLeft;
	this.attackTargets = data.attackTargets;
	if(this.entity != null) {
		this.entity.setTooltip(this.name);
		this.entity.x = this.position.x * TILE_SIZE;
		this.entity.y = this.position.y * TILE_SIZE;		
	}		
};

