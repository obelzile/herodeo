var playerInfoEntities = new Array(4);
var currentTurn = null;


Crafty.sprite(300, 128, "images/game/playerinfoborder.png", {
	playerinfo_border:[0,0]
});
Crafty.sprite(55, 100, "images/game/previous.png", {
	button_previous:[0,0]
});
Crafty.sprite(55, 100, "images/game/next.png", {
	button_next:[0,0]
});
Crafty.sprite(125, 50, "images/game/choose-button1.png", {
	button_choose:[0,0]
});

function setSelectedPlayerInfo(index) {
	for(var i in playerInfoEntities) {
		//playerInfoEntities[i].showBorder(false);		
	}
	if(index >= 0 && index < playerInfoEntities.length)
		playerInfoEntities[index].showBorder(true);
}

var heroid = 0;

Crafty.scene("herosselectionwait",function() {
	
	Crafty.background("white");
	
	Crafty.e("2D, DOM, Text").attr({ x: 200, y: 100, w: 200 }).text("Veuillez attendre pendant que x choisis sont hero");
	
	//currentTurn = Crafty.e("2D, Canvas, Color,Text")
	//.text("Turn : " + oasis.turn)
	//.color("#FFFFFF")
	//.attr({x:640, y:0,z:0,w:300,h:20});
	
	//playerInfoEntities[0] = new PlayerInfo(640,20,"#FF0000",0);
	//playerInfoEntities[1] = new PlayerInfo(640,148,"#00FF00",1);
	//playerInfoEntities[2] = new PlayerInfo(640,276,"#FFD800",2);
	//playerInfoEntities[3] = new PlayerInfo(640,404,"#0000FF",3);
	
});

Crafty.scene("heroselection",function() {
	
	Crafty.background("white");
	
	Crafty.e("DOM, 2D, hero_"+heroesdata[heroid].id).attr({x:150, y:100});
	
	Crafty.e("2D, DOM, Text").attr({ x: 240, y: 100, w: 200 }).text(heroesdata[heroid].name);
	Crafty.e("2D, DOM, Text").attr({ x: 240, y: 120, w: 200 }).text("Max. PV : " + heroesdata[heroid].maxPV);
	Crafty.e("2D, DOM, Text").attr({ x: 240, y: 140, w: 200 }).text("Force : " + heroesdata[heroid].strength);
	Crafty.e("2D, DOM, Text").attr({ x: 240, y: 160, w: 200 }).text("Protection : " + heroesdata[heroid].protection);
	Crafty.e("2D, DOM, Text").attr({ x: 240, y: 180, w: 200 }).text("Movement : " + heroesdata[heroid].movementType);
		
	Crafty.e("DOM, 2D, Mouse, button_previous")
	.attr({x:50, y:161})
	.bind("Click", function(e){
		heroid--;
		if(heroid < 0)
			heroid = heroesdata.length-1;
		Crafty.scene("heroselection");	
	});
	
	Crafty.e("DOM, 2D, Mouse, button_next")
	.attr({x:407, y:161})
	.bind("Click", function(e){
		heroid++;
		if(heroid >= heroesdata.length)
			heroid = 0;
		Crafty.scene("heroselection");
	});
	
	Crafty.e("DOM, 2D, Mouse, button_choose")
	.attr({x:190, y:210})
	.bind("Click", function(e){
		
		params = {};
		params.roomuuid = roomuuid;
		params.heroid = heroid;
		
		game.sendMessage("SelectHero",params);
	});
	
	//currentTurn = Crafty.e("2D, Canvas, Color,Text")
	//.text("Turn : " + oasis.turn)
	//.color("#FFFFFF")
	//.attr({x:640, y:0,z:0,w:300,h:20});
	
	//playerInfoEntities[0] = new PlayerInfo(640,20,"#FF0000",0);
	//playerInfoEntities[1] = new PlayerInfo(640,148,"#00FF00",1);
	//playerInfoEntities[2] = new PlayerInfo(640,276,"#FFD800",2);
	//playerInfoEntities[3] = new PlayerInfo(640,404,"#0000FF",3);
});

Crafty.c("PlayerInfo", {
	x : 0,
	y : 0,
	nickname : "",
	heroid : 0,
	color : "white",
	
	init : function() {		
	},
	PlayerInfo : function(positionx,positiony,name,id,c) {
		x = positionx;
		y = positiony;
		nickname = name;
		heroindex = id;
		color = c;		
	},
	createEntities : function() {
		
		this.color(color);
		
		Crafty.e("DOM, 2D, hero_"+heroid).attr({x:x, y:y});
		
		Crafty.e("2D, DOM, Text").attr({ x: x + 85, y: y + 10, w: 200 }).text(heroesdata[heroid].name);
		Crafty.e("2D, DOM, Text").attr({ x: x + 85, y: y + 30, w: 200 }).text("Max. PV : " + heroesdata[heroid].maxPV);
		Crafty.e("2D, DOM, Text").attr({ x: x + 85, y: y + 50, w: 200 }).text("Force : " + heroesdata[heroid].strength);
		Crafty.e("2D, DOM, Text").attr({ x: x + 85, y: y + 70, w: 200 }).text("Protection : " + heroesdata[heroid].protection);
		Crafty.e("2D, DOM, Text").attr({ x: x + 85, y: y + 90, w: 200 }).text("Bonus Movement : " + heroesdata[heroid].movementBonus);
	}	
});

function selectHero(heroId) {
	params = {};
	params.heroid = heroid;
	
	game.sendMessage("SelectHero",params);	
}


//WS functions

function onHeroSelected(data) {
	Logger.log(Logger.enum.notice,"onChooseHero");
	
	oasis.updatePlayer(data);
	
	//playerInfoEntities[data.playerindex].update();
}

function onPlayerIndexChanged_heroSelection(data) {
	Logger.log(Logger.enum.notice,"onPlayerIndexChanged_heroSelection");
	oasis.setCurrentPlayerIndex(data);	
}
