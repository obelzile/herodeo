function GameControls(data) {
	this.controlingmonster = data.controlingmonster;
	this.monstercanmove = data.monstercanmove;
	this.monstercanattack = data.monstercanattack;
	this.monstercanskip = data.monstercanskip;
	
	this.canmove = data.canmove;
	this.canattack = data.canattack;
	this.candrink = data.candrink;
	this.candropcoin = data.candropcoin;
	this.canopenchest = data.canopenchest;
	this.cancastspell = data.cancastspell;
	this.cancastnovicefree = data.cancastnovicefree;
	this.cancasefreespell = data.cancasefreespell;
	this.canvisitmerchant = data.canvisitmerchant;	
	this.cancontrolmonster = data.cancontrolmonster;	
	this.canendturn = data.canendturn;
		
	this.update = function(data) {		
		this.controlingmonster = data.controlingmonster;
		this.monstercanmove = data.monstercanmove;
		this.monstercanattack = data.monstercanattack;
		this.monstercanskip = data.monstercanskip;
		
		this.canmove = data.canmove;
		this.canattack = data.canattack;
		this.candrink = data.candrink;
		this.candropcoin = data.candropcoin;
		this.canopenchest = data.canopenchest;
		this.cancastspell = data.cancastspell;
		this.cancastnovicefree = data.cancastnovicefree;
		this.cancasefreespell = data.cancasefreespell;
		this.canvisitmerchant = data.canvisitmerchant;	
		this.cancontrolmonster = data.cancontrolmonster;	
		this.canendturn = data.canendturn;
	};
}

GameControls.prototype.disableControls = function() {
	enableButton($("#move_monster_button"),false);		
	enableButton($("#monster_attack_button"),false);
	enableButton($("#monster_end_control_button"),false);
	
	enableButton($("#move_button"),false);				
	enableButton($("#attack_button"),false);
	enableButton($("#boire_button"),false);
	enableButton($("#jeterpiece_button"),false);
	enableButton($("#ramassermarqueur_button"),false);
	enableButton($("#sortilege_button"),false);
	enableButton($("#sortilege_novice_button"),false);
	enableButton($("#sortilege_gratuit_button"),false);		
	enableButton($("#visite_marchand_button"),false);						
	enableButton($("#controler_monster_button"),false);			
	enableButton($("#endturn_button"),false);	
};

GameControls.prototype.updateControls = function(data) {
	if(data !== undefined)
		this.update(data);
	
	if(oasis.playerindex != oasis.myindex || oasis.gamestate != 2) {
		$("#control-ui").invisible();
	} else {		
		player = oasis.units.getUnit(oasis.myuuid);
		
		$("#control-ui").visible();
		
		if(this.controlingmonster) {
			var monstermoving = false;
			if(oasis.monstercontrol !== null && oasis.monstercontrol.monstermoving) {
				var monster = oasis.units.getUnit(oasis.monstercontrol.monsteruuid);
				$("#monster-movement-count").text(monster.stepLeft);			
				$("#move_monster_done_button").show();
				$("#move_monster_button").hide();
				monstermoving = true;
			} else {
				$("#move_monster_done_button").hide();
				$("#move_monster_button").show();			
			}		

			enableButton($("#move_monster_button"),this.monstercanmove);		
			enableButton($("#monster_attack_button"),this.monstercanattack && !monstermoving);
			enableButton($("#monster_end_control_button"),this.monstercanskip && !monstermoving);
			
			$("#monster-control").show();
			$("#player-control").hide();
						
		} else {
			$("#monster-control").hide();
			$("#player-control").show();
		}		
	

		if(oasis.moving) {
			$("#movement-count").text(oasis.me.stepLeft);
			$("#move_done_button").show();
			$("#move_button").hide();
		} else {
			$("#move_done_button").hide();
			$("#move_button").show();			
		}
		
		if(oasis.monstercontrol !== null && oasis.monstercontrol.monstermoving) {
			var monster = oasis.units.getUnit(oasis.monstercontrol.monsteruuid);
			$("#monster-movement-count").text(monster.stepLeft);			
			$("#controler_monster_done_button").show();
			$("#controler_monster_button").hide();
		} else {
			$("#controler_monster_done_button").hide();
			$("#controler_monster_button").show();			
		}		
				
		enableButton($("#move_button"),this.canmove && !oasis.moving);				
		enableButton($("#attack_button"),this.canattack && !oasis.moving);
		
		enableButton($("#boire_button"),this.candrink && !oasis.moving);
		enableButton($("#jeterpiece_button"),this.candropcoin && !oasis.moving);
		enableButton($("#ramassermarqueur_button"),this.canopenchest && !oasis.moving);
		
		enableButton($("#sortilege_button"),this.cancastspell && !oasis.moving);
		enableButton($("#sortilege_novice_button"),this.cancastnovicefree && !oasis.moving);
		enableButton($("#sortilege_gratuit_button"),this.cancasefreespell && !oasis.moving);		
		enableButton($("#visite_marchand_button"),this.canvisitmerchant && !oasis.moving);						
		enableButton($("#controler_monster_button"),this.cancontrolmonster && !oasis.moving);		
		
		enableButton($("#endturn_button"),this.canendturn && !oasis.moving);		
	}
};