
//Merchant window handler
var Merchant = (function () {
	
	var _x = 0;
	var _y = 0;
	var _w = 0;
	var _h = 0;
	var _shopitems = [];
	var _selector = "";
	var _visible = false;
	
	/**
	* add the item from the shopitems array to the list 
	*
	* @method renderItemList
	*/
	function renderItemList() {
		var itemlist = $("#merchant-items");
		itemlist.find("li").remove();
		
		//Build the li list with the shopItems list
		for(var index in _shopitems) {
			var li = $("<li class='' id='" + _shopitems[index].id + "'>" +
						"<div class='image'><img width='40px' height='40px' src='images/equipments/" + _shopitems[index].image + "'/></div>" +
						"<div class='info'>" +
						"<p class='name'>" + _shopitems[index].name + "</p>" +
						"<p class='description'>" + _shopitems[index].description + "</p>" +
						"</div>" +
						"<div class='cost'><h3>" + _shopitems[index].cost + "</h3></div>" +
						"</li>");
						
			if(!_shopitems[index].enabled)
				li.addClass("disabled");
			
			itemlist.append(li);
		}
		
		//enable or disable the buy button if any of the selected item are disabled
		itemlist.selectable({selected: function( event,ui) {
			if($("#merchant-items .ui-selected").hasClass("disabled"))						
			{
				$("#merchant-buy").attr("disabled", "disabled");	
			} else {
				$("#merchant-buy").removeAttr("disabled");							
			}	
		}});

		//gold ammount
		$("#player-gold").find("span").text(oasis.me.currentPO);
		
		if(_visible) {
			//update the scrollbar
			$(".merchant-scroll-wrapper").mCustomScrollbar("destroy");	
			$(".merchant-scroll-wrapper").mCustomScrollbar({
				theme:"dark"
			});	
		}
	}
	
	return {
		
		onBuyItem : null,	
		
		/**
		* My method description.  Like other pieces of your comment blocks, 
		* this can span multiple lines.
		*
		* @method init
		* @param {String} css selector
		*/
		init : function(selector) {
			_selector = selector;
			
			$(_selector).draggable();

			$("#merchant-close").click(function() {
				Merchant.hide();
			});
			
			$("#merchant-buy").click(function() {
				if(Merchant.onBuyItem != null) {
					
					var shoppingList = [];
					
					var items = $("#merchant-items li.ui-selected");
					for(var i=0;i<items.length;i++) {
					
						shoppingList[i] = $(items[i]).attr("id");
											
					}
					
					Merchant.hide();
					
					Merchant.onBuyItem(shoppingList);
				}
			});			
		},

		/**
		* My method description.  Like other pieces of your comment blocks, 
		* this can span multiple lines.
		*
		* @method show
		* @param {ShopItem[]} shopitems array of shopeitem  
		*/
		show : function(shopitems) {
			if (!$(_selector).length) {
				throw new Error(".init the merchant object first with the right div selector");
			}
			if(!_visible) {				
				
				_shopitems = shopitems;				
				
				renderItemList();
				
				$(_selector).show();
				_visible = true;
			
				//update the scrollbar
				$(".merchant-scroll-wrapper").mCustomScrollbar("destroy");	
				$(".merchant-scroll-wrapper").mCustomScrollbar({
					theme:"dark"
				});				
				
			}
		},
		
		/**
		* hide the merchant window  
		*
		* @method hide
		*/		
		hide : function() {
			if (!$(_selector).length) {
				throw new Error(".init the merchant object first with the right div selector");
			}
			$(_selector).hide();
			_visible = false;
		},
		
		/**
		* set items in the shop  
		*
		* @method setShopingList
		* @param {String} foo Argument 1
		*/		
		setShopingList : function(shopitems) {
			_shopitems = shopitems;
			
			renderItemList();
		}
	};	
})();
