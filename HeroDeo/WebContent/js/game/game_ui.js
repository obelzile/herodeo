$(function() {
	
	Choices.hide();
	
	$("#move_done_button").hide();
	$("#controler_monster_done_button").hide();	
	
	$("#spell-template").hide();
	
	$("#spells").hide();
	
	$("#spell-close").click(function() {
		oasis.spells.cancelSpell();
	});
	
	$("#combat").hide();	
	
	$("#merchant").hide();
	
	$(".sacredstone").hide();
	
	$(".selection-border").invisible();
	
	$("#control-ui").invisible();
	
	$("#move_button").click(function() {
		oasis.requestMoveState(true);
	});
	
	$("#move_done_button").click(function() {		
		oasis.requestMoveState(false);
	});	
		
	$("#controler_monster_button").click(function() {
		oasis.requestMonsterAction();
	});
	
	$("#move_monster_button").click(function() {		
		oasis.requestMonsterMovementState();
	});	
	
	$("#move_monster_done_button").click(function() {		
		oasis.finishMoveState();
	});
	
	$("#monster_attack_button").click(function() {		
		oasis.requestMonsterAttackState();
	});
	
	$("#monster_end_control_button").click(function() {		
		oasis.requestMonsterControlSkip();
	});

	$("#attack_button").click(function() {
		if(player.attackTargets.length == 0)
			return;
		
		if(player.attackTargets.length > 1) {
			
		} else if(player.attackTargets.length == 1) {
			oasis.initiateAttack(player.attackTargets[0]);
		}
	});		
	
	$("#boire_button").click(function() {
		oasis.drinkFromWell();
	});
	
	$("#jeterpiece_button").click(function() {
		oasis.throwCoinInWell();
	});
	
	$("#ramassermarqueur_button").click(function() {
		oasis.ramassermarqueur();
	});

	$("#visite_marchand_button").click(function() {
		oasis.visitemarchand();
	});	
		
	$("#sortilege_button").click(function() {
			
		params = {};
		params.roomuuid = roomuuid;
		params.roompw = pw;
		params.type = "";
		
		game.sendMessage("GetSpellList",params);
	});
	
	$("#sortilege_novice_button").click(function() {
		params = {};
		params.roomuuid = roomuuid;
		params.roompw = pw;
		params.type = "freenovice";
		
		game.sendMessage("GetSpellList",params);
	});
	
	$("#sortilege_gratuit_button").click(function() {
		params = {};
		params.roomuuid = roomuuid;
		params.roompw = pw;
		params.type = "free";
		
		game.sendMessage("GetSpellList",params);
	});		
	
	$("#endturn_button").click(function() {
		//setSelectedPlayerInfo(0);
		$("#control-ui").invisible();
		oasis.endTurn();
	});

	
});

function requestMonsterMovement(target) {
	position = {x:target.attr("x") / TILE_SIZE,y:target.attr("y") / TILE_SIZE};
	
	oasis.requestMonsterAction(position);
}


function testspell(params) {			
	cells = oasis.board.queryCellsOfTypes([11,12]);	//CellType.BlueEnergy CellType.RedEnergy
	i = 0;
	for(c in cells) {
		found = false;
		//check if a player in the position
		for(p in oasis.players) {
			if(cells[c].position.x == oasis.players[p].position.x && cells[c].position.y == oasis.players[p].position.y) {
				found = true;
			}
		}
		//check if a monster in the position
		for(m in oasis.monsters) {
			if(cells[c].position.x == oasis.monsters[m].position.x && cells[c].position.y == oasis.monsters[m].position.y) {
				found = true;
			}
		}									
		if(found == false) {
			params['position'][i] = cells[c].position;
			i++;
		}
	}	
}

function choiceCallback(obj) {
	console.log(obj.content);
}

function updateInfo() {
	updateTurn();
	updateControls();
	var players = oasis.units.getPlayers();
	for(var i=0;i<players.length;i++) {
		players[i].updateUI();
	}		
}

function updateTurn() {
	$("#turnvalue").text(oasis.turn);
}

function updateControls(params) {
/*	if(oasis.playerindex != oasis.myindex || oasis.gamestate != 2) {
		$("#control-ui").invisible();
	} else {		
		player = oasis.units.getUnit(oasis.myuuid);
		
		$("#control-ui").visible();
		
		if(oasis.moving) {
			$("#movement-count").text(oasis.me.stepLeft);
			$("#move_done_button").show();
			$("#move_button").hide();
		} else {
			$("#move_done_button").hide();
			$("#move_button").show();			
		}
		
		if(oasis.monstercontrol !== null && oasis.monstercontrol.monstermoving) {
			var monster = oasis.units.getUnit(oasis.monstercontrol.monsteruuid);
			$("#monster-movement-count").text(monster.stepLeft);			
			$("#controler_monster_done_button").show();
			$("#controler_monster_button").hide();
		} else {
			$("#controler_monster_done_button").hide();
			$("#controler_monster_button").show();			
		}		
				
		enableButton($("#move_button"),(!oasis.moved && !oasis.moving));
				
		enableButton($("#attack_button"),(player.attackTargets.length > 0) && oasis.attacked == false && !oasis.moving);
		
		enableButton($("#boire_button"),oasis.board.hasCellType(CellType.Well,player.position) && oasis.drank == false && player.currentPV < player.maxPV && !oasis.moving);
		enableButton($("#jeterpiece_button"),oasis.board.hasCellType(CellType.Well,player.position) && player.currentPO > 0 && !oasis.moving);
		enableButton($("#sortilege_button"),!oasis.moving);
		enableButton($("#sortilege_novice_button"),oasis.board.hasCellType(CellType.BlueEnergy,player.position) && !oasis.moving);
		enableButton($("#sortilege_gratuit_button"),oasis.board.hasCellType(CellType.RedEnergy,player.position) && !oasis.moving);		
		enableButton($("#visite_marchand_button"),oasis.board.hasAnyCellTypeOf([CellType.Start0,CellType.Start1,CellType.Start2,CellType.Start3,],player.position) && !oasis.moving);						
		//TODO treasure 
		enableButton($("#ramassermarqueur_button"),oasis.board.hasCellType(CellType.Chest,player.position) && !oasis.moving);
		enableButton($("#endturn_button"),!oasis.moving);
		
	}*/
}

function enableButton(button,value) {
	if(value == false) {
		button.attr("disabled","disabled");
		var src = button.find("img").attr("src");
		if(src.indexOf("-disabled") == -1) {
			src = src.substr(0,src.length-4);			
			src = src + "-disabled.png";
			button.find("img").attr("src",src);
		}		
	} else {
		if(button.attr("disabled","disabled")) {
			var src = button.find("img").attr("src");
			if(src.indexOf("-disabled") != 0) {
				src = src.replace("-disabled","");
				button.find("img").attr("src",src);
			}
		}
		button.removeAttr("disabled");
	}	
}

function setPlayerInfoBorder(index) {

	var player = oasis.units.getPlayer(index);
	var players = oasis.units.getPlayers();
	
	for(var i=0;i<players.length;i++) {
		players[i].unSelectUI();
	}
	
	player.selectUI();
}

function updatePlayerInfo(index) {
	$("#playerinfo-wrapper").show();
	
	$($(".nickname").get(index)).text(oasis.players[index].nickname);
	
	switch(index) {
	case 0:
		$($(".nickname-wrapper").get(index)).addClass("redbox");
		break;
	case 1:
		$($(".nickname-wrapper").get(index)).addClass("yellowbox");		
		break;		
	case 2:
		$($(".nickname-wrapper").get(index)).addClass("bluebox");		
		break;
	case 3:
		$($(".nickname-wrapper").get(index)).addClass("greenbox");		
		break;
	}
	$($(".nickname-wrapper").get(index))
	if(index == oasis.myindex) {
		$($(".nickname-wrapper").get(index)).find("img").show();
	}
	var heroId = oasis.players[index].heroId;
	if(heroId != -1) {	
		$($(".avatar-wrapper").get(index)).find("img").attr("src","images/avatars/avatar-" + oasis.players[index].heroId +".png");
		$($(".avatar-class").get(index)).text(heroesdata[heroId].name);
		$($(".strength").get(index)).text(oasis.players[index].strength + oasis.players[index].bonusStrength);
		if(oasis.players[index].bonusStrength > 0)
			$($(".strength").get(index)).addClass("attribute-bonus");
		$($(".bonusstrength").get(index)).text(oasis.players[index].bonusStrength);
		$($(".protection").get(index)).text(oasis.players[index].protection + oasis.players[index].bonusProtection);
		if(oasis.players[index].bonusProtection > 0)
			$($(".protection").get(index)).addClass("attribute-bonus");
		$($(".bonusprotection").get(index)).text(oasis.players[index].bonusProtection);
		$($(".bonusdeplacement").get(index)).text(oasis.players[index].bonusMovement);
		
		//$($("#movement-count").get(index)).text(oasis.players[index].movement);
		
		$($(".currentPV").get(index)).text(oasis.players[index].currentPV);
		$($(".maxPV").get(index)).text(oasis.players[index].maxPV);
		$($(".currentPM").get(index)).text(oasis.players[index].currentPM);
		$($(".currentPO").get(index)).text(oasis.players[index].currentPO);
		
		if(oasis.players[index].personalSpells.length == 3) {
			$($(".spell0").get(index)).text(oasis.players[index].personalSpells[0].name);
			$($(".spell1").get(index)).text(oasis.players[index].personalSpells[1].name);
			$($(".spell2").get(index)).text(oasis.players[index].personalSpells[2].name);
		}
				
		//Show correct ammount of stones in the game
		for(var i=0;i<oasis.players.length;i++) {
			$($($(".sacredstone-wrapper").get(index)).find(".sacredstone").get(i)).show();
		}
		
		for(var i=0;i<oasis.players.length;i++) {
			$($($(".sacredstone-wrapper").get(index)).find(".sacredstone").get(i)).addClass("sacredstone-nd");
		}
		//Show correct ammount of stones the player owns
		for(var i=0;i<oasis.players[index].sacredStone;i++) {
			$($($(".sacredstone-wrapper").get(index)).find(".sacredstone").get(i)).removeClass("sacredstone-nd");
		}
	}
}