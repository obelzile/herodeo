
//game over scene
Crafty.scene("gameover",function() {
	Logger.log(Logger.enum.notice,"gameover scene");
	Crafty.background("white");		

	if(oasis.players[oasis.myindex].sacredStone == oasis.players.length) {
		Crafty.e("2D, DOM, Text").attr({ x: 200, y: 100, w: 200 })
		.text("YOU WIN!!!")
		.textColor('#FF0000')
		.textFont({family: 'Arial', size: '36px', weight: 'bold' });		
	} else {
		Crafty.e("2D, DOM, Text").attr({ x: 200, y: 100, w: 200 })
		.text("Game Over")
		.textColor('#FF0000')
		.textFont({family: 'Arial', size: '36px', weight: 'bold' });		
	}

	Crafty.e("2D, DOM, Mouse,Image")
	.attr({x:185, y:160})
	.image("images/RedReturnButton.png")
	.bind("Click", function(e){
		$("#logoutBt").trigger('click');
	});
});