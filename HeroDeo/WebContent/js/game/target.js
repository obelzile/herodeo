/**
 * Target class handler showing an overlay over the gameboard 
 * and returning the position of the target clicked
 *
*/
var Target = function() {
	this.replyto = "";
	this.overlay = [];
	this.visible = false;
	this.targetclickCallback = null;
	this.oldOnKeyDown = null;
};

/**
 * Build the overlay and handle the click event
 *
 * @param filter filter params to query a target
 * @param callback function to callback on a successful target
*/
Target.prototype.queryTarget = function(filter,callback) {
	
	this.targetclickCallback = callback;
	var filteredPosition = [];
	var filteredCellType = [];
		
	if(filter.hasOwnProperty("position")) {
		filteredPosition = filter["position"];
	}

	if(filter.hasOwnProperty("celltype")) {
		filteredCellType = filter["celltype"];
	}
	
	if(this.visible == true) {
		Target.prototype.clearOverlay.call(this);
	}
	
	var show = false;
	
	for(var y=0;y<oasis.board.board.length;y++) {
		for(var x=0;x<oasis.board.board[y].length;x++) {
			if(!oasis.board.board[y][x].hasAnyCellTypeOf(filteredCellType) && !isFilteredPosition(x,y,filteredPosition)) {
				this.overlay[y*oasis.board.height+x] = Crafty.e("2D,Canvas,Color,Tint,Mouse")
				.color("#000000")
				.attr({x:x*TILE_SIZE,y:y*TILE_SIZE,w:TILE_SIZE,h:TILE_SIZE,z: 15})
				.tint("#000000", 0.5);
			} else {
				this.overlay[y*oasis.board.height+x] = Crafty.e("2D,Canvas,Color,Tint,Mouse")
				.color("#000000")
				.attr({x:x*TILE_SIZE,y:y*TILE_SIZE,w:TILE_SIZE,h:TILE_SIZE,z: 15})
				.tint("#000000", 0.0)
				.bind('Click', function() {					
					oasis.target.click(this);					
				});
				show = true;
			};
		};
	}
	
	if(show == true) {
	
		oasis.gamecontrols.disableControls();
		
		//ESC to cancel
		this.oldOnKeyDown = window.onkeydown; 
		window.onkeydown = function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 27) {
				oasis.target.clearOverlay();
				oasis.spells.cancelSpell();
			}
		};
		
		$(".container").bind('keypress',function(e) {
			//$.unbind('keydown');
			//alert('keydown');
		});
		
		this.visible = true;
	} else {
		alert("No possible target");
		Target.prototype.clearOverlay.call(this);
	}
};

Target.prototype.onkeydown = function() {
	window.onkeydown = undefined;
};

/**
 * check if the position is in the filter array
 *
 * @param x position x
 * @param y position y
 * @param filteredPosition array of positions to not shade
 * @returns false if the position should be shaded or true if it's a valid target 
 */
function isFilteredPosition(x,y,filteredPosition) {
	for(index in filteredPosition) {
		if(filteredPosition[index].x == x && filteredPosition[index].y == y) {
			return true;
		};
	}
	return false;
}

/**
 * Destroy the overlay entities 
 * 
 */
Target.prototype.clearOverlay = function() {
	//enable the controls again 
	oasis.gamecontrols.updateControls();
	
	for(var entitie in this.overlay) {
		this.overlay[entitie].destroy();
	}
	this.visible = false;
	window.onkeydown = oasis.target.oldOnKeyDown;
};


/**
 * Relay the click entity to the callback and remove the overlay
 *
 * @param entity Craftyjs entity clicked 
 */
Target.prototype.click = function(entity) {
	Target.prototype.clearOverlay.call(this);
	
	if(this.replyto != "") {
		params = {};
		//params.action = "settarget";
		params.target = {x:entity.attr("x") / TILE_SIZE,y:entity.attr("y") / TILE_SIZE};		
		params.roomuuid = roomuuid;
		params.roompw = pw;
		
		game.sendMessage(this.replyto,params);
		return;
	}
	
	if(this.targetclickCallback != null)
		this.targetclickCallback(entity);
};