function Player(player,index,playerinfodiv) {
	Unit.call(this,player);
	this.index = index;
	this.playerInfoDiv = playerinfodiv;
	this.ready = player.ready;
	this.currentPM = player.currentPM;
	this.currentPO = player.currentPO;
	this.bonusStrength = player.bonusStrength;
	this.bonusProtection = player.bonusProtection;
	this.bonusMovement = player.bonusMovement;
	this.movement = player.movement;
	this.sacredStone = player.sacredStone;
	this.position = player.position;
	this.personalSpells = player.spellBook.personalSpells;
	this.spellScrolls = player.spellScrolls;
	this.buffs = player.spellBook.activeSpells;
	this.equipements = player.equipements;
	this.reliques = player.reliques;	
	//this.lastuseruuid = -1;
	
	this.getTotalStrength = function() {
		return this.strength + this.bonusStrength;
	};
	
	this.getTotalProtection = function() {
		return this.protection + this.bonusProtection;
	};
	
	this.update = function(player) {
		Unit.prototype.update.call(this,player);
		this.ready = player.ready;
		this.bonusMaxPV = player.bonusMaxPV;
		this.currentPM = player.currentPM;
		this.currentPO = player.currentPO;
		this.bonusStrength = player.bonusStrength;
		this.bonusProtection = player.bonusProtection;
		this.bonusMovement = player.bonusMovement;
		this.movement = player.movement;
		this.sacredStone = player.sacredStone;
		this.personalSpells = player.spellBook.personalSpells;
		this.spellScrolls = player.spellScrolls;
		this.buffs = player.spellBook.activeSpells;
		this.equipements = player.equipements;
		this.reliques = player.reliques;
		//this.lastuseruuid = -1;
		//this.entity = null;	
	};
}

Player.prototype.dispose = function() {
	Unit.dispose.call(this);
};

Player.prototype.generateEntity = function() {
	if(this.entity != null) {
		this.dispose();
	}
	this.entity = Crafty.e("2D,Canvas,Tooltip,PlayerControls,player_"+ this.index +",Collision")
	.attr({x:this.position.x*TILE_SIZE, y:this.position.y*TILE_SIZE,z:10,movable:false})
	.tooltip("Player : " + this.name);
};

Player.prototype.selectUI = function() {	
	this.playerInfoDiv.addClass("selected");
};

Player.prototype.unSelectUI = function() {
	this.playerInfoDiv.removeClass("selected");
};

Player.prototype.updateUI = function() {	
	
	this.playerInfoDiv.find(".nickname").text(this.name);
	
	switch(this.index) {
	case 0:
		this.playerInfoDiv.find(".nickname-wrapper").addClass("redbox");
		break;
	case 1:
		this.playerInfoDiv.find(".nickname-wrapper").addClass("greenbox");			
		break;		
	case 2:
		this.playerInfoDiv.find(".nickname-wrapper").addClass("yellowbox");				
		break;
	case 3:
		this.playerInfoDiv.find(".nickname-wrapper").addClass("bluebox");		
		break;
	}
	
	if(this.index == oasis.myindex) {
		this.playerInfoDiv.find(".nickname-wrapper").find("img").show();
	}
	
	if(this.id != -1) {	
		this.playerInfoDiv.find(".avatar-wrapper").find("img").attr("src","images/avatars/avatar-" + this.id +".png");
		this.playerInfoDiv.find(".avatar-class").text(heroesdata[this.id].name);
		
		//Strength
		this.playerInfoDiv.find(".strength").text(this.strength + this.bonusStrength);
		if(this.bonusStrength > 0) {
			this.playerInfoDiv.find(".strength").addClass("attribute-bonus");
		} else {
			this.playerInfoDiv.find(".strength").removeClass("attribute-bonus");
		}
		this.playerInfoDiv.find(".bonusstrength").text(this.bonusStrength);
		
		//Protection
		this.playerInfoDiv.find(".protection").text(this.protection + this.bonusProtection);			
		if(this.bonusProtection > 0) {
			this.playerInfoDiv.find(".protection").addClass("attribute-bonus");
		} else {
			this.playerInfoDiv.find(".protection").removeClass("attribute-bonus");
		}		
		this.playerInfoDiv.find(".bonusprotection").text(this.bonusProtection);
		
		//Movement
		this.playerInfoDiv.find(".bonusdeplacement").text(this.bonusMovement);
		
		//$($("#movement-count").get(index)).text(oasis.players[index].movement);
		
			this.playerInfoDiv.find(".currentPV").text(this.currentPV);
			this.playerInfoDiv.find(".maxPV").text(this.maxPV);
			this.playerInfoDiv.find(".currentPM").text(this.currentPM);
			this.playerInfoDiv.find(".currentPO").text(this.currentPO);
		
		if(this.personalSpells.length == 3) {
			this.playerInfoDiv.find(".spell0").text(this.personalSpells[0].name);
			this.playerInfoDiv.find(".spell1").text(this.personalSpells[1].name);
			this.playerInfoDiv.find(".spell2").text(this.personalSpells[2].name);
		}
				
		var players = oasis.units.getPlayers();
		
		//Show correct ammount of stones in the game
		for(var i=0;i<players.length;i++) {
			$(this.playerInfoDiv.find(".sacredstone").get(i)).show();
		}
		
		for(var i=0;i<players.length;i++) {
			$(this.playerInfoDiv.find(".sacredstone").get(i)).addClass("sacredstone-nd");
		}
		//Show correct ammount of stones the player owns
		for(var i=0;i<this.sacredStone;i++) {
			$(this.playerInfoDiv.find(".sacredstone").get(i)).removeClass("sacredstone-nd");
		}
		
		this.playerInfoDiv.visible();
	}
	
	//buff update
	var bufflist = this.playerInfoDiv.find(".buff-wrapper").find("ul");
	bufflist.find("li").remove();
			
	count = this.buffs.length;
	if(count > 5)
		count = 4;
	
	for(var b=0;b< count;b++) {
		if(this.buffs[b].duration == -2) {
			durationTooltip = "Permanant";
			duration = "";
			image = "parchemin.png";
		} else if(this.buffs[b].duration == -1) {
			durationTooltip = "Till Death";
			duration = "";
			image = "parchemin.png";
		} else {
			durationTooltip = this.buffs[b].duration;
			duration = this.buffs[b].duration;
			image = "parchemin-vide.png";
		}
		
						
		bufflist.append("<li><div title='"+ this.buffs[b].spell.name + "&#10;Turn left : "+durationTooltip+"' class='buff'><img src='images/game/"+image+"'/><span class='turn'>"+duration+"</span></div></li>");		
	}	
	
	if(this.buffs.length > 5)
		bufflist.append("<li><div class='buffspopup'>...</div></li>");
	
	//equipment update
	var inventorylist = this.playerInfoDiv.find(".inventory-wrapper").find("ul");
	inventorylist.find("li").remove();
	
	var items = this.equipements.concat(this.reliques);
	
	count = items.length;
	if(count > 5)
		count = 4;
	
	for(var e=0;e< count;e++) {
		inventorylist.append("<li><div title='" + items[e].name + "&#10;Effects : ' class='inventory'><img src='images/equipments/" + items[e].image + "'/></div></li>");		
	}	
	
	if(count > 5)
		inventorylist.append("<li><div class='itemspopup'>...</div></li>");
};