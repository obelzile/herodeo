function Combat(attacker,defender) {
	this.attacker = attacker;
	this.defender = defender;
}

Combat.prototype.refreshCombatInfo = function() {
	//attacker
	var attacker = oasis.players[attackerindex];
	if(attacker instanceof Player)
		$("#avatar-player-attacker-img").attr("src","images/avatars/avatar-"+attacker.id+".png");
	if(attacker instanceof Monster)
		$("#avatar-player-attacker-img").attr("src","images/monsters/monster-"+attacker.id+".png");
	
	$("#attacker-nickname").text(attacker.nickname);
	$("#attacker-pv").text(attacker.currentPV);
	$("#attacker-maxpv").text(attacker.maxPV);
	$("#attacker-strenght").text(attacker.strenght);
	$("#attacker-protection").text(attacker.protection);
		
	//defender	
	var defender = oasis.players[defenderindex];
	if(attacker instanceof Player)
		$("#avatar-player-defender-img").attr("src","images/avatars/avatar-"+defender.id+".png");
	if(attacker instanceof Monster)	
		$("#avatar-player-defender-img").attr("src","images/monsters/monster-"+defender.id+".png");
	
	$("#defender-nickname").text(defender.nickname);
	$("#defender-pv").text(defender.currentPV);
	$("#defender-maxpv").text(defender.maxPV);
	$("#defender-strenght").text(defender.strenght);
	$("#defender-protection").text(defender.protection);
};

Combat.prototype.endCombat = function() {
	$("#popup").hide();	
};

attacker = null;
defender = null;

//onload
$(function(){
	$("#block-attack").click(function() {setDefenseAction(0);});
	$("#dodge-attack").click(function() {setDefenseAction(1);});
	$("#counter-attack").click(counterAttack);
	$("#end-combat").click(endCombat);
});

function onStartCombat(data) {
	
	attacker = oasis.units.getUnit(data.attackeruuid);
	defender = oasis.units.getUnit(data.defenderuuid);
	
	refreshCombatInfo();

	$("#defender-history").find("li").remove();
	$("#attacker-history").find("li").remove();
		
	/*if(oasis.myindex == data.attackerindex) {
		
	}*/
		
	onStateUpdate(data.state);
	
	$("#combat").show();	
}

function onEndCombat(data) {
	$("#combat").hide();	
}

function refreshCombatInfo() {
	//attacker
	//var attacker = oasis.players[attackerindex];
	if(attacker instanceof Player)
		$("#avatar-player-attacker-img").attr("src","images/avatars/avatar-"+attacker.id+".png");
	if(attacker instanceof Monster)
		$("#avatar-player-attacker-img").attr("src","images/monsters/monster-"+attacker.id+".png");
	
	
	$("#attacker-name").text(attacker.name);
	$("#attacker-pv").text(attacker.currentPV);
	$("#attacker-maxpv").text(attacker.maxPV);
	$("#attacker-strength").text(attacker.getTotalStrength());
	$("#attacker-protection").text(attacker.getTotalProtection());
		
	//defender	
	if(defender instanceof Player)
		$("#avatar-player-defender-img").attr("src","images/avatars/avatar-"+defender.id+".png");
	if(defender instanceof Monster)	
		$("#avatar-player-defender-img").attr("src","images/monsters/monster-"+defender.id+".png");
		
	$("#defender-name").text(defender.name);
	$("#defender-pv").text(defender.currentPV);
	$("#defender-maxpv").text(defender.maxPV);
	$("#defender-strength").text(defender.getTotalStrength());
	$("#defender-protection").text(defender.getTotalProtection());
}

function onAttackPerformed(data) {	
//	params.put("playerturn", this.playerturn);
//	params.put("defender", defender);
//	params.put("attacker", attacker);		
//	params.put("lasthistory", history.get(history.size()-1));
	
	attacker.update(data.attacker);
	defender.update(data.defender);
	
	$("#attacker-history").append("<li>"+data.lasthistory.attackerHistory+"</li>");
	$("#defender-history").append("<li>"+data.lasthistory.defenderHistory+"</li>");	
	
	refreshCombatInfo();
	
	onStateUpdate(data.state); 
}

function onStateUpdate(state) {
	switch(state) {
	case 0:
		onEndCombatState();
		break;
	case 1:
		onOffensiveState();
		break;
	case 2:
		onDefensiveState();
		break;
	case 3:
		onWaitState();
		break;
	case 4:
		onStartCombatState();
		break;
	}
	
}

function onCounterAttack(data) {
	onStateUpdate(data.state);	
}

function onStartCombatState() {	
	$("#start-combat").show();
	$("#block-attack").hide();
	$("#dodge-attack").hide();
	$("#counter-attack").hide();
	$("#end-combat").hide();
	$("#wait-notice").hide();	
}

function onEndCombatState() {
	$("#start-combat").hide();
	$("#block-attack").hide();
	$("#dodge-attack").hide();
	$("#counter-attack").hide();
	$("#end-combat").show();

	$("#wait-notice").hide();
}

function onDefensiveState() {
	$("#start-combat").hide();
	$("#block-attack").show();
	$("#dodge-attack").show();
	$("#counter-attack").hide();
	$("#end-combat").hide();

	$("#wait-notice").hide();
}

function onOffensiveState() {
	$("#start-combat").hide();
	$("#block-attack").hide();
	$("#dodge-attack").hide();
	$("#counter-attack").show();
	$("#end-combat").show();
	
	$("#wait-notice").hide();
}

function onWaitState() {
	$("#start-combat").hide();
	$("#block-attack").hide();
	$("#dodge-attack").hide();
	$("#counter-attack").hide();
	$("#end-combat").hide();
	
	$("#wait-notice").show();	
}

function counterAttack() {
	params = {};	
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("CounterAttack",params);
}

function endCombat() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("EndCombat",params);
}

function setDefenseAction(action) {
	
	onStateUpdate(3);
	
	params = {};
	params.defensetype = action;	
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("SetDefenseAction",params);
}