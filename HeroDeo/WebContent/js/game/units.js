function Units() {
	this.units = [];
}

Units.prototype.get = function(index) {
	if(index < 0 || index > this.units.length-1)
		return null;
	return this.units[index];
};

Units.prototype.getPlayer = function(index) {
	var count = -1;
	for(i in this.units) {
		if(this.units[i] instanceof Player) {
			count++;
			if(count === index)
				return this.units[i];
		}
	}
};

Units.prototype.getPlayers = function() {
	var players = [];

	for(i in this.units) {
		if(this.units[i] instanceof Player)
			players.push(this.units[i]);
	}

	return players;
};

Units.prototype.updateUnit = function(unit) {
	return Units.prototype.addUnit.call(this,unit);
};

Units.prototype.count = function() {
	return this.units.length;
};

Units.prototype.getUnit = function(uuid) {
	for(index in this.units) {
		if(this.units[index].uuid === uuid)
			return this.units[index];
	}
	return null;
};

Units.prototype.getUnitIndex = function(uuid) {
	for(index in this.units) {
		if(this.units[index].uuid === uuid)
			return index;
	}
	return -1;
};

Units.prototype.addUnit = function(unit) {
	
	var u = Units.prototype.getUnit.call(this,unit.uuid);
	if(u == null) {
		if(typeof unit === "Object") {
		    if(unit.type === "Monster") {
		    	u = new Monster(unit);		        
		    } else if(unit.type === "Player") {
		    	u = new Player(unit);	
		    }
		} else {
			u=unit;
		}
	    if(u != null) {
			this.units.push(u);		
			u.generateEntity();				    
	    }
	} else {
		u.update(unit);
	}
	return u;
};

Units.prototype.removeUnit = function(unit) {
	var u = null;
	if(unit != null) {
		var index = Units.prototype.getUnitIndex.call(this,unit.uuid);
		if(index != -1) {
			u = this.units[index];
			u.dispose();
			this.units.splice(index,1);
		}
	}
	return u;
};

Units.prototype.updateUI = function() {
	
};