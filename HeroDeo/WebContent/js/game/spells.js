var Spells = function() {
	this.spells = spellsdata;
	this.currentSpell = null;
	
	this.target = [];
	this.choice = [];
	
	$("#spells").draggable();	
};

Spells.prototype.getSpellFromId = function(spellId) {
	for(index in this.spells) {
		if(this.spells[index].id == spellId) {
			return this.spells[index];
		}
	}
	
	return null;
};

//init a new spell cast
Spells.prototype.prepareSpellCast = function(spellId) {
	this.currentSpell = Spells.prototype.getSpellFromId.call(this,spellId);
	Spells.prototype.spellStep.call(this);
};

Spells.prototype.spellStep = function() {
	if(this.currentSpell == null)
		return;
	
	//resolve target
	if(this.currentSpell.targetScript != "" && typeof this.currentSpell.targetScript !== "undefined") {
		if(this.target.length == 0) {
			params = {"celltype":[],"position":[]};
			var targetScriptFn = eval("["+this.currentSpell.targetScript+"][0]");
			targetScriptFn(params);
			
			oasis.target.queryTarget(params,oasis.spells.setTargets);			
			return;
		}
	}
	
	//resolve choice
	if(this.currentSpell.choiceScript != "" && typeof this.currentSpell.choiceScript !== "undefined") {
		if(this.choice.length == 0) {
			var choices = eval("["+this.currentSpell.choiceScript+"][0]");
		
			Choices.clearChoices();
						
			for(index in choices) {
				Choices.addChoice(new ChoiceItem(choices[index],oasis.spells.setChoice));
			}
			
			Choices.show();
			
			return;
		}
	}
	
	oasis.spells.castSpell();
	
};

Spells.prototype.setTargets = function(target) {
	oasis.spells.target[0] = {x:target.attr("x") / TILE_SIZE,y:target.attr("y") / TILE_SIZE};
	oasis.spells.spellStep();
};

Spells.prototype.setChoice = function(choice) {
	oasis.spells.choice[0] = choice.item.value;
	oasis.spells.spellStep();
};

Spells.prototype.castSpell = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	params.spellid = this.currentSpell.id;
	params.targetx = (this.target.length > 0 ? this.target[0].x : -1);
	params.targety = (this.target.length > 0 ? this.target[0].y : -1);
	params.choice = (this.choice.length > 0 ? this.choice[0] : -1);
	
	game.sendMessage("CastSpell",params);
	
	this.currentSpell = null;
	
	this.target = [];
	this.choice = [];
};

Spells.prototype.cancelSpell = function() {
	this.currentSpell = null;	
	this.target = [];
	this.choice = [];
	$("#spells").hide();
};

Spells.prototype.showSpells = function(data) {
	
	//remove previous spells
	$("#spell-container").find(".spell").not('#spell-template').remove();
	
	//create each spell div
	$(data.spells).each(function(index,data){
		spell = $("#spell-template").clone();
		spell.show();
		spell.attr("id",data.id);
		spell.find(".spell-hero").find("img").attr("src","images/avatars/avatar-" + this.heroId + ".png");
		spell.find(".spell-name").text(this.name);
		spell.find(".spell-cost").text("Sortil�ge " + this.level + " : " + this.cost +" PM");
		spell.find(".spell-duration").text("");
		spell.find(".spell-description").text(this.description);
		//spell.addClass(heroesdata[this.heroId].cssclass);

		spell.click(function() {
			$("#spells").hide();
			//oasis.spells.castSpell($(this).attr("id"));
			oasis.spells.prepareSpellCast($(this).attr("id"));
		});
		
		$("#spell-container").append(spell);
	});
	
	//set the spell container size to display spell on 1 line 
	var width = 0;
	$('#spell-container .spell')
	.not('#spell-template')
	.each(function() {
	    width += $(this).outerWidth( true );
	});
	$('#spell-container').css('width', width + "px");
		
	//show the popup
	$("#spells").show();
	
	//update the scrollbar
	$(".spell-scroll-wrapper").mCustomScrollbar("destroy");	
	$(".spell-scroll-wrapper").mCustomScrollbar({
		theme:"dark",
		horizontalScroll:true
	});
};