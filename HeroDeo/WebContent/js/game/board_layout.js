
//Cell type Enum
var CellType = Object.freeze({"Wall":0,"Floor":1, "Pit":2, "Well":3,"Trap":4,"Chest":5,"RedChest":6,"Start0":7,"Start1":8,"Start2":9,"Start3":10,"BlueEnergy":11,"RedEnergy":12 });

/**
* Layer in a cell containing the type and craftyjs entity
*
* @class Layer
* @constructor
*/
function Layer(celltype,entity) {	
	this.celltype = celltype;
	//craftyjs entity
	this.entity = entity;
}

/**
* Cell that contain layers
*
* @class Layer
* @constructor
*/
function Cell(x,y) {	
	this.position = {"x":x,"y":y};
	//Layer obj array
	this.layers = [];	
};

/**
* Search the cell layers for the presence of the celltype 
*
* @method hasCellType
* @param {CellType} celltype type of the layer to look for
* @return {Boolean} Returns true on success
*/
Cell.prototype.hasCellType = function(celltype) {
	for(index in this.layers) {
		if(this.layers[index].celltype == celltype)
			return true;
		
	}
	return false;
};

/**
*  Search the cell layers for any of the cell type presence 
*
* @method hasAnyCellTypeOf
* @param {CellType} celltype type of the layer to look for
* @return {Boolean} Returns true on success
*/
Cell.prototype.hasAnyCellTypeOf = function(celltypes) {
	
	var result = false;
	
	for(var index in celltypes) {
		result  = Cell.prototype.hasCellType.call(this,celltypes[index]);
		if(result == true)
			break;
	}
	
	return result;
};

/**
* add a new layer to the cell
*
* @method addLayer
* @param {CellType} celltype type of the layer to add
* @return {Boolean} Returns true on success
*/
Cell.prototype.addLayer = function(type) {
	this.layers[this.layers.length] = new Layer(type,null);
};

/**
* remove the layer of a type in the cell
*
* @method removeLayer
* @param {CellType} celltype type of the cell to look for
*/
Cell.prototype.removeLayer = function(type) {
	for(index in this.layers) {
		if(this.layers[index].celltype == type) {
			this.layers[index].entity.destroy();
			this.layers.splice(index,1);
		}
	}
};

/**
* Generate the CraftyJs entity base on the layer type
*
* @method generateEntities
* 
* */
Cell.prototype.generateEntities = function() {
	
	for(index in this.layers) {	
		var entity = null;
		switch(this.layers[index].celltype) {
		case CellType.Wall:				
			entity = Crafty.e("2D,Canvas,Tint,wall").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Wall})
			.tint("#000000", 0.0);
			break;
		case CellType.Floor:	
			entity = Crafty.e("2D,Canvas,Tint,floor").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Floor})
			.tint("#000000", 0.0);		
			break;
		case CellType.Pit:	
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,pit").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Pit})
			.tint("#000000", 0.0)
			.tooltip("Pit");			
			break;	
		case CellType.Well:				
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,well").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Well})
			.tint("#000000", 0.0)
			.tooltip("Puit");
			break;
		case CellType.Trap:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,trap").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Trap})
			.tint("#000000", 0.0)
			.tooltip("Trap");			
			break;
		case CellType.Chest:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,chest").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Chest})
			.tint("#000000", 0.0)
			.tooltip("Coffre");
			break;
		case CellType.RedChest:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,redchest").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.RedChest})
			.tint("#000000", 0.0);			
			break;
		case CellType.Start0:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,playerstart0").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Start0})
			.tint("#000000", 0.0)
			.tooltip("Marchand");
			break;
		case CellType.Start1:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,playerstart1").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Start1})
			.tint("#000000", 0.0)
			.tooltip("Marchand");
			break;
		case CellType.Start2:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,playerstart2").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Start2})
			.tint("#000000", 0.0)
			.tooltip("Marchand");
			break;
		case CellType.Start3:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,playerstart3").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.Start3})
			.tint("#000000", 0.0)
			.tooltip("Marchand");
			break;	
		case CellType.BlueEnergy:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,blueenergy").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.BlueEnergy})
			.tint("#000000", 0.0)
			.tooltip("Sortil�ge Novice Gratuit");
			break;	
		case CellType.RedEnergy:
			entity = Crafty.e("2D,Canvas,Tooltip,Tint,redenergy").attr({x:this.position.x*TILE_SIZE,y:this.position.y*TILE_SIZE,celltype:CellType.RedEnergy})
			.tint("#000000", 0.0)
			.tooltip("Sortil�ge Gratuit");
			break;		
		}		

		this.layers[index].entity = entity;
	}

};

/**
* Board containing the layout cells
*
* @class Board
* @constructor
*/
function Board() {
	this.board = [];
	this.name = "";
	this.description = "";
	this.width = 0;
	this.height = 0;
	this.initialized = false;
};

/**
* check if there a celltype at the position 
*
* @method hasCellType
* @param {CellType} celltype type of the cell to look for
* @param {Position} position object containing x and y
* @return {Boolean} Returns true on success
*/
Board.prototype.hasCellType = function(celltype,position) {
	if( this.board.length == 0)
		return false;
	
	return this.board[position.y][position.x].hasCellType(celltype);
};

/**
* check if any of the celltypes are at the position  
*
* @method hasAnyCellTypeOf
* @param {CellType} celltype type of the cell to look for
* @param {Position} position object containing x and y 
* @return {Boolean} Returns true on success
*/
Board.prototype.hasAnyCellTypeOf = function(celltypes,position) {
	if( this.board.length == 0)
		return false;
	
	return this.board[position.y][position.x].hasAnyCellTypeOf(celltypes);
};

/**
* add a celltype layer at the position if not already present
*
* @method addCellType
* @param {CellType} celltype type of the cell to look for
* @param {Position} position object containing x and y 
* @return {Boolean} Returns true on success
*/
Board.prototype.addLayer = function(celltype,position) {
	var result = false;
	if(!this.board[position.y][position.x].hasCellType(celltype)) {
		this.board[position.y][position.x].addLayer(celltype);
		result = true;
	}
	return result;
};

/**
* remove a layer at the position if present  
*
* @method removeLayer
* @param {CellType} celltype type of the cell to look for
* @param {Position} position object containing x and y 
* @return {Boolean} Returns true on success
*/
Board.prototype.removeLayer = function(celltype,position) {
	var result = false;	
	if(this.board[position.y][position.x].hasCellType(celltype)) {
		this.board[position.y][position.x].removeLayer(celltype);
		result = true;		
	}
	return result;	
};

/**
*  find cells of type and return them
*
* @method queryCellsOfTypes
* @param {CellType[]} celltype array to look for
* @return {Cell[]} Returns array of cells on success
*/
Board.prototype.queryCellsOfTypes = function(celltypes) {
	var cells = [];
	
	i=0;
	for(var y=0;y<this.board.length;y++) {
		for(var x=0;x<this.board[y].length;x++) {
			if(this.board[y][x].hasAnyCellTypeOf(celltypes)) {
				cells[i] = this.board[y][x];
				i++;
			}				
		}
	}
	
	return cells;
};

/**
* return cell in the radius except floor cells 
*
* @method queryCellsInRadius
* @param {Position} position object containing x and y 
* @param {Integer} distance to search  
* @return {Cell[]} Returns array of cell on success
*/
Board.prototype.queryCellsInRadius = function(position,radius) {
	if(position.x > this.width-1 || position.y > this.height-1) {
		throw new Error("queryCellsInRadius: Position outofbound");
	}
	
	cells = [];
	i=0;
	//check 4 side of the position for floor tile
	for(var r=1;r<=radius;r++) {
		for(var p=0;p<4;p++) {
			try {
				switch(p) {
					case 0:
						if(this.board[position.y][position.x-r].hasCellType(CellType.Floor)) {
							cells[i] = this.board[position.y][position.x-r];
							i++;
						}
						break;
					case 1:
						if(this.board[position.y-r][position.x].hasCellType(CellType.Floor)) {
							cells[i] = this.board[position.y-r][position.x];
							i++;
						}
						break;
					case 2:
						if(this.board[position.y][position.x+r].hasCellType(CellType.Floor)) {
							cells[i] = this.board[position.y][position.x+r];
							i++;
						}
						break;
					case 3:
						if(this.board[position.y+r][position.x].hasCellType(CellType.Floor)) {
							cells[i] = this.board[position.y+r][position.x];
							i++;
						}
						break;				
				}
			} catch(err) {
				
			}
		}
	}
	
	return cells;
};

/**
* Search the cell layer for the presence of the celltype 
*
* @method loadLayout
* @param {String} file layout file to load
* @param {function} callback to call once the layout is loaded
*/
Board.prototype.loadLayout = function(file,callback) {
	that = this;
	$.ajaxSetup({ cache: false });
	$.get(file,function(data) {
				
		//read the layout xml
		var layout = $(data).find('layout');
		that.name = layout.find('name').text();
		that.description = layout.find('description').text();
		that.width = layout.find('size').attr('width');
		that.height = layout.find('size').attr('height');
				
		var rows = layout.find('layer0').find('row');
		
		//if(rows.size() != this.width * this.height)
			//throw new Error("the number of rows in the layout doesn't match it's size");
		//this._gameBoard = create2DArray(this.width,this.height);
		
		// read each rows and add the layers
		rows.each(function(y,cells) {
			var row = [];
			$(cells).find("cell").each(function(x,cell) {			
				var newCell = new Cell(x,y);
				newCell.addLayer(parseInt($(cell).find("type").text()));
				row.push(newCell);				
			});
			that.board.push(row);
		});
				
		//layer1	
		var cells = layout.find('layer1').find('cell');
		
		//if(rows.size() != this.width * this.height)
			//throw new Error("the number of rows in the layout doesn't match it's size");
		
		//this._gameBoard = create2DArray(this.width,this.height);		
		cells.each(function(index,cell) {			
				var x = $(cell).find("x").text();
				var y = $(cell).find("y").text();

				that.board[y][x].addLayer(parseInt($(cell).attr('type')));
		});
		
		callback(that.board);
	});
};

/**
* if the game has been initialized generate gamejs entities for each layer of the board
*
*/
Board.prototype.generateEntities = function() {
	if(this.initialized) {
		for(var y=0;y<this.board.length;y++) {
			for(var x=0;x<this.board[y].length;x++) {
				this.board[y][x].generateEntities();
			}
		}
	} else {
		// if the game not done initializing retry in 100ms
		setTimeout(function(){oasis.board.generateEntities();},100);
	}
	
};