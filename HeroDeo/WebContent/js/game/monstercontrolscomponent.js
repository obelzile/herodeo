Crafty.c("MonsterControls", {
	_keys: {
		UP_ARROW: [0,-1],
		DOWN_ARROW: [0,1],
		RIGHT_ARROW: [1,0],
		LEFT_ARROW: [-1,0],
		W: [0,-1],
		S: [0,1],
		D: [1,0],
		A: [-1,0],
	},
	
	init: function() {
		this._moveX = 0;
		this._moveY = 0;
		this._prevX = 0;
		this._prevY = 0;
		
		for(var k in this._keys) {
			var keyCode = Crafty.keys[k] || k;
			this._keys[keyCode] = this._keys[k];
		}
		
		this.bind("KeyDown",function(e) {
			if(this._keys[e.key] && this.movable == true) {
				this._moveX = this._keys[e.key][0];
				this._moveY = this._keys[e.key][1];
			}
		}).bind("EnterFrame",function() {
			if(this._moveX || this._moveY) {
				Logger.log(Logger.notice,"movedRequest");
				
				params = {};
				params.roomuuid = roomuuid;
				
				params.position = {"x" : this.x / TILE_SIZE + this._moveX,"y" : this.y / TILE_SIZE + this._moveY};
				
				//params.x = oasis.players[oasis.myindex].position.x + this._moveX;
				//params.y = oasis.players[oasis.myindex].position.y + this._moveY;
				
				//params.x = this.x + this._moveX;
				//params.y = this.y + this._moveY;
								
				//game.sendMessage("MonsterMovementRequest",params);	
								
				//this._prevX = this.x;
				//this._prevY = this.y;
				//this.x += this._moveX * 32;
				//this.y += this._moveY * 32;
				this.trigger('Moved',{"x" : this.x / TILE_SIZE + this._moveX,"y" : this.y / TILE_SIZE + this._moveY});
				this._moveX = 0;
				this._moveY = 0;
			}
		});
	}
});