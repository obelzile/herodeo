//Choice object   text content and click callback
var ChoiceItem = function(item,callback) {
	this.item = item;
	this.callback = callback;	
};

var Choices = (function() {
				
	choices = []; //ChoiceItem objects
	replyto = "";
	title = "";
		
	//ChoiceItem click handle that call the callback
	_onclick = function(e) {
		
		Choices.hide();
		//retrieve the index from the id
		index = this.id.substring(6);
		//if set call the callback
		
		if(replyto != "") {
			params = {};
			//params.action = "setchoice";
			params.choice = choices[index].item.value;		
			params.roomuuid = roomuuid;
			params.roompw = pw;
			
			game.sendMessage(replyto,params);
		} else if(choices[index].callback !== null) {			
			choices[index].callback(index,choices[index]);
		}
	};
	
	return {
		
		replyto : "",
		
		//Init the Choice popup
		init : function(data) {
			if(data.replyto !== undefined) {
				replyto = data.replyto;
			}
			if(data.title !== undefined) {
				title = data.title;
			}
			
			$("#choices-close").click(function() {
				Choices.hide();
			});			
		},
		
		//show the Choice popup
		show : function(choices) {
			if(title != "") {
				$('#choices #choice-title').text(title);
			}			
			
			$('#choices').show();
			
			var $div = $('.choices-container');
			var height = 48 + $div.height();
			
			$('#choices').css('height', height + "px");
		},
		
		//hide the Choice popup
		hide : function() {
			$('#choices').hide();			
		},
		
		//clear the ChoiceItem objects array
		clearChoices: function() {
			choices = [];
			$('#choices').find("li").remove();
		},
		
		//add a ChoiceItem object to the array
		addChoice : function(choiceitem) {
			index = choices.length;
			choices[index] = choiceitem;
			//add choice as a li 
			var container = $('#choices').find("ul");
			var element = $('<li id="choice'+index+'"><div class="choice"><span>'+choiceitem.item.text+'</span></div></li>');
			element.click(_onclick);
			container.append(element);
		},
		
		//remove a ChoiceItem at the index
		removeChoice : function(index) {
			choices.splice(index,1);
			$($('#choices').find("li").get(index)).remove();
		}		
	};
	
})();