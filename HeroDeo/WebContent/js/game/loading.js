
//pions
Crafty.sprite(TILE_SIZE,"images/game/pions_40.png", {
	player_0:[0,0],
	player_2:[0,1],
	player_3:[0,2],
	player_1:[0,3],
	monster:[0,4],
});

//board celltypes
Crafty.sprite(TILE_SIZE,"images/game/sprites_40.png", {
	wall:[0,0],
	floor:[0,1],
	pit:[0,2],
	well:[0,3],
	trap:[0,4],
	chest:[0,5],
	redchest:[0,6],
	playerstart0:[0,7],
	playerstart1:[0,8],
	playerstart2:[0,9],
	playerstart3:[0,10],
	blueenergy:[0,11],
	redenergy:[0,12],
});

//heros icon
Crafty.sprite(80,"images/game/avatars_80.png", {
	hero_0: [0,0],
	hero_1: [0,1],
	hero_2: [0,2],
	hero_3: [0,3],
	hero_4: [0,4],
	hero_5: [0,5],
	hero_6: [0,6],
	hero_7: [0,7],
	hero_8: [0,8],
});

Crafty.sprite(TILE_SIZE,"images/game/monsters_40.png", {
	monster_0: [0,0],
	monster_1: [0,1],
	monster_2: [0,2],
	monster_3: [0,3],
	monster_4: [0,4],
	monster_5: [0,5],
	monster_6: [0,6],
	monster_7: [0,7],
});
	
Crafty.sprite(80,"images/game/monsters_80.png", {
	monster80_0: [0,0],
	monster80_1: [0,1],
	monster80_2: [0,2],
	monster80_3: [0,3],
	monster80_4: [0,4],
	monster80_5: [0,5],
	monster80_6: [0,6],
	monster80_7: [0,7],
});

Crafty.scene("loading",function() {
	
	console.log("loading");
	//load assets
	Crafty.load(["images/game/sprites.png","images/game/characters.png"],function() {
		Logger.log(Logger.notice,"loaded");
		Crafty.scene("main");
	},
    function(e) {
      //progress
    },

    function(e) {
    	Logger.log(Logger.notice,"error loading");
    });
		
	Crafty.background("#000");
	Crafty.e("2D,DOM,Text").attr({w:100,h:20,x:150,y:120})
		.text("Loading")
		.css({"text-align":"center"});
});