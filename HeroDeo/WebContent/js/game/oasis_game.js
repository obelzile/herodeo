var Oasis = function() {	
	this.initialised = false;
	this.myuuid = "";
	this.myindex = -1;
	this.layout = null;
	this.turn = 0;
	this.playerindex = 0;
	this.gamestate = 1;
	this.board = null;
	this.target = null;
	this.spells = null;
	this.gamecontrols = null;
	
	this.units = null;
	
	this.drank = false;
	this.attacked = false;
	
	this.moving = false;
	this.moved = false;
	
	this.monstercontrol = null;
	
	this.treasures = [];
	
	Object.defineProperties(this, {		
		"me" : {
	        get : function()
	        {
	        	return this.units.getUnit(this.myuuid);	        	
	        }
	    },
	    "merchant" : {
	        get : function()
	        {
	           return Merchant;
	        }
	    }    	    
	});
};

Oasis.prototype.init = function(data) {
	Logger.log(Logger.enum.notice,"init_game " + this.layout);
	this.myuuid = data.myuuid;
	this.layout = data.layout;
	this.myindex = data.myindex;
	this.gamestate = data.gamestate;
	this.turn = data.turn;
	this.playerindex = data.playerindex;	
	this.target = new Target();
	this.spells = new Spells();
	this.moving = data.moving;
	this.moved = data.moved;
	this.movementSteps = [];
		
	Crafty.init(16*TILE_SIZE,16*TILE_SIZE);
	Crafty.canvas.init();
	
	Crafty.background("black");
	
	this.units = new Units();
	
	Merchant.init("#merchant");
	Merchant.onBuyItem = Oasis.prototype.onBuyItem;
	
	this.treasures = data.treasures;
		
	this.drank = data.drank;
	
	this.board = new Board();
	this.board.loadLayout(this.layout,function() {
		for(index in oasis.treasures) {
			oasis.board.addLayer(CellType.Chest,oasis.treasures[index]);
		}
		
		oasis.board.initialized = true;
	});
	
	//monsters init
	for(index in data.monsters) {
		var monster = new Monster(data.monsters[index]);
		this.units.addUnit(monster);
	}
		
	//players init
		
	if(data.players.length == 2) {
		playerInfoDivIndex = [0,3,1,2];
	} else if(data.players.length == 3) {
		playerInfoDivIndex = [0,1,3,2];
	} else {
		playerInfoDivIndex = [0,2,1,3];
	}
		
	for(var index=0;index<data.players.length;index++) {
		var playerInfoDiv = $($(".selection-border").get(playerInfoDivIndex[index]));
		var player = new Player(data.players[index],playerInfoDivIndex[index],playerInfoDiv);		
	//	this.players[index] = new Player(data.players[index],playerInfoDivIndex[index],playerInfoDiv);
		this.units.addUnit(player);
	}
	
	setPlayerInfoBorder(this.playerindex);
	
/*	for(var i=0;i<this.players.length;i++) {
		this.players[i].updateUI();				
	}*/
	
	var players = oasis.units.getPlayers();
	for(var i=0;i<players.length;i++) {
		players[i].updateUI();			
	}
	
	this.initialised = true;
};

Oasis.prototype.onBuyItem = function(shoppingList) {
	
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	params.shoppinglist = shoppingList;
	
	game.sendMessage("BuyItem",params);
	
};

Oasis.prototype.getOpponents = function() {
	var opponents = [];

	for(var i=0;this.units.count();i++) {
		var unit = this.units.get(i);
		if(unit.uuid != this.myuuid) {
			opponents.push(unit);
		}
	}
	return opponents;
};

Oasis.prototype.filterOccupiedCells = function(cells) {
	result = [];
	i=0;	
	for(c in cells) {
		found = false;		
		for(var i=0; i<this.units.count();i++) {
			if(cells[c].x == this.units.get(i).position.x && cells[c].y == this.units.get(i).position.y) {
				found = true;
			}
		}			
		
		if(found == false) {
			result[i] = cells[c];
			i++;
		}
	}
	return result;
};

Oasis.prototype.run = function(data) {
	Logger.log(Logger.enum.notice,"run");
		
	this.updateScene();
};

Oasis.prototype.removeTreasure = function(data) {
	
	for(index in this.treasures) {
		if(this.treasures[index].x == data.position.x && this.treasures[index].y == data.position.y) {
			this.treasures.splice(index,1);
		}
	}
	
	oasis.board.removeLayer(CellType.Chest,data.position);
	this.gamecontrols.updateControls();
};

Oasis.prototype.updateTreasures = function(data) {
	for(index in data.treasures) {
		if(this.board.addCellType(CellType.Chest,data.treasures[index])) {
			this.treasures[treasures.length] = data.treasures[index];
		}
	}
};

Oasis.prototype.drinkFromWell = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	this.drank = true;
	
	game.sendMessage("DrinkFromWell",params);
	this.gamecontrols.updateControls();
};

Oasis.prototype.throwCoinInWell = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("ThrowCoinInWell",params);
	this.gamecontrols.updateControls();
};

Oasis.prototype.ramassermarqueur = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("RamasserMarqueur",params);
	this.gamecontrols.updateControls();
};

Oasis.prototype.visitemarchand = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("VisiteMarchand",params);
	this.gamecontrols.updateControls();
};

Oasis.prototype.initiateAttack = function(target) {
	if(this.attacked == false) {
		params = {};
		params.roomuuid = roomuuid;
		params.roompw = pw;
		
		params.target = target;
		this.attacked = false;
		
		game.sendMessage("InitiateAttack",params);
		this.gamecontrols.updateControls();
	}
};

Oasis.prototype.requestMoveState = function(value) {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	if(value == true) {
		if(this.moving == false || this.moved == false) {
			game.sendMessage("RequestMovementState",params);
		}
	} else {
		if(this.moving == true) {
			game.sendMessage("FinishMovementState",params);
		}
	}
};

Oasis.prototype.requestMonsterAction = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("MonsterActionRequest",params);
};

Oasis.prototype.requestMonsterMovementState = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("RequestMonsterMovementState",params);	
};

Oasis.prototype.requestMonsterAttackState = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("RequestMonsterAttackState",params);
};

Oasis.prototype.requestMonsterControlSkip = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("RequestMonsterControlSkip",params);
};

Oasis.prototype.finishMoveState = function() {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	params.state = "finish";
	
	if(this.monstercontrol.monstermoving == true) {
		game.sendMessage(this.monstercontrol.finishCallback,params);
	}
};

Oasis.prototype.onMonsterMoveState = function(data) {
	
	if(this.monstercontrol == null) {
		this.monstercontrol = new MonsterControl(data);
	}
	
	var monster = oasis.units.getUnit(this.monstercontrol.monsteruuid);
	
	//monster doesn't exist client side
	if(monster == null) {
		this.monstercontrol = null;
		return;
	}
		
	if(data.moving == true) {	
		monster.entity.attr({movable: true});
		monster.stepLeft = data.movement;
	} else {		
		monster.entity.attr({movable: false});
		for(step in this.movementSteps) {
			this.movementSteps[step].destroy();			
		}
		this.movementSteps = [];
		this.monstercontrol.monstermoved = true;
		this.monstercontrol.monstermoving = false;
		//this.monstercontrol = null;
	}
	this.gamecontrols.updateControls();
};

Oasis.prototype.onMonsterMoving = function(data) {
	params = {};
	params.roomuuid = roomuuid;
	
	params.position = data;
	
	game.sendMessage(oasis.monstercontrol.movementCallback,params);
};

Oasis.prototype.onMoveState = function(data) {	
	this.moving = data.moving;
	this.moved = data.moved;	
		
	var player = this.units.getUnit(this.myuuid);
	
	if(data.moving == true) {	
		player.entity.attr({movable: true});
		player.stepLeft  = data.stepleft;
	} else {		
		player.entity.attr({movable: false});
		for(step in this.movementSteps) {
			this.movementSteps[step].destroy();			
		}
		this.movementSteps = [];
	}
	this.gamecontrols.updateControls();
};


Oasis.prototype.endTurn = function() {
	
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("PlayerEndTurn",params);
};

Oasis.prototype.newPlayerTurn = function(data) {
	this.playerindex = data.playerindex;
	this.moving = false;
	this.moved = false;
	this.drinkwell = false;
	this.gamecontrols.updateControls();
	setPlayerInfoBorder(this.playerindex);
	this.units.getPlayer(this.playerindex).updateUI();
};

Oasis.prototype.turnBegin = function(playerIndex) {
	setSelectedPlayerInfo(playerIndex);
	this.players[this.playerindex].updateUI();
	//update player info
};

Oasis.prototype.attackTargetsUpdate = function(data) {
	this.units.getUnit(data.playeruuid).attackTargets = data.attacktargets;
	this.gamecontrols.updateControls();
};

Oasis.prototype.bonusPhase = function(data) {
	this.monstercontrol = null;
	this.moved = false;
	this.attacked = false;
	this.drank = false;	
	this.moving = false;
	
	updateInfo();
};

Oasis.prototype.mainPhase = function(data) {
	updateInfo();
};

Oasis.prototype.removeUnit = function(data) {	
	this.units.removeUnit(data.unit);	
};

Oasis.prototype.updateUnit = function(data) {
	this.units.updateUnit(data.unit);
	updateInfo();
};

Oasis.prototype.updateUnits = function(data) {
	for(var i in data.units) {
		this.units.updateUnit(data.units[i]);
	}
	updateInfo();
};

Oasis.prototype.moveUnit = function(data) {
	var unit = this.units.getUnit(data.unit.uuid);
		
	if(unit != null) {	
		var found = false;
		for(step in this.movementSteps) {
			if(this.movementSteps[step].attr("x")/TILE_SIZE == data.unit.position.x && this.movementSteps[step].attr("y")/TILE_SIZE == data.unit.position.y) {
				this.movementSteps[step].destroy();
				this.movementSteps.splice(step,1);
				found = true;
			}
		}
		
		if(found == false) {			
			var color = "#0026FF";
			if(unit instanceof Player) {
				if(this.board.hasCellType(CellType.Trap,unit.position)) {
					color = "#FF0000";
				}
			}
			this.movementSteps[this.movementSteps.length] = Crafty.e("2D,Canvas,Color,Tint")
			.color(color)
			.attr({x:unit.position.x*TILE_SIZE,y:unit.position.y*TILE_SIZE,w:TILE_SIZE,h:TILE_SIZE,z: 15})
			.tint(color, 0.2);
		}
		
		var unit = this.units.updateUnit(data.unit);
		
		unit.updateUI();
		this.gamecontrols.updateControls();
	}
};

Oasis.prototype.setCurrentPlayerIndex  = function(data) {
	
	this.playerindex = data.playerindex;
	
	if(data.refreshscene == true) {
		this.updateScene();
	}
};

Oasis.prototype.setGameState = function(data) {
	this.gamestate = data.gamestate;

	this.updateScene();
};

Oasis.prototype.updateScene = function() {
	switch(this.gamestate) {
	case 0:
		break;
	case 1: //hero selection		
		if(this.myindex == this.playerindex) {
			Crafty.scene("heroselection");
		} else {
			Crafty.scene("herosselectionwait");
		}
		updateInfo();
		break;
	case 2: //gamebegin
		Crafty.scene("main");
		updateInfo();
		break;
	case 3: //gameover
		Crafty.scene("gameover");
		break;
	}
};


Crafty.scene("main",function() {
	Logger.log(Logger.enum.notice,"main scene " + oasis.layout);
	Crafty.background("black");		
	oasis.board.generateEntities();			

	for(var i=0; i<oasis.units.count();i++) {
		oasis.units.get(i).generateEntity();
	}
});



