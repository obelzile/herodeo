Crafty.c("Tooltip", {
	_message: "",
	_entity: null,
	
	_mouseover: function(e) {
		this._entity.x = this.x+10;
		this._entity.y = this.y-10;
		this._entity.visible = true;
	},
	
	_mouseout: function(e) {
		this._entity.visible = false;
	},
	
	init: function() {
		this.requires("Mouse");
		
		this.bind("EnterFrame", function() {
			
		});
		
		this.bind("MouseOver", this._mouseover);
		this.bind("MouseOut", this._mouseout);
	},
	
	disposeTooltip: function() {
		if(this._entity != null) {
			this._entity.destroy();
		}
	},
	
	tooltip: function(message) {
		this._message = message;
		this._entity = Crafty.e("2D,DOM,Color,Text")
		.color("#808080")
		.attr({x: this.x+10,y:this.y-10,w:this._message.width('bold 15px Arial'),h:20,z:20,visible:false})
		.css({'padding': '2px','border': '2px solid black'})
		.textFont({ size: '15px', type: 'bold', family: 'Arial' })
		.textColor("#000000")
		.text(this._message);
		return this;
	},
	
	setTooltip : function(message) {
		this._message = message;
		this._entity.text(this._message);
		this._entity.attr({w:this._message.width('bold 15px Arial')});
	}	
});
