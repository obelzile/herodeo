var game = null;
var users = null;
var heroesdata = null;
var spellsdata = null;
var MAX_PLAYER = 4;

function init_network() {
	Logger.log(Logger.enum.notice,"init_network");

	game = WSockets.open("ws://" + ip + "/HeroDeo/goWS?UUID=" + useruuid,"game");
	game.onopen = onConnectionOpen;
	
	//game.registerAction("userjoinroom", onPlayerJoin);
	
	game.registerAction("userleftroom", onPlayerLeft);
	
	game.registerAction("playerdata", onPlayerData);
	game.registerAction("userlist", onUserList);
	game.registerAction("herosdata", onHeroesData);
	game.registerAction("spellsdata", onSpellsData);
	game.registerAction("itemsdata", onItemsData);
	
	
	game.registerAction("setlayout", onSetLayout);
	game.registerAction("gamestarted",onGameStarted);
	game.registerAction("heroselectionstart",onHeroSelectionStart);
	
	game.registerAction("unitupdate",onUnitUpdate);
	game.registerAction("unitsupdate",onUnitsUpdate);
	game.registerAction("unitremove",onRemoveUnit);	
	game.registerAction("unitmove",onMoveUnit);
	
	game.registerAction("playerupdate",onPlayerUpdate);
	game.registerAction("playersupdate",onPlayersUpdate);
	
	game.registerAction("monstersupdate",onMonstersUpdate);
		
	game.registerAction("gameStateChanged",onGameStateChanged);
	game.registerAction("turnchanged",onTurnChanged);	
		
	game.registerAction("onMoveState", onMoveState);
	
	game.registerAction("onMonsterMoveState", onMonsterMoveState);
	
	game.registerAction("newplayerindexturn", onPlayerTurn);
	game.registerAction("gamebegin", onGameBegin);
	
	game.registerAction("heroselected", onHeroSelected);
	
	game.registerAction("waitheroselection", onWaitHeroSelection);
			
	game.registerAction("newTurn", onNewTurn);
	game.registerAction("newPlayerTurn", onNewPlayerTurn);
	game.registerAction("bonusPhase", onBonusPhase);
	game.registerAction("mainPhase", onMainPhase);	
	
	game.registerAction("attacktargetsupdate", onAttackTargetsUpdate);
	
	//combat_ui.js
	game.registerAction("startcombat", onStartCombat);
	game.registerAction("endcombat", onEndCombat);	
	game.registerAction("attackperformed", onAttackPerformed);
	game.registerAction("counterattack", onCounterAttack);
	
	game.registerAction("treasuresupdate", onTreasuresUpdate);
	
	
	game.registerAction("spellsWindow", onSpellWindow);
	game.registerAction("castspell", onCastSpell);
	
	game.registerAction("merchantWindow", onMerchantWindow);
	game.registerAction("retirermarqueur", onRetirerMarqueur);
	
	game.registerAction("gamemessage", onGameMessage);
		
	game.registerAction("requestchoice", onRequestChoice);
	game.registerAction("requesttarget", onRequestTarget);
	
	game.registerAction("updatecontrols", onUpdateControls);	
}

function onUpdateControls(data) {
	if(oasis.gamecontrols == null)
		oasis.gamecontrols = new GameControls(data);
		
	oasis.gamecontrols.updateControls(data);
}

function relayTarget(target) {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	params.target = {x:target.attr("x") / TILE_SIZE,y:target.attr("y") / TILE_SIZE};
	
	game.sendMessage("ExecuteInitScripts",params);	
}

function relayChoice(index,choice) {
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	params.choice = index;
	
	game.sendMessage("ExecuteInitScripts",params);	
}

function onRequestChoice(data) {
	Logger.log(Logger.enum.notice,"onRequestChoice");
	
	Choices.init(data);
	Choices.clearChoices();
	
	for(index in data.choices) {
		Choices.addChoice(new ChoiceItem(data.choices[index],oasis.spells.setChoice));
	}
	
	Choices.show();
}

function onRequestTarget(data) {
	Logger.log(Logger.enum.notice,"onRequestTarget");
	
	oasis.target.replyto = data.replyto;
	
	if(data.targets.length > 0) {
		params = {"celltype":[],"position":data.targets};	
		oasis.target.queryTarget(params,relayTarget);			
		return;
	}
}

function onGameMessage(data) {
	ChatConsole.addGameMessage(data);
}

function onRetirerMarqueur(data) {
	
	oasis.removeTreasure(data);
	
}

function onCastSpell(data) {
	
	ChatConsole.addGameMessage(data);
	//alert(data.message);
}

function onMerchantWindow(data) {
	//oasis.merchant.setShopingList(data.shoppingList);
	oasis.merchant.show(data.shoppingList);
}

function onSpellWindow(data) {
	oasis.spells.showSpells(data);
}

function onSetLayout(data) {
	Logger.log(Logger.enum.notice,"onSetLayout");
	
}

function onTreasuresUpdate(data) {
	Logger.log(Logger.enum.notice,"onPlayerUpdate");
	oasis.updateTreasures(data);
}

function onUnitUpdate(data) {
	Logger.log(Logger.enum.notice,"onUnitUpdate");
	oasis.updateUnit(data);
}

function onUnitsUpdate(data) {
	Logger.log(Logger.enum.notice,"onUnitsUpdate");
	oasis.updateUnits(data);
}


function onRemoveUnit(data) {
	Logger.log(Logger.enum.notice,"onRemoveUnit");
	oasis.removeUnit(data);
}

function onMoveUnit(data) {
	Logger.log(Logger.enum.notice,"onMoveUnit");
	oasis.moveUnit(data);	
}

function onPlayerUpdate(data) {
	Logger.log(Logger.enum.notice,"onPlayerUpdate");
	oasis.updatePlayer(data);
}

function onPlayersUpdate(data) {
	Logger.log(Logger.enum.notice,"onPlayersUpdate");
	oasis.updatePlayers(data);	
}

function onMonstersUpdate(data) {
	Logger.log(Logger.enum.notice,"onMonstersUpdate");
	oasis.updateMonsters(data);	
}

function onAttackTargetsUpdate(data) {	
	Logger.log(Logger.enum.notice,"onAttackTargetsUpdate");
	oasis.attackTargetsUpdate(data);
}

function onNewTurn(data) {
	Logger.log(Logger.enum.notice,"onNewTurn");
	oasis.turn = data.turn;	
}

function onNewPlayerTurn(data) {
	Logger.log(Logger.enum.notice,"onNewPlayerTurn");
	oasis.newPlayerTurn(data);
}

function onMonsterMoveState(data) {
	Logger.log(Logger.enum.notice,"onMonsterMoveState");
	oasis.onMonsterMoveState(data);
}

function onMoveState(data) {
	Logger.log(Logger.enum.notice,"onMoveState");
	oasis.onMoveState(data);
}

function onBonusPhase(data) {
	Logger.log(Logger.enum.notice,"onBonusPhase");
	oasis.bonusPhase(data);
}

function onMainPhase(data) {
	Logger.log(Logger.enum.notice,"onMainPhase");
	oasis.mainPhase(data);
}

function onConnectionOpen() { 	
	params = {};
	params.roomuuid = roomuuid;
	params.roompw = pw;
	
	game.sendMessage("ConnectToRoom",params);	
}

function onGameStarted(data) {
	Logger.log(Logger.enum.notice,"onGameStarted");
	
	$("#startgame").hide();
	
	oasis = new Oasis();
	oasis.init(data);
	//TODO warn server ready state
}

function onHeroSelectionStart(data) {
	Logger.log(Logger.enum.notice,"onHeroSelectionStart");
	
	// all ready start hero selection
	game.registerAction("playerindexchanged",onPlayerIndexChanged_heroSelection);
	
	oasis.run();
}

function onGameBegin(data) {
	Logger.log(Logger.enum.notice,"onGameBegin");
	oasis.run();
}


function onGameStateChanged(data) {
	Logger.log(Logger.enum.notice,"onGameStateChanged");
	
	if(oasis != null)
		oasis.setGameState(data);	
}

function onTurnChanged(data) {
	Logger.log(Logger.enum.notice,"onTurnChanged");
}

function onWaitHeroSelection(data) {
	Logger.log(Logger.enum.notice,"onWaitHeroSelection");
	
}

function onHeroesData(data) {
	Logger.log(Logger.enum.notice,"onHeroesData");
	
	heroesdata = data.heroes;
}

function onSpellsData(data) {
	Logger.log(Logger.enum.notice,"onSpellsData");
	
	spellsdata = data.spells;
}

function onItemsData(data) {
	Logger.log(Logger.enum.notice,"onItemsData");
	
	itemsdata = data.items;
}

function onUserList(data){
	Logger.log(Logger.enum.notice,"onUserList");
	
	if(data.userlist.length > MAX_PLAYER)
		throw new Error("received more then MAX_PLAYER");
	
	users = data.userlist;		
	displayUsers();
	console.log(users.toString());
}

function onPlayerLeft(data) {
	Logger.log(Logger.enum.notice,"onPlayerLeft");	
	for(var j=0;j<users.length;j++) {
		if(users[j] === data.nickname) {
			users[j] = "";
		}		
	}
	displayUsers();	
}

function onPlayerTurn(data) {
	Logger.log(Logger.enum.notice,"onPlayerTurn");	
	
	oasis.playerindexturn = data.playerindex;
}

function onPlayerData(data) {
	Logger.log(Logger.enum.notice,"onPlayerData");	
}

function startgame() {
	
	params = {};
	params.roomuuid = roomuuid;
	
	game.sendMessage("StartGame",params);
}

function displayUsers() {
	var i=0;
	for(i=0;i<users.length;i++) {
		$("#player"+i).val(users[i]);
	}	
}
