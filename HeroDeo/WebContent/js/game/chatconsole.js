
var ChatConsole = (function() {
	
	// css selector
	var _selector = "";
				
	return {
		/**
		* init the ChatConsole with the css selector 
		*
		* @method init
		* @param {String} selector css selector
		*/
		init : function(selector) {
			_selector = selector;
		},
		
		/**
		* add a player chat message to the chat console
		*
		* @method addChatMessage
		* @param {Message} message object 
		*/		
		addChatMessage : function(m) {
			if (userNickname == m.nickname) {
				$(_selector + " #message").find("ul").append("<li class='red'>"+m.nickname + " : " + m.message+"</li>");
			}
			else {
				$(_selector + " #message").find("ul").append("<li>"+e.nickname + " : " + m.message+"</li>");
			}
		},
		
		/**
		* add a game event to the chat console 
		*
		* @method addGameMessage
		* @param {Message} message object
		*/		
		addGameMessage : function(m) {
			$(_selector + " #message").find("ul").append("<li class='blue'>\>\> "+m.nickname + " : " + m.message+"</li>");
		}
	};
	
})();
