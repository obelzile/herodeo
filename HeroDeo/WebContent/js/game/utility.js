(function($) {
    $.fn.invisible = function() {
        return this.each(function() {
            $(this).css("visibility", "hidden");
        });
    };
    $.fn.visible = function() {
        return this.each(function() {
            $(this).css("visibility", "visible");
        });
    };
    
    String.prototype.width = function(font) {
  	  var f = font || '12px arial',
  	      o = $('<div>' + this + '</div>')
  	            .css({'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font': f})
  	            .appendTo($('body')),
  	      w = o.width();

  	  o.remove();

  	  return w;
  };
}(jQuery));