function Monster(monster) {
	Unit.call(this,monster);
	this.alive = true;
	this.price = monster.price;	
	
	if(oasis.initialised == true) {
		Monster.prototype.generateEntity.call(this);		
	}
	
	this.update = function(monster) {
		var oldId = this.id;
		
		Unit.prototype.update.call(this,monster);
		this.monsterType = monster.monsterType;
		this.price = monster.price;	
		
		if(oldId !== monster.id) {
			Monster.prototype.generateEntity.call(this);
		}		
	};
	
	this.getTotalStrength = function() {
		return this.strength;
	};
	
	this.getTotalProtection = function() {
		return this.protection;
	};
}

Monster.prototype.updateUI = function() {
	
};

Monster.prototype.dispose = function() {
	this.dispose();
};

Monster.prototype.generateEntity = function() {
	if(this.entity != null) {
		this.dispose();
	}
	
	this.entity = Crafty.e("2D,Canvas,Tooltip,MonsterControls,monster_"+ this.id +",Collision")
	.attr({x:this.position.x*TILE_SIZE, y:this.position.y*TILE_SIZE,z:10,movable:false})
	.tooltip(this.name)
	.bind("Moved",oasis.onMonsterMoving);
};