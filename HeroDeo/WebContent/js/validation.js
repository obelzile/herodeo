emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/;  

var isShown = false;
var isEmailAvailable = true;
var html ='<form id="registerForm" action="go" method="post"><h1>Inscription</h1><fieldset id="inputs"><label>Courriel :</label><input id="emailInputRegister" type="text" maxlength="30"  name="email" autofocus ><div id="validationCheckEmail"></div>  <label>Pseudonyme :</label><input id="nickInputRegister" type="text" maxlength="30"  name="nickname" > <label>Pr&eacute;nom :</label><input id="firstnameInputRegister" type="text" maxlength="30"  name="firstname"><label>Nom de famille :</label><input id="lastnameInputRegister" type="text" maxlength="30"  name="lastname" autofocus ><label>Mot de passe :</label><input id="passwordInputRegister" type="password" maxlength="12" name="pw" ><div id="validationCheckPW"></div>  <label>Confirmation du mot de passe :</label><input id="passwordInputRegister2nd" type="password" maxlength="12" ></fieldset><fieldset id="actions"></fieldset><input type="submit" id="continuerBt" class="button gray" value="Continuer"></fieldset><input type="hidden" name="action" value="<%=RegisterAction.class.getSimpleName()%>" /></form>'
	
$( function() {
	$("#popupMain").hide();
	
	$(window).scroll(function(){
	      var headerChrome = document.body.scrollTop;
	      console.log(headerChrome);
	      if (headerChrome > 450 && isShown === false) {
	    	  	isShown = true;
	    		$('.circle').each(function (index){
	    			$(this).animate({
	    				opacity: 1
	    			}, 500 );
	    	});
	      }
	      
	    if (headerChrome > 370 && headerChrome < 450 && isShown === true) {
	    		isShown = false;
	    		$('.circle').each(function (index){
	    			$(this).animate({
	    				opacity: 0
	    			}, 500 ); 	
	    		});
	    }
	});
	
	if(document.getElementById("loginErrorBox") !== null)
	{
		$("#loginErrorBox").delay(1000).fadeOut(1000);
	}
	
	$("#emailInputRegister").bind('input propertychange', function() {
		var email = $("#emailInputRegister").val();
		var test = emailPattern.test(email);
		
		if ( test == true &&  email.length >= 6 ) {
			checkEmailAvailable();
		}	
		else {
			$("#validationCheckEmail").html("<img src='img/x_red.png'/>");
		}
	});
	
	$("#registerForm").submit( function(event) {
		 event.preventDefault();
		 
		if( isEmailAvailable === false) {
		}  else if ($("#passwordInputRegister").val() != $("#passwordInputRegister2nd").val()) {
			$("#validationCheckPW").show();
			$("#validationCheckPW").html('<div class="popup west"><ul><li>Mauvaise combinaison</li></ul><span class="arrow"></span></div>').delay(1000).fadeOut(500).css("display","inline-block");
			return false;
		} else if ($("#passwordInputRegister").val().length < 4 || $("#passwordInputRegister2nd").val().length < 4 ) {
			$("#validationCheckPW").show();
			$("#validationCheckPW").html('<div class="popup west"><ul><li>Mot de passe trop court</li></ul><span class="arrow"></span></div>').delay(1000).fadeOut(500).css("display","inline-block");
			return false;
		} else {
			$.post('go', $(this).serialize())
			.done ( function() {
				$("#registerForm").animate({
					opacity: 0
				}, 1000);
				$("#cercles").css({
					'-webkit-animation-play-state': 'running'
				});
				setTimeout(function() {
					$("#cercles").css('-webkit-animation-play-state', 'paused');
				}, 1000);
				clearRegisterForm();
			});		
		}
	});

	$("#loginBt").click(function() {
		$("#loginForm").submit();
	});
	
	$("#signupBt").click(function() {
		$("#cercles").css({
			'-webkit-animation': 'myfirst 1s linear infinite alternate',
			'-webkit-transform-origin' : '-35% -70%',
		});
		setTimeout(function() {
			$("#registerForm").animate({
				opacity: 1
			}, 500);
			$("#cercles").css('-webkit-animation-play-state', 'paused');
		}, 1000);
	});
});

function checkEmailAvailable() {
	$.post('go', {action : "CheckEmailAction",email : $("#emailInputRegister").val()})
	.done ( function() {
		$("#validationCheckEmail").html("<img src='img/green_check_mark.png'/>"); 
	})
    .fail( function() {
    	$("#validationCheckEmail").html("<img src='img/x_red.png'/>");
    	$("#validationCheckEmail").append('<div class="popup west"><ul><li>Ce courriel est deja pris</li></ul><span class="arrow"></span></div>');
    	isEmailAvailable = false;
    });
}
	
function clearRegisterForm() {
	$("#registerForm input").each(function(){ $(this).val(''); });
	$("#validationCheckEmail").html("");
	isEmailAvailable = true;
}
	