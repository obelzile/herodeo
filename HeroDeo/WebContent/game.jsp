<%@page import="com.khaosstar.herodeo.model.game.GameRoom"%>
<%@page import="com.khaosstar.herodeo.model.actions.CreateLobbyAction"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.khaosstar.herodeo.model.entities.WSUser"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>  
<%
WSUser connectedUser = null;
ArrayList<String> alOnlineUsers;
Boolean isAdmin;
String roomName, userUUid, roomUUID, roomPW;

if (session == null || session.getAttribute("user_info") == null ) {
	RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
	rd.forward(request, response);
} else {
	connectedUser = (WSUser)session.getAttribute("user_info");
	userUUid = connectedUser.getUUID();
	
	roomName = request.getParameter("roomName");
	roomUUID = (String)request.getAttribute("roomUUID");
	roomPW = (String)request.getAttribute("roomPW");
	isAdmin = (Boolean)request.getAttribute("isAdmin");
%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">
<title>OASIS - Lobby : <%=roomName%></title>
<link rel="stylesheet" href="css/game.css" />
<link rel="stylesheet" href="css/main.css" />
<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/global.js"></script>
<script type="text/javascript" src="js/logger.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/game/utility.js"></script>
<script type="text/javascript" src="js/websocket.js"></script>
<script type="text/javascript" src="js/game.js"></script>
<script type="text/javascript" src="js/game/crafty.js"></script>
<script type="text/javascript" src="js/game/gameover.js"></script>
<script type="text/javascript" src="js/game/game_ui.js"></script>
<script type="text/javascript" src="js/game/combat_ui.js"></script>
<script type="text/javascript" src="js/game/oasis.js"></script>
<script type="text/javascript" src="js/game/player.js"></script>
<script type="text/javascript" src="js/game/unit.js"></script>
<script type="text/javascript" src="js/game/units.js"></script>
<script type="text/javascript" src="js/game/playerinfo.js"></script>
<script type="text/javascript" src="js/game/canvastext.js"></script>
<script type="text/javascript" src="js/game/hero_selection.js"></script>
<script type="text/javascript" src="js/game/loading.js"></script>
<script type="text/javascript" src="js/game/playercontrolscomponent.js"></script>
<script type="text/javascript" src="js/game/monstercontrolscomponent.js"></script>
<script type="text/javascript" src="js/game/board_layout.js"></script>
<script type="text/javascript" src="js/game/oasis_network.js"></script>
<script type="text/javascript" src="js/game/oasis_game.js"></script>
<script type="text/javascript" src="js/game/target.js"></script>
<script type="text/javascript" src="js/game/spells.js"></script>
<script type="text/javascript" src="js/game/merchant.js"></script>
<script type="text/javascript" src="js/game/chatconsole.js"></script>
<script type="text/javascript" src="js/game/tooltip.js"></script>
<script type="text/javascript" src="js/game/monster.js"></script>
<script type="text/javascript" src="js/game/choices.js"></script>
<script type="text/javascript" src="js/game/monstercontrol.js"></script>
<script type="text/javascript" src="js/game/gamecontrols.js"></script>

<script>
var roomuuid = "<%=roomUUID%>";
var pw = "<%=roomPW%>";
var useruuid = "<%=userUUid%>";

function onUserKicked(e) {
	console.log("onUserKicked: " + JSON.stringify(e, replacer));
	$("#connectLobbyForm").submit();
}

function onUserQuit(e) {
	console.log("onUserKicked: " + JSON.stringify(e, replacer));
	var $input = $("body").find(':contains('+e.nickname+')');
	$input.val("");
}
	
</script>
</head>
<body>

<%@include file="jspmodules/spells.jsp" %>

<%@include file="jspmodules/market.jsp" %>

<%@include file="jspmodules/combat.jsp" %>

<%@include file="jspmodules/choices.jsp" %>

<div class="container">
	<div id="wrapper">
		<div id="left-column">
			<div class="wrapper-column">
	            <div class="top-row"><div style="float: right;"><%@include file="jspmodules/playerinfo.jsp" %></div></div>
	            <div class="middle-row">
	            	<div style="width: 338px;margin-right: 15px;margin-top: 5px; float: right;">
		            	<div id="turn">Turn : <span id="turnvalue">0</span></div>
						<div id="lobbyName">Game name : <%=roomName%></div>
						<button id="logoutBt" type="Submit">Quitter la partie</button>
							
						<div id="chatWindow">
							<div id="message">
								<ul>			 		
								</ul>
						    </div>
							<input id="chatBar" type="text" value=""/>   
							<form style="height: 0px; width: 0px;"id="connectLobbyForm" action="go" method="post">
								<input id="inputLobbyName" name="roomName" type="hidden" value="Global Chat"/>
								<input id="inputLobbyPW" name="pw" type="hidden" value=""/>
								<input name="action" type="hidden" value="<%=CreateLobbyAction.class.getSimpleName()%>"/>
							</form>
						</div>	
	            	</div>
	            </div>
	            <div class="bottom-row"><div style="float: right;"><%@include file="jspmodules/playerinfo.jsp" %></div></div>
	        </div>   
		</div>
		<div id="middle-column">
			<div id="cr-stage"></div>
			<%@include file="jspmodules/gamelobby.jsp" %>
		</div>
		<div id="right-column">
			<div class="wrapper-column">
	            <div class="top-row"><div style="float: left;"><%@include file="jspmodules/playerinfo.jsp" %></div></div>
	            <div class="middle-row">
	            	<div id="control-ui" style="margin-left: 15px;">
	            		<div id="monster-control">
		            		<button id="move_monster_button" title="Déplacement Monstre" class="button-action"><img src="images/buttons/action-monsterdeplacement.png"/></button>			
							<button id="move_monster_done_button" title="Déplacement Monstre terminer" class="button-action" ><img src="images/buttons/action-monsterdeplacement-done.png"/><span id="monster-movement-count">5</span></button>
							<button id="monster_attack_button" title="Monstre Attaque" class="button-action"><img src="images/buttons/action-monstercombat.png"/></button>
							<button id="monster_end_control_button" title="Fin Controle" class="button-action"><img src="images/buttons/action-monsterfincontrole.png"/></button>
	            		</div>
	            		<div id="player-control">
							<button id="move_button" title="Déplacement" class="button-action"><img src="images/buttons/action-deplacement.png"/></button>			
							<button id="move_done_button" title="Déplacement terminer" class="button-action" ><img src="images/buttons/action-deplacement-done.png"/><span id="movement-count">5</span></button>
							<button id="attack_button" title="Attaquer" class="button-action"><img src="images/buttons/action-combat.png"/></button>
							<button id="boire_button" title="Boire de l'eau" class="button-action"><img src="images/buttons/action-boireeau.png"/></button>
							<button id="jeterpiece_button" title="Jeter piece" class="button-action"><img src="images/buttons/action-jeterpieceorpuits.png"/></button>
							<button id="ramassermarqueur_button" title="Ramasser marqueur" class="button-action"><img src="images/buttons/action-ramassermarqueur.png"/></button>
							<button id="sortilege_button" title="Lancer sortilège" class="button-action"><img src="images/buttons/action-sortilege.png"/></button>
							<button id="sortilege_novice_button" title="Lancer un sortilège novice gratuit" class="button-action"><img src="images/buttons/action-sortilegenovicegratuit.png"/></button>
							<button id="sortilege_gratuit_button" title="Lancer un sortilège gratuit" class="button-action"><img src="images/buttons/action-sortilegegratuit.png"/></button>
							<button id="visite_marchand_button" title="Visiter Marchand" class="button-action"><img src="images/buttons/action-visitermarchand.png"/></button>
							<button id="controler_monster_button" title="Control d'un monstre" class="button-action"><img src="images/buttons/action-controlermonstre.png"/></button>
							<button id="endturn_button" title="Fin du tour" class="button-action"><img src="images/buttons/action-fintour.png"/></button>
						</div>
					</div>
				</div>
	            <div class="bottom-row"><div style="float: left;"><%@include file="jspmodules/playerinfo.jsp" %></div></div>
	        </div> 
	    </div>
	</div>
	

</div>
</body>
</html>
<%}%>
